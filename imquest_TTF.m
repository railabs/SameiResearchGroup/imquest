% Copyright 20xx - 2019. Duke University
classdef imquest_TTF < handle
    
    properties
        graphicHandle
        tool %handle to the imquest object
        selected = false;
        marker = '.';
        lineWidth = [1 2];
        visible = true;
        type
        f
        TTF
        contrast
        settings
        stats
        slices
        psize
        sliceThickness
        SeriesInstanceUID
        withGraphics %boolean that is needed to create copies of the object without graphics components
        versionInfoTable
    end
    
    properties (Dependent = true)
        label
        fullLabel
        TTFinfo
        noise
        f10
        f50
        TTFmax
        IdealTTF    %This is the ideal TTF given the pixel size (i.e., sinc(psize*f))
    end
    
    events
        TTFdeleted
    end
    
    methods
        
        function TTFhandle = imquest_TTF(varargin)
            
            switch nargin
                case 1 %This is the constructor used when making copies of the object
                    TTF = varargin{1};
                    TTFhandle.withGraphics = false;
                    TTFhandle.type=TTF.type;
                    TTFhandle.f=TTF.f;
                    TTFhandle.TTF=TTF.TTF;
                    TTFhandle.contrast = TTF.contrast;
                    TTFhandle.graphicHandle=[];
                    TTFhandle.tool=[];
                    TTFhandle.settings=TTF.settings;
                    TTFhandle.stats=TTF.stats;
                    TTFhandle.slices = TTF.slices;
                    TTFhandle.psize=TTF.psize;
                    TTFhandle.sliceThickness=TTF.sliceThickness;
                    TTFhandle.SeriesInstanceUID=TTF.SeriesInstanceUID;
                    TTFhandle.versionInfoTable=TTF.versionInfoTable;
                  
                case 2 %This is the normal contrusctor
                    tool = varargin{1};
                    TTFROI = varargin{2};
                    if isa(TTFROI,'imquest_TTF')
                        TTFhandle = copy(TTFROI);
                        TTFhandle.tool = tool;
                        TTFhandle.withGraphics = true;
                        TTFhandle.graphicHandle = plot(TTFhandle.f,TTFhandle.TTF,'Parent',tool.handles.TTFaxis,'LineWidth',TTFhandle.lineWidth(1),'MarkerSize',12);
                    else
                        
                        TTFhandle.withGraphics = true;
                        TTFhandle.type='XY';
                        TTFhandle.tool=tool;
                        
                        %Get the slices of interest
                        [im, TTFhandle.slices] = getImageSlices(tool,tool.handles.TTFsliceSelector);
                        
                        %Get the DICOM series UID
                        TTFhandle.SeriesInstanceUID = tool.DICOMheaders(TTFhandle.slices(1)).SeriesInstanceUID;
                        
                        %Get the pixel size
                        psize = tool.CTpsize;
                        psize=psize(1);
                        TTFhandle.psize=psize;
                        TTFhandle.sliceThickness=tool.CTsliceThickness;
                        
                        %Get the inner circle ROI handle
                        InnerCircleROI = getMatchingTTFInnerCircleROI(tool,TTFROI);
                        
                        %Construct the ROI that vector that get2DTTF is expecting
                        pos = getPosition(TTFROI);
                        ROI = pos(1:2); %Center position (in pixels)
                        R = mean(psize*pos(3:4)/2); %size of outer ROI in mm
                        rod = R*InnerCircleROI.R;
                        ROI = [ROI R rod]; %This is the format expected by the TTF code, ROI = [Cx Cy R rod]
                        
                        %Get the spatial frequency extent
                        ny = 1/(2*psize);
                        fmax = ceil(2*ny*sqrt(2));
                        fSamples = fmax*64;
                        fRange = [0 fmax];
                        
                        %compute the TTF
                        [TTFhandle.f, TTFhandle.TTF, TTFhandle.contrast, TTFhandle.settings, TTFhandle.stats] = get2DTTF(im,ROI,psize,'Verbose',false, 'fRange',fRange,'fSamples',fSamples);
                        
                        if TTFhandle.stats.f10>ny
                            %tool.showTempStatus('Warning, measured TTF f10 is greater than the Nyquist frequency');
                        end
                        %adjust the position of the ROI to match where the center was
                        %identified
                        newPositionSameSize(TTFROI,TTFhandle.stats.center);
                        
                        %make the plot
                        TTFhandle.withGraphics = true;
                        TTFhandle.graphicHandle = plot(TTFhandle.f,TTFhandle.TTF,'Parent',tool.handles.TTFaxis,'LineWidth',TTFhandle.lineWidth(1),'MarkerSize',12);
                        
                        %Get the imquest version info
                        TTFhandle.versionInfoTable=tool.versionInfoTable;
                    end
                    
                    %add to the imquest list of measured TTFs
                    addTTF(tool,TTFhandle);
            end
            
            
        end
        
        function label = get.label(TTF)
            label = ['Contrast: ' sprintf('%3.1f',TTF.contrast) ' HU'];
        end
        
        function t = get.fullLabel(TTF)
            t={};
            t{end+1,1} = ['Type: ' TTF.type];
            t{end+1,1} = TTF.label;
            t{end+1,1} = ['f10: ' sprintf('%1.2f',TTF.f10) ' 1/mm'];
            t{end+1,1} = ['f50: ' sprintf('%1.2f',TTF.f50) ' 1/mm'];
            t{end+1,1} = ['Noise: ' sprintf('%.2f', TTF.noise) ' HU'];
            t{end+1,1} = ['CNR: ' sprintf('%.2f',TTF.stats.CNR)];
            t{end+1,1} = ['Total CNR: ' sprintf('%.2f',TTF.stats.CNR_total)];
            t{end+1,1} = ['Rod Center: (' sprintf('%.1f',TTF.stats.center(1)) ',' sprintf('%.1f',TTF.stats.center(2)) ')'];
            t{end+1,1} = ['Slice thickness: ' num2str(TTF.sliceThickness) ' mm'];
            t{end+1,1} = ['Slices: ' num2str(TTF.slices(1)) ' to ' num2str(TTF.slices(2))];
        end
        
        function set.selected(TTF,selected)
            if TTF.withGraphics
                TTF.selected = selected;
                if selected
                    set(TTF.graphicHandle,'Marker',TTF.marker,'LineWidth',TTF.lineWidth(2));
                    %uistack(TTF.graphicHandle,'top');
                    
                else
                    set(TTF.graphicHandle,'Marker','none','LineWidth',TTF.lineWidth(1));
                end
            end
        end
        
        function set.marker(TTF,marker)
            if TTF.withGraphics
                TTF.marker = marker;
                if TTF.selected
                    set(TTF.graphicHandle,'Marker',marker)
                end
            end
        end
        
        function showTTFinfo(TTF)
            h = imageinfo(TTF.TTFinfo);
            set(h,'Name',['TTF Info']);
        end
        
        function TTFinfo = get.TTFinfo(TTF)
            %The puts some of the key meta data about the TTF into a
            %structured variable so it can be easily shown with an
            %imageinfo figure
            TTFinfo.type=TTF.type;
            TTFinfo.contrast=TTF.contrast;
            TTFinfo.slices = TTF.slices;
            TTFinfo.SeriesInstanceUID=TTF.SeriesInstanceUID;
            %get the stats fieldnames
            names = fieldnames(TTF.stats);
            for i=1:length(names)
                TTFinfo.(names{i})=TTF.stats.(names{i});
            end
            names = fieldnames(TTF.settings);
            for i=1:length(names)
                TTFinfo.(names{i})=TTF.settings.(names{i});
            end
            
            tab = TTF.versionInfoTable;
            TTFinfo.(tab.RepositoryName)=['Git branch: ' tab.BranchName ', Commit ID: ' tab.GitID];
%             for i=1:size(tab,1)
%                 TTFinfo.(tab{i,1}{1})=['Git branch: ' tab{i,3}{1} ', Commit ID: ' tab{i,2}{1}];
%             end
            
            
        end
        
        function f10 = get.f10(TTF)
            f10 = TTF.stats.f10;
        end
        
        function f50 = get.f50(TTF)
            f50 = TTF.stats.f50;
        end
        
        function noise = get.noise(TTF)
            noise = TTF.stats.noise;
        end
        
        function TTFmax = get.TTFmax(TTF)
            TTFmax=max(TTF.TTF);
        end
        
        function varargout = getResampled1DTTF(TTF,psize,N)
            
            %Create coordinate system
            fx = getFFTfrequency(psize(1),N(1));
            
            %perform 1D linear interpolation
            TTFup = interp1(TTF.f,TTF.TTF,abs(fx),'linear',0);
            
            switch nargout
                case 1
                    varargout{1}=TTFup;
                case 2
                    varargout{1} = fx;
                    varargout{2} = TTFup;
            end
            
        end
        
        function varargout = getResampled2DTTF(TTF,psize,N)
            %This is useful for getting a 2D TTF that matches a task
            %function/NPS
            
            %Create a coordinate system
            fx = getFFTfrequency(psize(1),N(1));
            if length(psize)>1 && length(N)>1
                fy = getFFTfrequency(psize(2),N(2));
            elseif length(psize)>1 && length(N)==1
                fy = getFFTfrequency(psize(2),N);
            else
                fy=fx;
            end
            [Fx,Fy]=meshgrid(fx,fy);
            [~,Fr]=cart2pol(Fx,Fy);
            
            %perform 1D linear interpolation
            TTFup = interp1(TTF.f,TTF.TTF,Fr,'linear',0);
            
            switch nargout
                case 1
                    varargout{1}=TTFup;
                case 3
                    varargout{1}=fx;
                    varargout{2}=fy;
                    varargout{3}=TTFup;
            end
            
        end
        
        function varargout = getResampled3DTTF(TTFx,TTFy,TTFz,psize,N)
            
            %Get 1D TTFs
            [fx, tx] = getResampled1DTTF(TTFx,psize(1),N(1));
            [fy, ty] = getResampled1DTTF(TTFy,psize(2),N(2));
            [fz, tz] = getResampled1DTTF(TTFz,psize(3),N(3));
            
            %Mesh them together
            [Tx,Ty,Tz] = meshgrid(tx,ty,tz);
            
            %Compute 3D TTF
            TTFup = Tx.*Ty.*Tz;
            
            switch nargout
                case 1
                    varargout{1}=TTFup;
                case 4
                    varargout{1}=fx;
                    varargout{2}=fy;
                    varargout{3}=fz;
                    varargout{3}=TTFup;
            end
        end
        
        function Iblur = blurWithTTF(TTF,im,psize)
            %This function blurs an image with a given TTF
            
            %Get the number of pixels
            N=[size(im,2) size(im,1)];
            %Get the upsampled TTF
            TTFup = getResampled2DTTF(TTF,psize,N);
            %Blur the image
            MV=mean(im(:)); %get mean value
            im=im-MV; %remove mean
            F=fftn(im); %Fourier Transform
            %F(1)=0;     %Remove DC component (same as filtering with a notch filter)
            TTFup=fftshift(TTFup); %Shift MTF
            H=F.*TTFup; %Apply filtering
            Iblur = real(ifftn(H))+MV; %Inverse fourier transform
            
            
        end
        
        function Iblur = blur3DwithTTFs(TTFx,TTFy,TTFz,im,psize)
            %Applys 3D blurring to im (psize is ordered [x y z], ie, [cols
            %rows slices])
            N=[size(im,2) size(im,1) size(im,3)];
            
            %Get 3D TTF
            TTFup = getResampled3DTTF(TTFx,TTFy,TTFz,psize,N);
            
            %Blur the image
            MV=mean(im(:)); %get mean value
            im=im-MV; %remove mean
            F=fftn(im); %Fourier Transform
            F(1)=0;     %Remove DC component (same as filtering with a notch filter)
            TTFup=fftshift(TTFup); %Shift MTF
            H=F.*TTFup; %Apply filtering
            Iblur = real(ifftn(H))+MV; %Inverse fourier transform
            
        end
        
        function IdealTTF = get.IdealTTF(TTF)
            switch TTF.type
                case 'XY'
                    IdealTTF = sinc(TTF.f*TTF.psize);
                case 'Z'
                    IdealTTF = sinc(TTF.f*TTF.sliceThickness);
            end
            
        end
        
        function newTTF = copy(TTF)
            newTTF = imquest_TTF(TTF);
        end
        
        function writeToCSVFile(TTF,filename)
            out = [TTF.f TTF.TTF];
            out = array2table(out);
            out.Properties.VariableNames = {'f','TTF'};
            writetable(out,filename);
        end
             
        function delete(TTFhandle)
            delete(TTFhandle.graphicHandle);
            notify(TTFhandle,'TTFdeleted');
        end
        
       
    end
    
    
end
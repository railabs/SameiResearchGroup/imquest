% Copyright 20xx - 2019. Duke University
classdef protocolMatcher_Controler < handle
    %This class contains the graphics interface objects used to control
    %the protocolMatcher object
    
    properties
        handles
        position
        units = 'normalized';
        tool %handle to tool object
    end
    
    methods
        function obj = protocolMatcher_Controler(tool,position) %Constructor
            %Set the tool property
            obj.tool=tool;
            
            %Create the panel
            obj.handles.panel = uipanel(tool.handles.fig,...
                'Title','Matching Settings','BackgroundColor','k',...
                'ForegroundColor','w','HighlightColor','k','TitlePosition',...
                'lefttop','FontSize',12);
            set(obj.handles.panel,'Units',obj.units,'Position',position)
            obj.position=position;
            
            %Create the criteria pulldown menu
            ind=find(strcmp(tool.possibleCriteria,tool.criteria));
            fun=@(hObject,evnt) callbacks(hObject,evnt,obj);
            obj.handles.popupCriteria= uicontrol(obj.handles.panel,'Style','popupmenu',...
                'String',tool.possibleCriteria,'Value',ind,'Units','normalized','Position',...
                [0 5/6 .5 1/6],'Callback',fun);
            obj.handles.popupCriteria_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Matching Critera',...
                'Units','normalized','Position',[.5 5/6 .5 1/6],'HorizontalAlignment','left');
            
            %Create the TTF material pulldown menu
            ind=find(strcmp(tool.possibleMaterials,tool.TTFmaterial));
            obj.handles.popupTTFMaterial= uicontrol(obj.handles.panel,'Style','popupmenu',...
                'String',tool.possibleMaterials,'Value',ind,'Units','normalized','Position',...
                [0 4/6 .5 1/6],'Callback',fun);
            obj.handles.popupTTFMaterial_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','TTF Material',...
                'Units','normalized','Position',[.5 4/6 .5 1/6],'HorizontalAlignment','left');
            
            %Create the NPS stat pulldown menu
            ind=find(strcmp(tool.possibleNPSStats,tool.NPSStat));
            obj.handles.popupNPSStat= uicontrol(obj.handles.panel,'Style','popupmenu',...
                'String',tool.possibleNPSStats,'Value',ind,'Units','normalized','Position',...
                [0 3/6 .5 1/6],'Callback',fun);
            obj.handles.popupNPSStat_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','NPS Statistic',...
                'Units','normalized','Position',[.5 3/6 .5 1/6],'HorizontalAlignment','left');
            
            %Create the Reference dose edit box
            obj.handles.editDRef = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(tool.refDose),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 2/6 .5 1/6],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',tool.refDose);
            obj.handles.editDRef_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Reference Dose [mGy]',...
                'Units','normalized','Position',[.5 2/6 .5 1/6],'HorizontalAlignment','left');
            
            %Create the Reference size edit box
            obj.handles.editSizeRef = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(tool.refSize),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 1/6 .5 1/6],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',tool.refSize);
            obj.handles.editSizeRef_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Reference Size (WED) [mm]',...
                'Units','normalized','Position',[.5 1/6 .5 1/6],'HorizontalAlignment','left');
            
            %Create the Reference slice thickness edit box
            obj.handles.editSTRef = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(tool.refSliceThickness),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 0/6 .5 1/6],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',tool.refSliceThickness);
            obj.handles.editSTRef_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Reference Slice Thickness [mm]',...
                'Units','normalized','Position',[.5 0/6 .5 1/6],'HorizontalAlignment','left');
             
            addlistener(tool,'distanceUpdated',@obj.handlePropEvents);
        end
        
        function handlePropEvents(obj,src,evnt,varargin)
            switch evnt.EventName
                case 'distanceUpdated'
                    updateGraphics(obj);
            end
        end
        
        function updateGraphics(obj)
            %Sync the criteria pulldown menu
            ind=find(strcmp(get(obj.handles.popupCriteria,'String'),obj.tool.criteria));
            set(obj.handles.popupCriteria,'Value',ind);
            
            %Sync the TTF pulldown menu
            ind=find(strcmp(get(obj.handles.popupTTFMaterial,'String'),obj.tool.TTFmaterial));
            set(obj.handles.popupTTFMaterial,'Value',ind);
            
            %Sync the NPS pulldown menu
            ind=find(strcmp(get(obj.handles.popupNPSStat,'String'),obj.tool.NPSStat));
            set(obj.handles.popupNPSStat,'Value',ind);
            
            %Sync the Reference Dose text box
            set(obj.handles.editDRef,'String',num2str(obj.tool.refDose));
            
        end
        
        function set.position(obj,position)
            set(obj.handles.panel,'Position',position);
            obj.position=position;
        end
        
        function set.units(obj,units)
            set(obj.handles.panel,'Units',units);
            obj.units=units;
        end
        
    end
    
end

function callbacks(hObject,evnt,obj)
switch hObject
    case obj.handles.popupCriteria
        obj.tool.criteria=obj.tool.possibleCriteria{hObject.Value};
    case obj.handles.popupTTFMaterial
        obj.tool.TTFmaterial=obj.tool.possibleMaterials{hObject.Value};
    case obj.handles.popupNPSStat
        obj.tool.NPSStat=obj.tool.possibleNPSStats{hObject.Value};
    case obj.handles.editDRef
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.tool.refDose
            obj.tool.refDose=new;
        end
    case obj.handles.editSizeRef
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.tool.refDose
            obj.tool.refSize=new;
        end
    case obj.handles.editSTRef
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.tool.refDose
            obj.tool.refSliceThickness=new;
        end
end
end

% Copyright 20xx - 2019. Duke University
classdef protocolMatcher < handle
    properties
        handles
        CDim = protocolMatcher_ContrastDetailImage;
    end
    
    properties (SetObservable)
        distance                                            %This is the distance measured between each row of the data table. data(i,j) is a measure of how different two conditions are. The distance changes depending on the criteria and other settings
        criteria = 'Noise Texture and Resolution';          %String denoting which distance metric to use. see possibleCriteria property for list.
        TTFmaterial = 'Air';                                %String denoted which material should be used as basis of resolution comparision
        NPSStat = 'av';                                     %String denoting which NPS stat to use
        refDose = 15;
        DRange = [0 30];
        ND = 100;
        refSize = 200;                                      %Reference water equivalent diameter assumed when calculating the noise (mm)
        refSliceThickness = 5;                              %Reference slice thickness assumed when calculating the noise (mm)
    end
    
    properties (SetAccess = immutable)
        tab             %This is the data table (without the NPS objects)
        mfile      
        root            %Directory containing the table
        dataDir         %Directoty containing the NPSTTF files
        models          %List of scanner models in the table
        
    end
    
    properties (Hidden = true)
        lists
        scatterbuff = .1;
    end
    
    properties (Constant = true)
        fname = 'protocolMatchingData.mat';
        dataDirName = 'NPSTTFObjects';
        possibleCriteria = {'Noise Texture and Resolution'};
        possibleMaterials = {'Poly','Bone','Air','Acrylic'};
        possibleNPSStats = {'av','peak'};
        colors = {'w','g','r','y','m','c','b'};
    end
    
    properties (Dependent = true)
        criteriaDescription
        TTFMetric           %Returns the f50 for the currently selected TTFmaterial for each row of the table
        NPSMetric           %Returns either fav or fpeak depending on NPSStat
        doses               %Returns a vector of doses (dependent on DRange and ND)
        statsSummaryTable   %Returns a table of data correpsonding to the currently selected conditions in each of the lists
    end
    
    events
        needToUpdateDistance
        distanceUpdated
        refValuesChaged
        
    end
    
    methods
        function tool = protocolMatcher(varargin) %constructor
            
            %make the figure
            tool.handles.fig = figure('Units','Normalized','Position',...
                [.05 .05 .9 .9],'Color','k','MenuBar','none','Name','Protocol Matcher',...
                'Tag','tool.handles.fig','NumberTitle','off');
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,[]);
            set(tool.handles.fig,'ResizeFcn',fun);
            
            %set the root directory
            p = mfilename('fullpath');
            [root, ~, ~] = fileparts(p);
            root = [root filesep];
            tool.root = root;
            
            %Set the dataDir directory
            tool.dataDir= [tool.root tool.dataDirName filesep];
            
            %set the table
            load([tool.root tool.fname]);
            output.rows = (1:size(output,1))';
            tool.tab=output; clear output;
            
            %Set the list of models
            tool.models = unique(tool.tab.ManufacturerModelName);
            
            %Update the distance
            updateDistance(tool)
            
            %Make tab group
            tool.handles.tabs.base = uitabgroup(tool.handles.fig,'Units',...
                'Normalized','Position',[.5 0 .5 1],'TabLocation','top');
            tool.handles.tabs.BarScatter = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','Stats');
            tool.handles.tabs.TTFNPS = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','TTF/NPS');
            tool.handles.tabs.ContrastDetail = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','Images');
            
            %Make axis to show the NPS and TTF data
            tool.handles.TTFNPSAxis = axes(tool.handles.tabs.BarScatter,'Units','normalized',...
                'Position',[.05 .05 .9 .4],'Color','none','XColor','w','YColor','w'); hold on;
            tool.handles.XYScatter=scatter(tool.TTFMetric,tool.NPSMetric,'ow');
            axis equal; axis square; box on;
            xlabel(['TTF f50-' tool.TTFmaterial ' [1/mm]'])
            ylabel(['NPS f' tool.NPSStat ' [1/mm]'])
            xlim([0 max(tool.TTFMetric)+.1]);
            ylim([0 max(tool.NPSMetric)+.1]);
            grid on;
            
            %make the axis to show the dose bar plots
            tool.handles.DoseAxis = axes(tool.handles.tabs.BarScatter,'Units','normalized',...
                'Position',[.05 .55 .9 .4],'Color','none','XColor','w','YColor','w'); hold on;
            grid on; box on;
            ylabel('Dose for equal noise [mGy]');
            set(tool.handles.DoseAxis,'XTick',1:length(tool.models),'XTickLabel',tool.models,...
                'XTickLabelRotation',15);
            
            %Make a grid of axes for the CD images
            gridSize = [4 2]; %Rows and columns of the grid
            rowbuff = .01;
            colbuff = .1;        
            w = (1-(gridSize(2)+1)*colbuff)/gridSize(2); %Width and height of each axis
            h = (1-(gridSize(1)+1)*rowbuff)/gridSize(1);
            for i=1:length(tool.models)
                [row,col] = ind2sub(gridSize,i);
                %Flip the row so row = 1 shows up on top
                row = gridSize(1)-row + 1;
                
                tool.handles.imAxis(i) = axes(tool.handles.tabs.ContrastDetail,...
                    'Units','normalized','Position',[(col-1)*w+col*colbuff (row-1)*h+row*rowbuff w h],...
                    'XColor',tool.colors{i},'YColor',tool.colors{i},'Color','none'); hold on
                box on; 
                set(tool.handles.imAxis(i),'Xlim',[0 tool.CDim.Npix_total],...
                    'Ylim',[0 tool.CDim.Npix_total],'XTick',[],'YTick',[],'Visible','on',...
                    'LineWidth',4);
            end
            
            %Make the axes for the TTF, NPS, and noise vs dose plots
            buff = .075;
            h=(1-buff*4)/3;
            tool.handles.TTFAxis = axes(tool.handles.tabs.TTFNPS,'Units','normalized',...
                'Position',[.05 2*h+3*buff .9 h],'Color','none','XColor','w','YColor','w','XLim',[0 1.2]); hold on;
            xlabel('Spatial Frequency [1/mm]'); ylabel('TTF'); grid on;
            tool.handles.NPSAxis = axes(tool.handles.tabs.TTFNPS,'Units','normalized',...
                'Position',[.05 1*h+2*buff .9 h],'Color','none','XColor','w','YColor','w','XLim',[0 1.2]); hold on;
            xlabel('Spatial Frequency [1/mm]'); ylabel('NPS [mm^2HU^2]'); grid on;
            tool.handles.NoiseDose = axes(tool.handles.tabs.TTFNPS,'Units','normalized',...
                'Position',[.05 buff .9 h],'Color','none','XColor','w','YColor','w','XLim',tool.DRange); hold on;
            xlabel('Dose [mGy]'); ylabel('Noise [HU]'); grid on;
            
            %Make the list boxes
            W=.5; %Total relative width used for these boxes
            n=length(tool.models);
            w=W/n;  %Relative width of a single list
            tool.lists=[];
            for i=1:n
                if i==1
                    refList=[];
                else
                    refList=tool.lists(1);
                end
                tool.lists=[tool.lists; protocolMatcher_ScannerList(tool,tool.handles.fig,tool.models{i},refList,[(i-1)*w 0 w .8],tool.colors{i},i)];
            end
            
            %Make the matching controler object
            tool.handles.controler= protocolMatcher_Controler(tool,[0 .8 .25 .2]);
            
            %Make the contrast detail image controler object
            tool.handles.controler= protocolMatcher_ContrastDetailImage_Controler(tool,[.25 .8 .25 .2]);
            
            addlistener(tool,'needToUpdateDistance',@tool.handlePropEvents);
            addlistener(tool,'distanceUpdated',@tool.handlePropEvents);
            updateScatterPlot(tool)
            
            
        end
        
        function handlePropEvents(tool,src,evnt,varargin)
            switch evnt.EventName
                case 'needToUpdateDistance'
                    updateDistance(tool)
                case 'distanceUpdated'
                    updateScatterPlot(tool)
            end
        end
        
        function set.criteria(tool,criteria)
            if any(strcmp(tool.possibleCriteria,criteria))
                tool.criteria=criteria;
                notify(tool,'needToUpdateDistance');
            else
                warning(['Chosen criteria not available. Using ''' tool.criteria ''''])
            end
        end
        
        function set.TTFmaterial(tool,material)
            if any(strcmp(tool.possibleMaterials,material))
                tool.TTFmaterial=material;
                notify(tool,'needToUpdateDistance');
            else
                warning(['Chosen material not available. Using ''' tool.TTFmaterial ''''])
            end
        end
        
        function set.NPSStat(tool,NPSstat)
            if any(strcmp(tool.possibleNPSStats,NPSstat))
                tool.NPSStat=NPSstat;
                notify(tool,'needToUpdateDistance');
            else
                warning(['Chosen NPS stat not available. Using ''' tool.NPSStat ''''])
            end
        end
        
        function set.refDose(tool,refDose)
            if refDose>0
                tool.refDose=refDose;
                notify(tool,'refValuesChaged');
            else
                warning('refDose must be > 0');
            end
        end
        
        function set.refSize(tool,refSize)
            if refSize > 0
                tool.refSize=refSize;
                notify(tool,'refValuesChaged');
            else
                warning('refSize must be > 0')
            end
        end
        
        function set.refSliceThickness(tool,refSliceThickness)
            if refSliceThickness > 0
                tool.refSliceThickness=refSliceThickness;
                notify(tool,'refValuesChaged');
            else
                warning('refSliceThickness must be > 0')
            end
        end
        
        function set.DRange(tool,DRange)
            if diff(DRange)>0 && DRange(1)>0
                tool.DRange=DRange;
                notify(tool,'needToUpdateDistance');
            end
        end
        
        function set.ND(tool,ND)
            if ND>0
                if ceil(ND) == floor(ND) %Integer value
                    tool.ND=ND;
                else
                    warning('ND must be integer, using ceil(ND)');
                    tool.ND=ceil(ND);
                end
                notify(tool,'needToUpdateDistance');
            else
                warning('ND must be greater than zero');
            end
        end
        
        function m = get.TTFMetric(tool)
            m=tool.tab{:,['f50_' tool.TTFmaterial]};  
        end
        
        function m = get.NPSMetric(tool)
            m=tool.tab{:,['f' tool.NPSStat]};  
        end
        
        function TTF = getTTFs(tool,rows)
            varname = ['TTF_' tool.TTFmaterial];
            for i=1:length(rows)
                t = load([tool.dataDir tool.tab.fname{rows(i)}],varname);
                TTF(i,1)=t.(varname);
            end
            
        end
        
        function NPS = getNPSs(tool,rows)
            varname = 'NPS';
            for i=1:length(rows)
                t = load([tool.dataDir tool.tab.fname{rows(i)}],varname);
                NPS(i,1)=t.(varname);
            end
        end
        
        function updateDistance(tool)
            %This function computes the distance between each condition
            %based on the current distance criteria. Also sets the distance
            %property of the tool (which triggers any listeners of the
            %distanceUpdated event).
            
            switch tool.criteria
                case 'Noise Texture and Resolution'
                    %This metric is the euclidean distance between the TTF
                    %based f50 and NPS stat
                    [I,J] = getIndexGrids(size(tool.tab,1));
                    tool.distance = sqrt( (tool.TTFMetric(I)-tool.TTFMetric(J)).^2 + (tool.NPSMetric(I)-tool.NPSMetric(J)).^2 );
                otherwise
                    warning('Could not find this criteria, distance was not updated');
                    
            end
            
            notify(tool,'distanceUpdated');
            
        end
        
        function updateScatterPlot(tool)
            set(tool.handles.XYScatter,'XData',tool.TTFMetric,'YData',tool.NPSMetric);
            xlabel(tool.handles.TTFNPSAxis,['TTF f50-' tool.TTFmaterial ' [1/mm]'])
            ylabel(tool.handles.TTFNPSAxis,['NPS f' tool.NPSStat ' [1/mm]'])
            xlim(tool.handles.TTFNPSAxis,[tool.lists(1).TTFMetric-tool.scatterbuff tool.lists(1).TTFMetric+tool.scatterbuff]);
            %xlim(tool.handles.TTFNPSAxis,[0 max(tool.TTFMetric)+.1]);
            ylim(tool.handles.TTFNPSAxis,[tool.lists(1).NPSMetric-tool.scatterbuff tool.lists(1).NPSMetric+tool.scatterbuff]);
        end
        
        function set.distance(tool,distance)
            tool.distance=distance;
            notify(tool,'distanceUpdated');
        end
        
        function str = get.criteriaDescription(tool)
            %Need to fill this in
            str='';
        end
        
        function noise = noiseAtGivenDose(tool,dose,varargin)
            %this returns the noise you would expect at a given dose
            %Dose can be a scalar or vector (if a vector, it must be true
            %that numel(dose) == size(tool.tab,1);
            %Assumes that noise = alpha*dose^b (if beta = -.5, then you
            %have the classic noise dose relationship)
            if nargin>2
                rows=varargin{1};
            else
                rows = 1:size(tool.tab,1);
            end
                
            noise = (tool.tab.alpha(rows).*dose.^tool.tab.beta(rows))...
                .*(tool.tab.alphaWED(rows).*tool.refSize.^tool.tab.betaWED(rows))...
                .*(tool.tab.alphaSliceThickness(rows).*tool.refSliceThickness.^tool.tab.betaSliceThickness(rows));
            
        end
        
        function dose = doseAtGivenNoise(tool,noise,varargin)
            %This is the inverse of noiseAtGivenDose.
            if nargin>2
                rows=varargin{1};
            else
                rows = 1:size(tool.tab,1);
            end
            
            dose = (... 
                    noise./... numerator
                    (... denominator
                    tool.tab.alpha(rows)...
                    .*(tool.tab.alphaWED(rows).*tool.refSize.^tool.tab.betaWED(rows))...
                    .*(tool.tab.alphaSliceThickness(rows).*tool.refSliceThickness.^tool.tab.betaSliceThickness(rows))...
                    )...
                    )...
                    .^(1./tool.tab.beta(rows)); %1/b
            
            
        end
        
        function doses = get.doses(tool)
            doses = linspace(tool.DRange(1),tool.DRange(2),tool.ND);
        end
        
        function delete(tool)
            try
                delete(tool.handles.fig);
                delete(tool.lists);
                delete(tool.CDim);
                delete(tool.handles.controler);
            end
        end
        
        function res = get.statsSummaryTable(tool)
            Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            for i=1:length(tool.lists)
                res(i).model = tool.lists(i).model;
                res(i).reconLabel = tool.tab.ReconLabel{tool.lists(i).row};
                res(i).label = tool.lists(i).label;
                %res(i).label = ['Scanner ' Alphabet(i)];
                res(i).(['f_' tool.NPSStat]) = tool.lists(i).NPSMetric;
                res(i).(['f50_' tool.TTFmaterial]) = tool.lists(i).TTFMetric;
                res(i).dose = tool.lists(i).dose;
                res(i).noise = tool.lists(i).noise;
                res(i).isReference = tool.lists(i).isReference;
                res(i).NPShandle = tool.lists(i).handles.NPSplot;
                res(i).TTFhandle = tool.lists(i).handles.TTFplot;
                res(i).ImageHandle = tool.lists(i).handles.im;
                res(i).ImageAxesHandle = tool.handles.imAxis(tool.lists(i).n);
                res(i).ScatterHandle = tool.lists(i).handles.scatter;
            end
            res = struct2table(res);
        end
        
        function varargout = makeStatsSummaryFigure(tool,varargin)
            %Settings
            xSize  = 6.5;
            ySize = 9;
            flim = [0 1.2];
            %reorder = [3 2 1 4 5 6]; %Used to re-order the list of scanner to match the protocol review report
            reorder = 1:6;
            colors = {'r','y','g','c','b','m'};
            
            %Parse inputs
            switch nargin
                case 1
                    fname = '';
                case 2
                    fname = varargin{1};
            end
            
            %Create the figure
            fig = createFigForPrinting(xSize,ySize);
            
            %get the stats table
            stats = tool.statsSummaryTable;
            stats = stats(reorder,:);
            
            %Scatter plot
            s=subplot(3,1,3);
            h=scatter(tool.TTFMetric,tool.NPSMetric,'o'); hold on;
            set(h,'MarkerEdgeColor',[.5 .5 .5]);
            %copy scatter objects from list
            for i=1:size(stats,1)
                c(i) = copyobj(stats.ScatterHandle(i),s);
                c(i).MarkerFaceColor=colors{i};
                c(i).SizeData = 50;
            end
            axis equal
            xlabel({'Resolution (blurry -> sharp)',['TTF f50-' tool.TTFmaterial ' [1/mm]']})
            ylabel({'Noise Texture (coarse -> fine)',['NPS f' tool.NPSStat ' [1/mm]']})
            title('Noise texture vs resolution')
            grid on
            
            %TTF plot
            s=subplot(3,1,1);
            clear c
            for i=1:size(stats,1)
                c(i) = copyobj(stats.TTFhandle(i),s);
                c(i).Color = colors{i};
            end
            set(s,'Xlim',flim)
            xlabel('Spatial Frequency [1/mm]'); ylabel('TTF'); grid on;
            title('Resolution');
            legend(c,stats.label);
            legend boxoff
            
            %NPS plot
            s=subplot(3,1,2);
            clear c
            for i=1:size(stats,1)
                c(i) = copyobj(stats.NPShandle(i),s);
                c(i).Color = colors{i};
            end
            set(s,'Xlim',flim,'YTickLabel','')
            xlabel('Spatial Frequency [1/mm]'); ylabel('NPS [mm^2HU^2]'); grid on;
            title('Noise Texture')
            
            
            if ~isempty(fname)
                print(fig, '-r300', '-dpng',fname);
            end
            
            switch nargout
                case 0
                    %Close the figure;
                    close(fig);
                case 1
                    varargout{1} = fig;
            end
            
        end
        
        function varargout = makeContrastDetailFigure(tool,varargin)
            %Settings
            xSize  = 12;
            ySize = 12;
            flim = [0 1.2];
            %reorder = [3 2 1 4 5 6]; %Used to re-order the list of scanner to match the protocol review report
            reorder = 1:6;
            colors = {'r','y','g','c','b','m'};
            
            %Parse inputs
            switch nargin
                case 1
                    fname = '';
                case 2
                    fname = varargin{1};
            end
            
            %Create the figure
            fig = createFigForPrinting(xSize,ySize);
            
            %get the stats table
            stats = tool.statsSummaryTable;
            stats = stats(reorder,:);
            
            %Create the text object
            subplot(3,3,1)
            t = tool.CDim.summaryText;
            t{end+1,1} = ['Noise [HU]: ' sprintf('%.1f',tool.lists(1).noise)];
            text(0,1,t,'HorizontalAlignment','Left','VerticalAlignment','top');
            axis off
            
            %Create the template image
            s=subplot(3,3,2);
            L = mean(tool.CDim.CRange);
            W = 4*diff(tool.CDim.CRange);
            imshow(mat2gray(tool.CDim.im,[L-W/2 L+W/2]));
            title('Template');
            axis on
            set(s,'XTick',tool.CDim.ticks,'YTick',tool.CDim.ticks,'YTicklabel',tool.CDim.Clabels,'XTicklabel',tool.CDim.Slabels,'XTickLabelRotation',45)
            
            %Get the relative dose
            RD = 100*(stats.dose-stats.dose(1))./stats.dose(1);
            
            %Create the images
            for i=1:size(stats,1)
                s=subplot(3,3,i+3);
                imshow(stats.ImageHandle(i).CData);
                %title({stats.model{i},stats.reconLabel{i}})
                title({stats.label{i},['Dose Required: ' sprintf('%+.1f',RD(i)) '%']});
            end
            
            %Create the annotation
            str = ['Contrast-detail images: Noise = ' sprintf('%.1f',tool.lists(1).noise) ' HU, ', 'Window Width = ' num2str(W) ' HU, ', 'Window Level = ' num2str(L) ' HU'];
            annotation('textbox',[0 0 1 1],'String',str,'HorizontalAlignment','Center',...
                'VerticalAlignment','top','EdgeColor','none','FontSize',16,...
                'FontWeight','Bold');
            
            if ~isempty(fname)
                print(fig, '-r300', '-dpng',fname);
            end
            
            switch nargout
                case 0
                    %Close the figure;
                    close(fig);
                case 1
                    varargout{1} = fig;
            end
        end
        
    end
    
end

function panelResizeFunction(hObject,events,tool,buff)


switch hObject
    case tool.handles.fig
end


end

function [I,J] = getIndexGrids(N)
[J,I]=meshgrid(1:N,1:N);
end
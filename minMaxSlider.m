% Copyright 20xx - 2019. Duke University
classdef minMaxSlider < handle
    
    properties
        handles
        position % position of the panel containing the slider [xmin ymin xsize ysize];
        units = 'Normalized';
        buff = 30;
        globalMin = 1;
        globalMax = 10;
        selectedMin = 1;
        selectedMax = 10;
        currentSlice = 5;
        integerOnly = true;
        enable = true;
        visible = true;
        label = '';
    end
    
    methods
        function slider = minMaxSlider(parent,position)
            
            
            slider.handles.parent = parent;
            slider.handles.fig = getParentFigure(slider.handles.parent);
            
            
            %make the axes
            slider.handles.axes = axes('Parent',slider.handles.parent,'Units',slider.units,'Position',position);
            set(slider.handles.axes,'Color','k','XColor','w','YColor','w','XLim',[0 1],'XTick',0,...
                'Ylim',[slider.globalMin slider.globalMax],'YAxisLocation','origin','XColor','none');
            hold on;
            ylabel(slider.label);
           
            
            %make the min, max, and currentSlice handles
            slider.handles.currentSliceHandle = plot([0 .5],[slider.currentSlice slider.currentSlice],'-w');
            
            slider.handles.minHandle = plot([0 .25 .5],[slider.selectedMin slider.selectedMin slider.selectedMin],'-sr','MarkerFaceColor','r');
            slider.handles.minHandleText = text(.5,slider.selectedMin,[' Min: ' num2str(slider.selectedMin)],'Color','w','HorizontalAlignment','left');
            fun = @(hObject,evnt) ButtonDownFunction(hObject,evnt,slider,'Min'); set(slider.handles.minHandle,'ButtonDownFcn',fun);
            
            slider.handles.maxHandle = plot([0 .25 .5],[slider.selectedMax slider.selectedMax slider.selectedMax],'-sr','MarkerFaceColor','r'); 
            slider.handles.maxHandleText = text(.5,slider.selectedMax,[' Max: ' num2str(slider.selectedMax)],'Color','w','HorizontalAlignment','left');
            fun = @(hObject,evnt) ButtonDownFunction(hObject,evnt,slider,'Max'); set(slider.handles.maxHandle,'ButtonDownFcn',fun);
            
            %make the text edit boxes
            pos = getPixelPosition(slider.handles.axes);
            fun=@(hObject,evnt) callbacks(hObject,evnt,slider);
            slider.handles.minEditBox = uicontrol(slider.handles.parent,'Style','Edit','Units','Pixels',...
                'Position',[pos(1)-1.5*slider.buff pos(2) slider.buff slider.buff],'ForegroundColor','w','BackgroundColor','k',...
                'string',num2str(slider.selectedMin),'Callback',fun);
            slider.handles.maxEditBox = uicontrol(slider.handles.parent,'Style','Edit','Units','Pixels',...
                'Position',[pos(1)-1.5*slider.buff pos(2)+pos(4)-slider.buff slider.buff slider.buff],'ForegroundColor','w','BackgroundColor','k',...
                'string',num2str(slider.selectedMax),'Callback',fun);
            
            %set the position 
            slider.position = position;
            
            
        end
        
        function setRange(slider,range)
            range = sort(range); %Make sure the first element is lowest;
            
            %Set the slider to the extremes
            slider.selectedMin=slider.globalMin;
            slider.selectedMax=slider.globalMax;
            
            %Set the slider according to desired range
            slider.selectedMin = range(1);
            slider.selectedMax = range(2);
            
            
            
        end
        
        function set.selectedMin(slider,selectedMin)
            if selectedMin >= slider.globalMin && selectedMin <= slider.selectedMax
                slider.selectedMin = selectedMin;
                set(slider.handles.minHandle,'YData',[selectedMin selectedMin selectedMin])
            end
            if slider.integerOnly
                t = [' Min: ' num2str(round(slider.selectedMin))];
                t2=num2str(round(slider.selectedMin));
            else
                t = [' Max: ' sprintf('%3.1f',slider.selectedMin)];
                t2 = sprintf('%3.1f',slider.selectedMin);
            end
            set(slider.handles.minHandleText,'Position',[.5 slider.selectedMin],'String',t);
            set(slider.handles.minEditBox,'String',t2);
            
            
        end
        
        function set.selectedMax(slider,selectedMax)
            if selectedMax <= slider.globalMax && selectedMax >= slider.selectedMin
                slider.selectedMax = selectedMax;
                set(slider.handles.maxHandle,'YData',[selectedMax selectedMax selectedMax])
            end
            if slider.integerOnly
                t = [' Max: ' num2str(round(slider.selectedMax))];
                t2=num2str(round(slider.selectedMax));
            else
                t = [' Max: ' sprintf('%3.1f',slider.selectedMax)];
                t2 = sprintf('%3.1f',slider.selectedMax);
            end
            set(slider.handles.maxHandleText,'Position',[.5 slider.selectedMax],'String',t);
            set(slider.handles.maxEditBox,'String',t2);
            
            
        end
        
        function set.integerOnly(slider,integerOnly)
            integerOnly = logical(integerOnly);
            slider.integerOnly = integerOnly;
            slider.selectedMin = slider.selectedMin;
            slider.selectedMax = slider.selectedMax;
        end
        
        function set.currentSlice(slider,currentSlice)
            if currentSlice <= slider.globalMax && currentSlice >= slider.globalMin
                slider.currentSlice = currentSlice;
                set(slider.handles.currentSliceHandle,'YData',[currentSlice currentSlice]);
            end
        end
        
        function set.globalMin(slider,globalMin)
            slider.globalMin = globalMin;
            
            if slider.selectedMin < globalMin
                slider.selectedMin = globalMin;
            end
            if slider.selectedMax < globalMin
                slider.selectedMax = globalMin;
            end
            if slider.currentSlice < globalMin
                slider.currentSlice = globalMin;
            end
            
            set(slider.handles.axes,'YLim',[slider.globalMin slider.globalMax]);
        end
        
        function set.globalMax(slider,globalMax)
            slider.globalMax = globalMax;
            
            if slider.selectedMin > globalMax
                slider.selectedMin = globalMax;
            end
            if slider.selectedMax > globalMax
                slider.selectedMax = globalMax;
            end
            if slider.currentSlice > globalMax
                slider.currentSlice = globalMax;
            end
            
            set(slider.handles.axes,'YLim',[slider.globalMin slider.globalMax]);
            
        end
        
        function set.position(slider,position)
            slider.position = position;
            set(slider.handles.axes,'Position',position);
            pos = getPixelPosition(slider.handles.axes);
            set(slider.handles.minEditBox,'Position',[pos(1)-1.5*slider.buff pos(2) slider.buff slider.buff]);
            set(slider.handles.maxEditBox,'Position',[pos(1)-1.5*slider.buff pos(2)+pos(4)-slider.buff slider.buff slider.buff]);
            
        end
        
        function set.units(slider,units)
            slider.units = units;
            set(slider.handles.axes,'Units',units);
        end
        
        function set.enable(slider,enable)
            enable = logical(enable);
            slider.enable = enable;
            
            if enable
                col = 'w';
                col2 = 'r';
            else
                col = [.5 .5 .5];
                col2 = [.5 0 0 ];
                
            end
            set(slider.handles.axes,'YColor',col);
            set(slider.handles.minHandleText,'Color',col);
            set(slider.handles.maxHandleText,'Color',col);
            set(slider.handles.currentSliceHandle,'Color',col);
            set(slider.handles.minHandle,'Color',col2,'MarkerFaceColor',col2,'MarkerEdgeColor',col2);
            set(slider.handles.maxHandle,'Color',col2,'MarkerFaceColor',col2,'MarkerEdgeColor',col2);
            
        end
        
        function set.visible(slider,visible)
            visible = logical(visible);
            slider.visible = visible;
            if visible
                vis = 'on';
            else
                vis = 'off';
            end
            set(slider.handles.axes,'Visible',vis);
            set(slider.handles.minHandleText,'Visible',vis);
            set(slider.handles.maxHandleText,'Visible',vis);
            set(slider.handles.minHandle,'Visible',vis);
            set(slider.handles.maxHandle,'Visible',vis);
            set(slider.handles.currentSliceHandle,'Visible',vis);
        end
        
        function set.label(slider,label)
            ylabel(slider.handles.axes,label);
        end
        
        function delete(slider)
            delete(slider.handles.axes);
        end
        
    end
    
    
end

function callbacks(hObject,evnt,slider)
%This runs when the edit box is changed
new = str2num(get(hObject,'String'));
if isreal(new) && ~isempty(new)
    switch hObject
        case slider.handles.minEditBox
            slider.selectedMin=new;
        case slider.handles.maxEditBox
            slider.selectedMax=new;
    end
else
    switch hObject
        case slider.handles.minEditBox
            slider.selectedMin=slider.selectedMin;
        case slider.handles.maxEditBox
            slider.selectedMax=slider.selectedMax;
    end
end


end

function ButtonDownFunction(hObject,evnt,slider,label)
if slider.enable
    %get the parent figure handle
    fig = slider.handles.fig;
    
    %get the current button motion and button up functions of the figure
    WBMF_old = get(fig,'WindowButtonMotionFcn');
    WBUF_old = get(fig,'WindowButtonUpFcn');
    
    %set the new window button motion function and button up function of the figure
    fun = @(src,evnt) ButtonMotionFunction(src,evnt,slider,label);
    fun2=@(src,evnt)  ButtonUpFunction(src,evnt,slider,WBMF_old,WBUF_old);
    set(fig,'WindowButtonMotionFcn',fun,'WindowButtonUpFcn',fun2);
end

end

function ButtonUpFunction(src,evnt,slider,WBMF_old,WBUF_old)
fig = slider.handles.fig;
set(fig,'WindowButtonMotionFcn',WBMF_old,'WindowButtonUpFcn',WBUF_old);
end

function ButtonMotionFunction(src,evnt,slider,label)
cp = get(slider.handles.axes,'CurrentPoint'); cp=cp(1,2);

switch label
    case 'Min'
        slider.selectedMin = cp;
    case 'Max'
        slider.selectedMax = cp;
end


end

function fig = getParentFigure(fig)
% if the object is a figure or figure descendent, return the
% figure. Otherwise return [].
while ~isempty(fig) & ~strcmp('figure', get(fig,'type'))
  fig = get(fig,'parent');
end
end

function pos = getPixelPosition(h)
oldUnits = get(h,'Units');
set(h,'Units','Pixels');
pos = get(h,'Position');
set(h,'Units',oldUnits);
end
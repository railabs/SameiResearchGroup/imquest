% Copyright 20xx - 2019. Duke University
classdef protocolMatcher_ContrastDetailImage < handle
    properties
        profileType = 'Designer';
        n = 1;
        psize = 220/512;
        Npix = 30;
        taskFunction
        CRange = [5 25]; %Range of contrasts
        SRange = [3 7]; %Range of diameters
        N = 10; %Number of contrasts/sizes
        im
    end
    
    properties (Dependent = true)
        Npix_total
        possibleProfileTypes
        summaryText
        contrasts %Vector of contrast values
        sizes %Vector of size values
        ticks
        Clabels
        Slabels
    end
    
    events
        needToUpdateImage
        ImageUpdated
    end
    
    methods
        function obj = protocolMatcher_ContrastDetailImage(varargin)
            
            obj.taskFunction = imquest_TaskFunction;
            obj.taskFunction.profileType=obj.profileType;
            obj.taskFunction.n=obj.n;
            obj.taskFunction.psize=obj.psize;
            obj.taskFunction.N=obj.Npix;
            updateImage(obj);
            addlistener(obj,'needToUpdateImage',@obj.handlePropEvents);
            
        end
        
        function  handlePropEvents(obj,src,evnt,varargin)
            switch evnt.EventName
                case 'needToUpdateImage'
                    updateImage(obj)
            end
        end
        
        function updateImage(obj)
            
            %Initialize the image
            im = zeros(obj.Npix_total);
            N = obj.Npix;
            
            %Get contrasts
            Cs = obj.contrasts;
            Ss = obj.sizes;
            
            %Create contrast grid
            for i=1:obj.N
                for j=1:obj.N
                    %Set the contrast and diameter of the task function
                    %object
                    obj.taskFunction.Contrast=Cs(i);
                    obj.taskFunction.diameter=Ss(j);
                    
                    im((i-1)*N+1:i*N,(j-1)*N+1:j*N) = obj.taskFunction.im;
                    
                end
            end
            
            obj.im=im;
            
            notify(obj,'ImageUpdated');
            
        end
        
        function Cs = get.contrasts(obj)
            Cs = linspace(obj.CRange(1),obj.CRange(2),obj.N);
        end
        
        function Ss = get.sizes(obj)
            Ss = linspace(obj.SRange(1),obj.SRange(2),obj.N);
        end
        
        function N = get.Npix_total(obj)
            N = obj.N*obj.Npix;
        end
        
        function set.profileType(obj,profileType)
            obj.taskFunction.profileType=profileType;
            obj.profileType=obj.taskFunction.profileType;
            notify(obj,'needToUpdateImage');
        end
        
        function set.n(obj,n)
            obj.taskFunction.n=n;
            obj.n=n;
            notify(obj,'needToUpdateImage');
        end
        
        function set.psize(obj,psize)
            obj.taskFunction.psize=psize;
            obj.psize=psize;
            notify(obj,'needToUpdateImage');
        end
        
        function set.Npix(obj,Npix)
            obj.taskFunction.N=Npix;
            obj.Npix=Npix;
            notify(obj,'needToUpdateImage');
        end
        
        function set.CRange(obj,CRange)
            obj.CRange=CRange;
            notify(obj,'needToUpdateImage');
        end
        
        function set.SRange(obj,SRange)
            obj.SRange=SRange;
            notify(obj,'needToUpdateImage');
        end
        
        function set.N(obj,N)
            obj.N=N;
            notify(obj,'needToUpdateImage');
        end
        
        function im = getBlurredNoisyImage(obj,TTF,NPS)
            
            %Blur the image
            im = blurWithTTF(TTF,obj.im,obj.psize);
            
            %Add noise
            im = im + getCorrelatedNoise(NPS,obj.psize,size(im));
            
        end
        
        function types = get.possibleProfileTypes(obj)
            types = obj.taskFunction.possibleProfileTypes;
        end
        
        function t = get.summaryText(obj)
            t={};
            t{end+1,1} = ['Profile Type: ' obj.profileType];
            t{end+1,1} = ['Exponent: ' num2str(obj.n)];
            t{end+1,1} = ['Pixel size [mm]: ' sprintf('%.2f',obj.psize)];
            t{end+1,1} = ['N pixels per signal: ' num2str(obj.Npix) 'x' num2str(obj.Npix)];
            t{end+1,1} = ['N total pixels: ' num2str(obj.Npix_total) 'x' num2str(obj.Npix_total)];
            t{end+1,1} = ['Contrast Range [HU]: [' num2str(obj.CRange(1)) ' ' num2str(obj.CRange(2)) ']'];
            t{end+1,1} = ['Size Range [mm]: [' num2str(obj.SRange(1)) ' ' num2str(obj.SRange(2)) ']'];
            t{end+1,1} = ['N signals: ' num2str(obj.N) 'x' num2str(obj.N)];
            
            
        end
        
        function ticks = get.ticks(obj)
            
            ticks = (0:obj.N-1)*obj.Npix + obj.Npix/2;
            
        end
        
        function lab = get.Clabels(obj)
            vals=obj.contrasts;
            for i=1:length(vals)
                lab{i,1} = ['C = ' sprintf('%.1f',vals(i)) ' HU'];
            end
            
        end
        
        function lab = get.Slabels(obj)
            vals=obj.sizes;
            for i=1:length(vals)
                lab{i,1} = ['S = ' sprintf('%.1f',vals(i)) ' mm'];
            end
        end
        
    end
end
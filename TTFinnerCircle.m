% Copyright 20xx - 2019. Duke University
classdef TTFinnerCircle < handle
    
    properties
        ROI %handle to the parent ROI
        R = .5;  %Ratio of inner circle size compared to outer circle size
        handle
        positionListener
        deleteListener
        visibleListener
    end
    
    events
    end
    
    methods
        function circle = TTFinnerCircle(ROI)
            
            circle.ROI = ROI;
            R = circle.R;
            %make the circle 
            position = getPosition(ROI);
            position(3) = position(3)*R;
            position(4) = position(4)*R;
            pos = [position(1)-position(3)/2 position(2)-position(4)/2 position(3) position(4)];
            circle.handle = rectangle('Position',pos,'Parent',ROI.axesHandle,'EdgeColor',ROI.lineColor,'LineWidth',1.5,'Curvature',[1 1],'LineStyle','--');
            
            %add listeners
            circle.positionListener = addlistener(ROI,'newROIPosition',@circle.handlePropEvents);
            circle.deleteListener=addlistener(ROI,'ROIdeleted',@circle.handlePropEvents);
            circle.visibleListener = addlistener(ROI,'visible','PostSet',@circle.handlePropEvents);
            
            
        end
        
        function pos = getInnerPosition(circle)
            position = getPosition(circle.ROI);
            R = circle.R;
            position(3) = position(3)*R;
            position(4) = position(4)*R;
            pos = [position(1)-position(3)/2 position(2)-position(4)/2 position(3) position(4)];
        end
        
        function handlePropEvents(circle,src,evnt,varargin)
            switch evnt.EventName
                case 'newROIPosition'
                   
                    pos = getInnerPosition(circle);
                    set(circle.handle,'Position',pos);
                case 'ROIdeleted'
                    delete(circle)
                case 'PostSet'
                    if circle.ROI.visible
                        val = 'on';
                    else
                        val = 'off';
                    end
                    set(circle.handle,'Visible',val)
            end
        end
        
        function set.R(slider,R)
            if R>0 && R<=1
                slider.R = R;
                notify(slider.ROI,'newROIPosition');
            end
            
        end
        
        function delete(circle)
            delete(circle.handle);
            delete(circle.positionListener);
            delete(circle.deleteListener);
            delete(circle.visibleListener);
        end
    end
    
end
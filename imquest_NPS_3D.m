% Copyright 20xx - 2019. Duke University
classdef imquest_NPS_3D < imquest_NPS
    
    
    methods
        function NPShandle = imquest_NPS_3D(varargin)
            switch nargin
                case 1 %NPShandle = imquest_NPS(NPS) This makes a copy of the NPS without grapihcs
                    NPS=varargin{1};
                    NPShandle.withGraphics=false;
                    NPShandle.f=NPS.f;
                    NPShandle.NPS=NPS.NPS;
                    NPShandle.noise=NPS.noise;
                    NPShandle.graphicHandle=[];
                    NPShandle.tool=[];
                    NPShandle.settings=NPS.settings;
                    NPShandle.stats=NPS.stats;
                    NPShandle.slices=NPS.slices;
                    NPShandle.psize=NPS.psize;
                    NPShandle.sliceThickness=NPS.sliceThickness;
                    NPShandle.SeriesInstanceUID=NPS.SeriesInstanceUID;
                    NPShandle.versionInfoTable=NPS.versionInfoTable;
                case 2 %NPShandle = imquest_NPS(tool,NPSROIs)
                    tool=varargin{1};
                    NPSROIs=varargin{2};
                    
                    %set the NPShandle tool property
                    NPShandle.tool=tool;
                    
                    %Get the slices of interest
                    [im, NPShandle.slices] = getImageSlices(tool,tool.handles.NPSsliceSelector);
                    
                    %Get the DICOM series UID
                    NPShandle.SeriesInstanceUID = tool.DICOMheaders(NPShandle.slices(1)).SeriesInstanceUID;
                    
                    %Get the pixel size
                    psize = tool.CTpsize;
                    NPShandle.psize=psize;
                    NPShandle.sliceThickness=tool.CTsliceThickness;
                    
                    %Get central slice
                    slice = ceil(size(im,3)/2);
                    
                    %set up the ROIs
                    for i=1:length(NPSROIs)
                        pos = getPosition(NPSROIs(i));
                        pos(1) = pos(1);
                        pos(2) = pos(2);
                        ROIs(i,:) = [round(pos(1:2)) slice round(pos(3:4)) size(im,3)];
                    end
                    psize = [psize' tool.CTsliceThickness];
                    
                    %measure the NPS
                    [NPShandle.f, NPShandle.NPS,NPShandle.noise,NPShandle.settings,NPShandle.stats] = get3DNPS(im,ROIs,psize);
                    
                    NPShandle.withGraphics = false;
                    NPShandle.graphicHandle=[];
                    
                    %Get the imquest version info
                    NPShandle.versionInfoTable=tool.versionInfoTable;
            end
            
            
            
        end
        
        function newNPS=copy(NPS)
            newNPS = imquest_NPS_3D(NPS);
        end
        
        function NPSup = getResampled3DNPS(NPS,psize,N)
            
            %Create coordinate system
            fx = getFFTfrequency(psize(1),N(1));
            fy = getFFTfrequency(psize(2),N(2));
            fz = getFFTfrequency(psize(3),N(3));
            [Fx,Fy,Fz]=meshgrid(fx,fy,fz);
            dfx = fx(2)-fx(1);
            dfy = fy(2)-fy(1);
            dfz = fz(2)-fz(1);
            
            %Interpolate
            NPSup = interp3(NPS.f{1},NPS.f{2},NPS.f{3},NPS.NPS,Fx,Fy,Fz,'linear',0);
            
            %Scale the NPS as needed to maintain the noise variance from
            %the original NPS
            NPSup = NPSup*(NPS.noise^2)/(sum(NPSup(:))*dfx*dfy*dfz);
            
        end
        
        function noise = getCorrelatedNoise(NPS,psize,N)
            
            %Get the sqrt of the resampled NPS
            NPSup = sqrt(getResampled3DNPS(NPS,psize,N));
            
            %Generate white noise image
            N=[N(2) N(1) N(3)];
            im = random('norm',0,1,N);
            
            %Filter with NPS
            MV=mean(im(:)); %get mean value
            im=im-MV; %remove mean
            F=fftn(im); %Fourier Transform
            F(1)=0;     %Remove DC component (same as filtering with a notch filter)
            NPSup=fftshift(NPSup); %Shift MTF
            H=F.*NPSup; %Apply filtering
            noise = real(ifftn(H))+MV; %Inverse fourier transform
            
            %Scale to get the right STD
            noise = noise*NPS.noise/std(noise(:));
            
        end
        
    end
    
end
clear; close all;
input = 'path/to/your/imagesSeries/'; %Change this path for your machine
output = 'path/to/your/results/'; %Change this path for your machine
tool = imquest; %open up instance of imquest and save handle
list = dir([input 'Test*']); %Get all the folders that start with Test
%Make output directory if needed
if ~isdir(output)
    mkdir(output)
end
for i=1:length(list) %loop over image series
    disp(['Processing case ' num2str(i) '/' num2str(length(list)) ':'])
    disp(list(i).name);
    try
        %Load the images
        disp('Loading images')
        tool.loadCTdata([input list(i).name])
        
        %Run Mercury Phantom 4.0 analysis
        disp('Making measurements')
        res = imquest_MercuryPhantomAutoAnalyze(tool,'4.0');
        
        %Generate report
        disp('Generating report')
        res.generateReport([output list(i).name '.pdf'])
        
        %Save results to .mat file
        disp('Saving results')
        save([output list(i).name '.mat'],'res')
    catch ME
        disp('Error with this case')
        disp(ME)
    end
end


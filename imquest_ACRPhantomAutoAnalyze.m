% Copyright 20xx - 2019. Duke University
classdef imquest_ACRPhantomAutoAnalyze < handle
    properties
        registrationMethod = 'Quick'; %'Full', 'Quick', or 'None'.
        zOffset = 0;
    end
    
    properties (SetAccess = immutable)
        
        %Meta data about the CT series
        CTSeriesSummary
        psize
        SliceThickness
        
        %HU measurements
        HU_Water
        HU_Poly
        HU_Bone
        HU_Air
        HU_Acrylic
        
        %TTFs
        TTF_Poly
        TTF_Bone
        TTF_Air
        TTF_Acrylic
        
        %CNR from low contrast resolution section (module 2)
        LCR_Signal
        LCR_Background
        LCR_Noise
        
        %NPS
        NPS
        
        %Uniformity
        HU_Center
        HU_12
        HU_3
        HU_6
        HU_9
        STD_Center
        STD_12
        STD_3
        STD_6
        STD_9
        
        %Detectability
        dPrime_Poly
        dPrime_Bone
        dPrime_Air
        dPrime_Acrylic
        
        %Registration
        tform           %This is the transformation from the template phantom to the actual images
        
        
    end
    
    properties (Transient = true)
        tool        %handle to the imquest object
    end
    
    properties (SetAccess = immutable, Hidden = true)
        x   %World coordinates
        y
        z
        S   %3D size of image (in voxels)
        DICOMheaders
        
        Pos_TTF_ROI_Poly_Pix;        %Position of the ROI AFTER the TTF is measured (i.e., after the center of the ROI is found)
        Pos_TTF_ROI_Bone_Pix;
        Pos_TTF_ROI_Air_Pix
        Pos_TTF_ROI_Acrylic_Pix
        
        %Images for report generation
        im_HU_ROIs
        im_TTF_ROIs
        im_LCR_ROIs
        im_Uniformity_ROIs
        im_NPS_ROIs
        im_TTFs
        im_NPS
        im_HCR
        im_Dprime
    end
    
    properties (Constant = true, Hidden = true)
        PhantomDiameter         =   200;                %Diameter of the phantom in mm
        ModuleCenters           =   [0 40 80 120];      %z-location for center of each module (mm) Used when generating ROI images
        T_size                  =   -500;               %Threshold used to segment the phantom from the background
        
        Module1SliceRange       =   [-15     15 ];      %Slice locations (in template coordinates) that will be used for module 1 measurements
        HUmaterials             =   {'Water','Poly','Bone','Air','Acrylic'};
        TTFmaterials            =   {'Poly','Bone','Air','Acrylic'};
        HU_ROI_Area             =   200;                %Size of the ROI in mm^2
        TTF_ROI_Diameter        =   25;                 %size of the inserts used for TTF measurement
        HU_Water_Threshold      =   [-7     7   ];      %Thresholds for passing HU value measurements
        HU_Poly_Threshold       =   [-107   -84 ];
        HU_Bone_Threshold       =   [850    970 ];
        HU_Air_Threshold        =   [-1005  -970];
        HU_Acrylic_Threshold    =   [110     135];
        
        Pos_Water               =   [-62    0   0];      %x-y Position (mm) of the inserts in the phantom template
        Pos_Poly                =  -[62     62  0]./sqrt(2);
        Pos_Bone                =   [62    -62  0]./sqrt(2);
        Pos_Air                 =   [62     62  0]./sqrt(2);
        Pos_Acrylic             =   [-62    62  0]./sqrt(2);
        
        Module2SliceRange       =   [25     55  ];      %Slice locations that will be used for module 2 measurements
        LCR_ROI_Area            =   100;                %Size of LCR ROI in mm^2
        Pos_LCR_Signal          =   [0      -57 40];      %x-y position (mm) of the LCR signal and background
        Pos_NPS                 =   [-4 4 80];
        Module3SliceRange       =   [65     95  ];      %Slice locations that will be used for module 3 measurements
        NPS_ROI_Diameter        =   30;
        Uniformity_ROI_Area     =   .01*pi*(100^2);
        Pos_Uniformity          =   [0  0 80];
        Uniformty_Threshold     =   5;
        UniformityLocations     =   {'Center','12','3','6','9'};
        
        Template_N              =   512; %These are the settings used for creatign the template image of the ACR phantom
        Template_FOV            =   205;
        Template_ST             =   1;
        
        
    end
    
    properties (Dependent = true, Hidden = true)
        PhantomCenter                                   %The phantom center in mm
        PhantomCenter_pix                               %The phantom center in pixel units
        PhantomArea                                     %The area of the phantom
        
        HU_ROI_Diameter                                 %Diameter of the HU ROI measurement in mm
        HU_ROI_Diameter_pix                             %Diameter of the HU ROI measurement in pixels
        HU_ROI_Slices                                   %Slices over which the HU ROIs are measured
        Module1SliceRange_reg                           %Slice locations (in registered image coordinates) that will be used for module 1 measurements
        
        TTF_ROI_Diameter_pix
        
        LCR_ROI_Diameter
        LCR_ROI_Diameter_pix
        LCR_ROI_Slices
        Module2SliceRange_reg                           %Slice locations (in registered image coordinates) that will be used for module 2 measurements
        
        NPS_ROI_Slices
        Pos_NPS_ROI
        NPS_ROI_Diameter_pix
        Uniformity_ROI_Diameter
        Uniformity_ROI_Diameter_pix
        Module3SliceRange_reg                           %Slice locations (in registered image coordinates) that will be used for module 3 measurements
        
        Template_xy                                      %in-plane x-y locations for creating ACR phantom template image
        Template_psize
        Template_z
        Template_S                                       %Size of the template image
        Rtemp       %Spatial referencing object to the template image
    end
    
    properties (Dependent = true)
        
        HU_Water_Pass                                   %Boolean determining if the HU test passed for each material
        HU_Poly_Pass
        HU_Bone_Pass
        HU_Air_Pass
        HU_Acrylic_Pass
        
        LCR_Contrast
        LCR_CNR
        
        NPS_noise
        
        Uniformity                                      %Maximum difference between center and edge ROIs in module 3
        Uniformity_Pass
        
        Rim                                             %Spatial referencing object of the image data
        
        SeriesDescription                               %Gets the series description from dicom header
        resultsSummaryTable                             %Table with Pass fail results for each test
    end
    
    events
    end
    
    methods
        
        %Constructor and general methods-----------------------------------
        %------------------------------------------------------------------
        function res = imquest_ACRPhantomAutoAnalyze(tool,varargin) %Constructor
            switch length(varargin)
                case 1
                    res.registrationMethod = varargin{1};
                case 2
                    res.registrationMethod = varargin{1};
                    res.zOffset = varargin{2};
            end
            
            %Set the tool property
            res.tool=tool;
            
            %Set the z offset
            res.zOffset=tool.ACRZeroOffset;
            
            %Get the currently displayed slice
            oldSlice = getCurrentSlice(tool.handles.imtool);
            [Wold,Lold] = getWindowLevel(tool.handles.imtool);
            
            %First clear all ROIs
            removeTTFROI(tool);
            removeNPSROI(tool);
            
            %Clear all TTFs and NPSs and Dprimes
            removeTTF(tool);
            removeNPS(tool);
            removeDprime(tool);
            
            %Get the meta data
            res.CTSeriesSummary=tool.CTSeriesSummary;
            res.DICOMheaders=tool.DICOMheaders;
            res.SliceThickness = tool.CTsliceThickness;
            res.psize=tool.CTpsize(1);
            res.S=tool.CTImageSize;
            [res.x,res.y,res.z]=getCTcoordinates(res.DICOMheaders);
            res.z = res.z - res.zOffset;
            
            %Registration
            res.tool.status = 'ACR Analysis: Registering images to template';
            res.tform = registerToTemplate(res);
            
            %Get the TTF measurements
            res.tool.status = 'ACR Analysis: Measuring TTF';
            displayCentralSlice(res,1)
            materials = res.TTFmaterials;
            for i=1:length(materials)
                [res.(['TTF_' materials{i}]), res.(['Pos_TTF_ROI_' materials{i} '_Pix'])] = getTTFMeasurement(res,materials{i});
            end
            
            %Get the HU measurements
            res.tool.status = 'ACR Analysis: Measuring HU';
            materials = res.HUmaterials;
            for i=1:length(materials)
                res.(['HU_' materials{i}]) = getHUMeasurement(res,materials{i});
            end
            
            %Get the LCR measurements (module 2)
            displayCentralSlice(res,2)
            res.tool.status = 'ACR Analysis: Measuring Low contrast performance';
            [res.LCR_Signal, res.LCR_Background, res.LCR_Noise] = getLCRMeasurements(res);
            
            %Get the NPS measurement (module 3)
            displayCentralSlice(res,3)
            res.tool.status = 'ACR Analysis: Measuring NPS';
            res.NPS=getNPSMeasurement(res);
            
            %Get Uniformity measurement (module 3)
            res.tool.status = 'ACR Analysis: Measuring Uniformity';
            locs = res.UniformityLocations;
            for i=1:length(locs)
                [res.(['HU_' locs{i}]), res.(['STD_' locs{i}])]=getUniformityMeasurement(res,locs{i});
            end
            
            %Get the dprime measurements
            measureDprime(res.tool.handles.ObserverModelController)
            materials = res.TTFmaterials;
            for i=1:length(materials)
                res.(['dPrime_' materials{i}]) = copy(res.tool.Dprimes(i));
            end
            
            %Get the images (used when generating the report)
            res.tool.status = 'ACR Analysis: Generating report images';
            res.im_HU_ROIs=getHUROIim(res);
            res.im_TTF_ROIs=getTTFROIim(res);
            res.im_TTFs=getTTFim(res);
            res.im_LCR_ROIs=getLCRROIim(res);
            res.im_Uniformity_ROIs=getUniformityROIim(res);
            res.im_NPS_ROIs = getNPSROIim(res);
            res.im_NPS=getNPSim(res);
            res.im_HCR=getHCRim(res);
            res.im_Dprime=getDprimeim(res);
            
            %Return to the slice and WL settings that were on when the
            %analysis started
            setCurrentSlice(res.tool.handles.imtool,oldSlice);
            setWindowLevel(res.tool.handles.imtool,Wold,Lold);
            
            showTempStatus(res.tool,'ACR Analsysis: Done!')
            res.tool.status = res.tool.defaultStatus;
            
        end
        
        function pos = getROIPixelLocation(res,material)
            d=[];
            switch material
                case 'Water'
                    pos=res.Pos_Water;
                case 'Poly'
                    pos=res.Pos_Poly;
                    d = res.Pos_TTF_ROI_Poly_Pix;
                case 'Bone'
                    pos=res.Pos_Bone;
                    d = res.Pos_TTF_ROI_Bone_Pix;
                case 'Air'
                    pos=res.Pos_Air;
                    d = res.Pos_TTF_ROI_Air_Pix;
                case 'Acrylic'
                    pos=res.Pos_Acrylic;
                    d = res.Pos_TTF_ROI_Acrylic_Pix;
                case 'LCR_Signal'
                    pos = res.Pos_LCR_Signal;
                case 'LCR_Background'
                    pos = res.Pos_LCR_Signal;
                    [theta,rho]=cart2pol(pos(1),pos(2));
                    theta = theta - 25*pi/180; %Rotate ROI location by 15 degrees
                    [pos(1), pos(2)] = pol2cart(theta,rho);
                case 'NPS'
                    pos = res.Pos_NPS_ROI;
                case 'Center'
                    pos=res.Pos_Uniformity;
                case '12'
                    pos=res.Pos_Uniformity;
                    pos(2)=pos(2)-res.PhantomDiameter/2+1.5*res.Uniformity_ROI_Diameter;
                case '3'
                    pos=res.Pos_Uniformity;
                    pos(1)=pos(1)+res.PhantomDiameter/2-1.5*res.Uniformity_ROI_Diameter;
                case '6'
                    pos=res.Pos_Uniformity;
                    pos(2)=pos(2)+res.PhantomDiameter/2-1.5*res.Uniformity_ROI_Diameter;
                case '9'
                    pos=res.Pos_Uniformity;
                    pos(1)=pos(1)-res.PhantomDiameter/2+1.5*res.Uniformity_ROI_Diameter;
                        
            end
            
            %Apply transformation from registration
            [x,y,z] = transformPointsForward(res.tform,pos(:,1),pos(:,2),pos(:,3));
            pos =[x y];
            
            %convert position to pixel coordinates
            pos = worldPos2PixelPos(res,pos);
            
            %For HU rods, if the TTF has already been measured, you can use
            %the output ROI location
            if ~isempty(d)
                pos = d(1:2);
            end
            
        end
        
        function slices = getSlicesNumbers(res,range)
            slices = find(res.z>=range(1) & res.z<=range(2));
        end
        
        function pos = get.PhantomCenter(res)
            pos = pixelPos2WorldPos(res,res.PhantomCenter_pix);
        end
        
        function pos = get.PhantomCenter_pix(res)
            pos = res.S(1:2)/2; %This assumes the phantom is centered in the FOV. May want to adjust in the future to make this more robust
        end
        
        function area = get.PhantomArea(res)
            area = pi*(res.PhantomDiameter/2)^2;
        end
        
        function pos = worldPos2PixelPos(res,pos)
            pos(:,1)=world2pixel(pos(:,1),res.x);
            pos(:,2)=world2pixel(pos(:,2),res.y);
        end
        
        function pos = pixelPos2WorldPos(res,pos)
            pos(1)=pixel2world(pos(1),res.x);
            pos(2)=pixel2world(pos(2),res.y);
        end
        
        function slice = getCentralSlice(res,module)
            %get the z position of interest
            z = res.ModuleCenters(module);
            
            %Transform slice location with the registration transformation
            [~,~,z] = transformPointsForward(res.tform,0,0,z);
            
            %find the closest slice
            [~,slice] = min(abs(z-res.z));
        end
        
        function displayCentralSlice(res,module)
            %Get the slice of interest
            slice=getCentralSlice(res,module);
            
            %display the slice
            setCurrentSlice(res.tool.handles.imtool,slice);
            
            %Set the window and level
            WL = get(res.tool.handles.CT_WLbutton(module),'UserData');
            setWindowLevel(res.tool.handles.imtool,WL(1),WL(2));
        end
        
        
        %Module 1----------------------------------------------------------
        %------------------------------------------------------------------
        function varargout = getHUMeasurement(res,material)
            %Get the location of the ROI
            pos = getROIPixelLocation(res,material);
            
            %Make the ROI object
            h=getHandles(res.tool.handles.imtool);
            ROI = imtool3DROI_ellipse(h.I,[pos res.HU_ROI_Diameter_pix res.HU_ROI_Diameter_pix]);
            
            slices = res.HU_ROI_Slices;
            for i=1:length(slices)
                setCurrentSlice(res.tool.handles.imtool,slices(i));
                stats=getMeasurements(ROI);
                HU(i)=stats.mean;
                STD(i)=stats.STD;
            end
            HU=mean(HU);
            STD=mean(STD);
            
            varargout{1}=HU;
            if nargout > 1
                varargout{2}=STD;
            end
            
            delete(ROI);
            
            
        end
        
        function pass = get.HU_Water_Pass(res)
            pass = didHUPass(res,'Water');
        end
        
        function pass = get.HU_Poly_Pass(res)
            pass = didHUPass(res,'Poly');
        end
        
        function pass = get.HU_Bone_Pass(res)
            pass = didHUPass(res,'Bone');
        end
        
        function pass = get.HU_Air_Pass(res)
            pass = didHUPass(res,'Air');
        end
        
        function pass = get.HU_Acrylic_Pass(res)
            pass = didHUPass(res,'Acrylic');
        end
        
        function pass = didHUPass(res,material)
            switch material
                case 'Water'
                    HU = res.HU_Water;
                    range = res.HU_Water_Threshold;
                case 'Poly'
                    HU = res.HU_Poly;
                    range = res.HU_Poly_Threshold;
                case 'Bone'
                    HU = res.HU_Bone;
                    range = res.HU_Bone_Threshold;
                case 'Air'
                    HU = res.HU_Air;
                    range = res.HU_Air_Threshold;
                case 'Acrylic'
                    HU = res.HU_Acrylic;
                    range = res.HU_Acrylic_Threshold;
            end
            
            if HU >=range(1) && HU <=range(2)
                pass=true;
            else
                pass = false;
            end
            
            
        end
        
        function HU_ROI_Diameter = get.HU_ROI_Diameter(res)
            HU_ROI_Diameter = 2*sqrt(res.HU_ROI_Area/pi);
        end
        
        function HU_ROI_Diameter_pix = get.HU_ROI_Diameter_pix(res)
            HU_ROI_Diameter_pix = res.HU_ROI_Diameter/res.psize;
        end
        
        function slices = get.HU_ROI_Slices(res)
            slices = getSlicesNumbers(res,res.Module1SliceRange_reg);
        end
        
        function z = get.Module1SliceRange_reg(res)
            x=zeros(2,1); y=zeros(2,1); z = res.Module1SliceRange';
            [~,~,z] = transformPointsForward(res.tform,x,y,z);
            z = z';
        end
        
        function varargout = getTTFMeasurement(res,material)
            %Get the location of the ROI
            pos = getROIPixelLocation(res,material);
            
            %make a TTF ROI
            addTTFROI(res.tool,[pos res.TTF_ROI_Diameter_pix res.TTF_ROI_Diameter_pix]);
            
            %Set the slice range
            slices = res.HU_ROI_Slices;
            setTTFSliceRange(res.tool,[min(slices) max(slices)]);
            
            %Make the TTF measurement
            measureTTF(res.tool)
            
            %Get the TTF
            TTF=copy(res.tool.TTFs(end));
            
            varargout{1}=TTF;
            if nargout>1
                varargout{2}=getPosition(res.tool.TTFROIs(end));
            end
            
            %Remove the ROI
            removeTTFROI(res.tool);
            
        end
        
        function TTF_ROI_Diameter_pix = get.TTF_ROI_Diameter_pix(res)
            TTF_ROI_Diameter_pix=res.TTF_ROI_Diameter/res.tool.TTF_R/res.psize;
        end
        
        function im = getHUROIim(res)
            %First display the central slice of module 1
            displayCentralSlice(res,1)
            
            %Turn off CT info display
            oldD = res.tool.showCTInfo;
            res.tool.showCTInfo = false;
            
            %Now draw the ROIs
            materials = res.HUmaterials;
            h=getHandles(res.tool.handles.imtool);
            for i=1:length(materials)
                pos = getROIPixelLocation(res,materials{i});
                ROI(i) = imtool3DROI_ellipse(h.I,[pos res.HU_ROI_Diameter_pix res.HU_ROI_Diameter_pix]);
            end
            
            %Put window and level on the image axes
            [W,L] = getWindowLevel(res.tool.handles.imtool);
            str{1} = ['WL: ' num2str(L)];
            str{2} = ['WW: ' num2str(W)];
             t = text(5,5,str,'HorizontalAlignment','left','VerticalAlignment','top',...
                'FontSize',12,'Color','y','Parent',h.Axes);
            
            %Take a snapshot of the axis
            im = getImageFrame(h.Axes);
            
            %Delete the ROIs
            delete(ROI);
            delete(t);
            
            %Revert CT info display
            res.tool.showCTInfo = oldD;
        end
        
        function im = getTTFROIim(res)
            %First display the central slice of module 1
            displayCentralSlice(res,1)
            
            %Turn off CT info display
            oldD = res.tool.showCTInfo;
            res.tool.showCTInfo = false;
            
            %Make the TTF ROIs
            materials = res.TTFmaterials;
            for i=1:length(materials)
                addTTFROI(res.tool,res.(['Pos_TTF_ROI_' materials{i} '_Pix']));
            end
            
            %Put window and level on the image axes
            h=getHandles(res.tool.handles.imtool);
            [W,L] = getWindowLevel(res.tool.handles.imtool);
            str{1} = ['WL: ' num2str(L)];
            str{2} = ['WW: ' num2str(W)];
            t = text(5,5,str,'HorizontalAlignment','left','VerticalAlignment','top',...
                'FontSize',12,'Color','y','Parent',h.Axes);
            
            %Take snapshot
            im = getImageFrame(h.Axes);
            
            %delete the ROIs
            removeTTFROI(res.tool);
            delete(t);
            
            %Revert CT info display
            res.tool.showCTInfo = oldD;
        end
        
        function im = getTTFim(res)
            %Make the figure
            xSize = 7.5; ySize = xSize/2;
            fig = createFigForPrinting(xSize,ySize);
            fig.Visible = 'off';
            
            %Make the TTF plot
            materials = res.TTFmaterials;
            ax = axes(fig,'OuterPosition',[.5 0 .5 1]);
            for i=1:length(materials)
                x = res.(['TTF_' materials{i}]).f;
                y = res.(['TTF_' materials{i}]).TTF;
                f50 = res.(['TTF_' materials{i}]).f50;
                h(i,1)=plot(x,y,'-','LineWidth',2); hold on
                plot([0 f50],[.5 .5],'--k')
                plot([f50 f50],[0 .5],'--k');
                leg{i,1} = materials{i};
            end
            legend(h,leg,'FontSize',10);
            legend boxoff
            ny = 1/(2*res.psize);
            set(ax,'Xlim',[0 ny],'XTick',[0:.1:ny]);
            grid on
            xlabel('Spatialy Frequency [1/mm]')
            ylabel('TTF');
            
            %Show the ROI image
            ax = axes(fig,'OuterPosition',[0 0 .5 1]);
            imshow(res.im_TTF_ROIs);
            
            im = getImageFrame(fig);
            close(fig);
            
        end
        
        
        %Module 2----------------------------------------------------------
        %------------------------------------------------------------------
        function [LCR_Signal, LCR_Background, LCR_Noise] = getLCRMeasurements(res)
            %get the slices of interest
            slices = res.LCR_ROI_Slices;
            
            %Get the signal measurement
            pos = getROIPixelLocation(res,'LCR_Signal');
            h=getHandles(res.tool.handles.imtool);
            ROI = imtool3DROI_ellipse(h.I,[pos res.LCR_ROI_Diameter_pix res.LCR_ROI_Diameter_pix]);
            for i=1:length(slices)
                setCurrentSlice(res.tool.handles.imtool,slices(i));
                stats=getMeasurements(ROI);
                LCR_Signal(i)=stats.mean;
            end
            LCR_Signal=mean(LCR_Signal);
            delete(ROI);
            
            %Get the background and noise measurement
            pos = getROIPixelLocation(res,'LCR_Background');
            ROI = imtool3DROI_ellipse(h.I,[pos res.LCR_ROI_Diameter_pix res.LCR_ROI_Diameter_pix]);
            for i=1:length(slices)
                setCurrentSlice(res.tool.handles.imtool,slices(i));
                stats=getMeasurements(ROI);
                LCR_Background(i)=stats.mean;
                LCR_Noise(i)=stats.STD;
            end
            LCR_Background=mean(LCR_Background);
            LCR_Noise=mean(LCR_Noise);
            delete(ROI);
            
        end
        
        function slices = get.LCR_ROI_Slices(res)
            slices = getSlicesNumbers(res,res.Module2SliceRange_reg);
        end
        
        function z = get.Module2SliceRange_reg(res)
            x=zeros(2,1); y=zeros(2,1); z = res.Module2SliceRange';
            [~,~,z] = transformPointsForward(res.tform,x,y,z);
            z = z';
        end
        
        function LCR_Contrast = get.LCR_Contrast(res)
            LCR_Contrast = res.LCR_Signal-res.LCR_Background;
        end
        
        function LCR_CNR = get.LCR_CNR(res)
            LCR_CNR = res.LCR_Contrast/res.LCR_Noise;
        end
        
        function LCR_ROI_Diameter = get.LCR_ROI_Diameter(res)
            LCR_ROI_Diameter = 2*sqrt(res.LCR_ROI_Area/pi);
        end
        
        function LCR_ROI_Diameter_pix = get.LCR_ROI_Diameter_pix(res)
            LCR_ROI_Diameter_pix = res.LCR_ROI_Diameter/res.psize;
        end
        
        function im = getLCRROIim(res)
            displayCentralSlice(res,2)
            
            %Turn off CT info display
            oldD = res.tool.showCTInfo;
            res.tool.showCTInfo = false;
            
            %Now draw the ROIs
            materials = {'LCR_Signal','LCR_Background'};
            h=getHandles(res.tool.handles.imtool);
            for i=1:length(materials)
                pos = getROIPixelLocation(res,materials{i});
                ROI(i) = imtool3DROI_ellipse(h.I,[pos res.LCR_ROI_Diameter_pix res.LCR_ROI_Diameter_pix]);
            end
            
            %Put window and level on the image axes
            [W,L] = getWindowLevel(res.tool.handles.imtool);
            str{1} = ['WL: ' num2str(L)];
            str{2} = ['WW: ' num2str(W)];
             t = text(5,5,str,'HorizontalAlignment','left','VerticalAlignment','top',...
                'FontSize',12,'Color','y','Parent',h.Axes);
            
            %Take a snapshot of the axis
            im = getImageFrame(h.Axes);
            
            %Delete the ROIs
            delete(ROI);
            delete(t);
            
            %Revert CT info display
            res.tool.showCTInfo = oldD;
        end
        
        
        %Module 3----------------------------------------------------------
        %------------------------------------------------------------------
        function slices = get.NPS_ROI_Slices(res)
            slices = getSlicesNumbers(res,res.Module3SliceRange_reg);
        end
        
        function z = get.Module3SliceRange_reg(res)
            x=zeros(2,1); y=zeros(2,1); z = res.Module3SliceRange';
            [~,~,z] = transformPointsForward(res.tform,x,y,z);
            z = z';
        end
        
        function pos = get.Pos_NPS_ROI(res)
            pos = repmat(res.Pos_NPS,[5 1]);
            r = res.NPS_ROI_Diameter;
            pos(2,1)=pos(2,1)-r; %9:00
            pos(3,2)=pos(3,2)-r; %12:00
            pos(4,1)=pos(4,1)+r; %3:00
            pos(5,2)=pos(5,2)+r; %6:00
        end
        
        function NPS_ROI_Diameter_pix= get.NPS_ROI_Diameter_pix(res)
            NPS_ROI_Diameter_pix = res.NPS_ROI_Diameter/res.psize;
        end
        
        function NPS = getNPSMeasurement(res)
            %Get the positions of the ROIs
            pos = getROIPixelLocation(res,'NPS');
            
            %make NPSs ROIs
            for i=1:size(pos,1)
                addNPSROI(res.tool,[pos(i,:) res.NPS_ROI_Diameter_pix res.NPS_ROI_Diameter_pix]);
            end
            
            %Set the slice range
            slices = res.NPS_ROI_Slices;
            setNPSSliceRange(res.tool,[min(slices) max(slices)]);
            
            %Make the NPS measurement
            measureNPS(res.tool)
            
            %Get the NPS
            NPS=copy(res.tool.NPSs(end));
            
            %Remove the ROIs
            removeNPSROI(res.tool)
            
            
        end
        
        function NPS_noise = get.NPS_noise(res)
            NPS_noise = res.NPS.noise;
        end
        
        function Uniformity_ROI_Diameter = get.Uniformity_ROI_Diameter(res)
            Uniformity_ROI_Diameter = 2*sqrt(res.Uniformity_ROI_Area/pi);
        end
        
        function Uniformity_ROI_Diameter_pix = get.Uniformity_ROI_Diameter_pix(res)
            Uniformity_ROI_Diameter_pix = res.Uniformity_ROI_Diameter/res.psize;
        end
        
        function varargout = getUniformityMeasurement(res,position)
            %Get the location of the ROI
            pos = getROIPixelLocation(res,position);
            
            %Make the ROI object
            h=getHandles(res.tool.handles.imtool);
            ROI = imtool3DROI_ellipse(h.I,[pos res.Uniformity_ROI_Diameter_pix res.Uniformity_ROI_Diameter_pix]);
            
            slices = res.NPS_ROI_Slices;
            for i=1:length(slices)
                setCurrentSlice(res.tool.handles.imtool,slices(i));
                stats=getMeasurements(ROI);
                HU(i)=stats.mean;
                STD(i)=stats.STD;
            end
            HU=mean(HU);
            STD=mean(STD);
            varargout{1}=HU;
            if nargout>1
                varargout{2}=STD;
            end
            delete(ROI);
            
        end
        
        function Uniformity = get.Uniformity(res)
            Uniformity=max(abs(res.HU_Center - [res.HU_12 res.HU_3 res.HU_6 res.HU_9]));
        end
        
        function pass = get.Uniformity_Pass(res)
            if res.Uniformity <=res.Uniformty_Threshold
                pass=true;
            else
                pass=false;
            end
        end
        
        function im = getUniformityROIim(res)
            displayCentralSlice(res,3)
            
            %Turn off CT info display
            oldD = res.tool.showCTInfo;
            res.tool.showCTInfo = false;
            
            %Now draw the ROIs
            materials = res.UniformityLocations;
            h=getHandles(res.tool.handles.imtool);
            for i=1:length(materials)
                pos = getROIPixelLocation(res,materials{i});
                ROI(i) = imtool3DROI_ellipse(h.I,[pos res.Uniformity_ROI_Diameter_pix res.Uniformity_ROI_Diameter_pix]);
            end
            
            %Put window and level on the image axes
            [W,L] = getWindowLevel(res.tool.handles.imtool);
            str{1} = ['WL: ' num2str(L)];
            str{2} = ['WW: ' num2str(W)];
            t = text(5,5,str,'HorizontalAlignment','left','VerticalAlignment','top',...
                'FontSize',12,'Color','y','Parent',h.Axes);
            
            %Take a snapshot of the axis
            im = getImageFrame(h.Axes);
            
            %Delete the ROIs
            delete(ROI);
            delete(t);
            
            %Revert CT info display
            res.tool.showCTInfo = oldD;
        end
        
        function im = getNPSROIim(res)
            displayCentralSlice(res,3)
            
            %Turn off CT info display
            oldD = res.tool.showCTInfo;
            res.tool.showCTInfo = false;
            
            %Get the positions of the ROIs
            pos = getROIPixelLocation(res,'NPS');
            
            %make NPSs ROIs
            for i=1:size(pos,1)
                addNPSROI(res.tool,[pos(i,:) res.NPS_ROI_Diameter_pix res.NPS_ROI_Diameter_pix]);
            end
            
            %Put window and level on the image axes
            h=getHandles(res.tool.handles.imtool);
            [W,L] = getWindowLevel(res.tool.handles.imtool);
            str{1} = ['WL: ' num2str(L)];
            str{2} = ['WW: ' num2str(W)];
            t = text(5,5,str,'HorizontalAlignment','left','VerticalAlignment','top',...
                'FontSize',12,'Color','y','Parent',h.Axes);
            
            %Take a snapshot of the axis
            im = getImageFrame(h.Axes);

            %Remove the ROIs
            removeNPSROI(res.tool)
            delete(t);
            
            %Revert CT info display
            res.tool.showCTInfo = oldD;
        end
        
        function [im,varargout] = getNPSim(res)
            %Make the figure
            xSize = 7.5; ySize = 4.5;
            fig = createFigForPrinting(xSize,ySize);
            fig.Visible = 'off';
            
            %Make 2D NPS figure
            ax = axes(fig,'OuterPosition',[.5 .5 .5 .5]);
            x=res.NPS.stats.fx;
            y=res.NPS.stats.fy;
            vals = res.NPS.stats.NPS_2D;
            imshow(vals,[min(vals(:)) max(vals(:))],'XData',x,'YData',y,'Parent',ax);
            c=colorbar;
            ylabel(c,'NPS [HU^2mm^2]','FontSize',9)
            axis on
            xlabel(ax,'f_x [1/mm]','FontSize',9);
            ylabel(ax,'f_y [1/mm]','FontSize',9);
            set(ax,'FontSize',9)
            title('2D NPS','FontSize',12)
            
            %Make 1D NPS figure
            ax = axes(fig,'OuterPosition',[.5 0 .5 .5]);
            plot(res.NPS.f,res.NPS.NPS,'-','LineWidth',2); hold on;
            ny = 1/(2*res.psize);
            set(ax,'Xlim',[0 sqrt(2)*ny],'XTick',[0:.2:sqrt(2)*ny]);
            xlabel('Spatialy Frequency [1/mm]')
            ylabel('NPS [HU^2mm^2]')
            title('Radially averaged NPS');
            x = res.NPS.fav;
            y = interp1(res.NPS.f,res.NPS.NPS,x);
            p(1) = plot([x x],[0 y],'-k');
            x = res.NPS.fpeak;
            y = interp1(res.NPS.f,res.NPS.NPS,x);
            p(2) = plot([x x],[0 y],'--k');
            legend(p,{'f_a_v','f_p_e_a_k'});
            
            %Show the ROI image
            ax = axes(fig,'OuterPosition',[0 0 .5 1]);
            imshow(res.im_NPS_ROIs);
            title('NPS ROI Locations')
            
            im = getImageFrame(fig);
            switch nargout
                case 1
                    close(fig);
                case 2
                    varargout{1} = fig;
            end
        end
        
        function im = getDprimeim(res)
            %Make the figure
            xSize = 7.5; ySize = xSize;
            fig = createFigForPrinting(xSize,ySize);
            fig.Visible = 'off';
            
            im = [];
            materials = res.TTFmaterials;
            WL = [400 40; 1800 400; 1600 -600; 400 40];
            s = -.4:.2:.4; %Scale factor to make contrast detail diagram
            nCol = 2; %Number of columns of axes
            nRow = ceil(length(materials)/2); 
            aw = 1/nCol; %Axes widths and heights
            ah = 1/nRow;
            psize = res.psize;
            for i=1:length(materials)
                %get the position of the axes
                [row, col] = ind2sub([nRow nCol],i);
                row = nRow-row+1;
                ax = axes('OuterPosition',[(col-1)*1/nCol (row-1)*1/nRow aw ah]);
                
                %Make the contrast detail image
                dp = copy((res.(['dPrime_' materials{i}])));
                N = ceil(dp.FOV/psize);
                dp.task.psize=psize;
                dp.task.N = N;
                im = zeros(N*length(s));
                Cref = dp.task.Contrast;
                Dref = dp.task.diameter;
                clear DP X Y T
                for j=1:length(s)
                    for k=1:length(s)
                        %Set the task diameter and contrast
                        C = sign(Cref)*(abs(Cref) + s(j)*abs(Cref)); %contrast of this particular task
                        D = Dref + s(k)*Dref;      %Diameter of this particular task
                        dp.task.Contrast=C;
                        dp.task.diameter = D;
                        Y(j,k) = (j-1)*N;
                        X(j,k) = (k-1/2)*N;
                        T{j,k} = ['d'' = ' sprintf('%.1f',dp.detectabilityIndex)];
                        %Get the task image
                        im((j-1)*N+1:(j-1)*N+N,(k-1)*N+1:(k-1)*N+N)=dp.task.im;
                    end
                end
                
                %Blur the image
                im = blurWithTTF(dp.TTF,im,dp.task.psize);
                %Add noise
                noise = getCorrelatedNoise(dp.NPS,dp.task.psize,[size(im,2) size(im,1)],dp.noiseGenerationMode);
                im = im + noise;
                
                %Show the image
                W=WL(i,1); L=WL(i,2);
                imshow(im,[L-W/2 L+W/2]);
                text(0,size(im,1),['WL: ' num2str(L) ', WW: '  num2str(W)],'HorizontalAlignment','Left',...
                    'VerticalAlignment','bottom','Color','r','FontSize',8);
                
                %Label the contrast detail grid with dprimes
                for j=1:length(s)
                    for k=1:length(s)
                        text(X(j,k),Y(j,k),T{j,k},...
                            'HorizontalAlignment','Center','VerticalAlignment','top',...
                            'Color','r','FontSize',8);
                    end
                end
                
                %Add labels to the plot
                title(['Based on ' materials{i} ' TTF and contrast']);
                xlabel('Signal Diameter [mm]')
                ylabel('Contrast [HU]');
                clabs = sign(Cref)*(abs(Cref) + s*abs(Cref));
                dlabs = Dref + s*Dref;
                clear ylab xlab
                for k=1:length(s)
                    xlab{k} = sprintf('%.1f',dlabs(k));
                    ylab{k} = sprintf('%.0f',clabs(k));
                end
                set(ax,'XTick',X(1,:),'XTickLabel',xlab,'YTick',X(1,:),'YTickLabel',ylab);
                axis on

            end
            
            %Get the image
            im = getImageFrame(fig);
            close(fig);
        end
        
        
        %Module 4----------------------------------------------------------
        %------------------------------------------------------------------
        function im = getHCRim(res)
            displayCentralSlice(res,4)
            
            %Turn off CT info display
            oldD = res.tool.showCTInfo;
            res.tool.showCTInfo = false;
            
            %Take a snapshot of the axis
            h=getHandles(res.tool.handles.imtool);
            [W,L] = getWindowLevel(res.tool.handles.imtool);
            str{1} = ['WL: ' num2str(L)];
            str{2} = ['WW: ' num2str(W)];
            t = text(5,5,str,'HorizontalAlignment','left','VerticalAlignment','top',...
                'FontSize',12,'Color','y','Parent',h.Axes);
            im = getImageFrame(h.Axes);
            
            %Remove the text
            delete(t);
            
            %Revert CT info display
            res.tool.showCTInfo = oldD;
        end
        
        %Report------------------------------------------------------------
        %------------------------------------------------------------------
        function generateReport(res,output)
            res.tool.status = 'ACR Analysis: Generating report';
            
            %Make it compiler compatable if needed
            if isdeployed
                makeDOMCompilable
            end
            
            %Import the API
            import mlreportgen.dom.*;
            
            %Set some constants
            ROIimSize={Width('65%'),Height('65%')};
            toDelete={};
            %Create the page margins object
            margins=PageMargins;
            margins.Top='0.5in';
            margins.Bottom='0.5in';
            margins.Left='0.5in';
            margins.Right='0.5in';
            margins.Header='0.0in';
            margins.Footer='0.0in';
            
            
            %Create the document object
            d = Document(output,'pdf');
            
            %Create title
            t=Text('imQuest ACR Phantom Report');
            t.Bold=true;
            t.FontSize='24';
            append(d,t);
            t=Text(['Report generated on ' date]);
            t.FontSize='11';
            append(d,t);
            d.CurrentPageLayout.PageMargins=margins;
            
            %Table of contents
            t = TOC(2,' ');
            append(d,t);
            
            %Series info section
            h=Heading1('Series Info');
            append(d,h);
            append(d,HorizontalRule());
            str=res.CTSeriesSummary;
            clear t
            for i=1:length(str)
                t{i} = Text(str{i});
                t{i}.FontSize = '12';
            end
            t = UnorderedList(t);
            t.Style = {LineSpacing(1)};
            append(d,t);
            append(d,PageBreak);
            
            %Results summary
            h=Heading1('Results Summary');
            append(d,h);
            append(d,HorizontalRule());
            tab = res.resultsSummaryTable;
            header = tab.Properties.VariableNames;
            t = table2cell(tab);
            t = FormalTable(header,t);
            t.HAlign = 'Center';
            t.Width = '3in';
            t.Border='hidden';
            t.Body.ColSep='solid';
            t.TableEntriesHAlign='Left';
            t.TableEntriesInnerMargin='2pt';
            t.Header.Style={Bold};
            t.Header.RowSep='single';
            t.Header.ColSep='single';
            append(d,t);
            append(d,HorizontalRule());
            append(d,PageBreak);
            
            %HU Section
            h=Heading1('HU Accuracy');
            append(d,h);
            append(d,HorizontalRule());
            %Create the HU Table
            header = {'Material', 'Mean [HU]', 'Tolerance [HU]  ', 'Pass/Fail?'};
            t = { 'Water', sprintf('%3.1f',res.HU_Water), res.HU_Water_Threshold(1), res.HU_Water_Threshold(2), boolean2PassFail(res.HU_Water_Pass); ...
                'Poly', sprintf('%3.1f',res.HU_Poly), res.HU_Poly_Threshold(1), res.HU_Poly_Threshold(2), boolean2PassFail(res.HU_Poly_Pass);...
                'Bone', sprintf('%3.1f',res.HU_Bone), res.HU_Bone_Threshold(1), res.HU_Bone_Threshold(2), boolean2PassFail(res.HU_Bone_Pass);...
                'Air', sprintf('%3.1f',res.HU_Air), res.HU_Air_Threshold(1), res.HU_Air_Threshold(2), boolean2PassFail(res.HU_Air_Pass);...
                'Acrylic', sprintf('%3.1f',res.HU_Acrylic), res.HU_Acrylic_Threshold(1), res.HU_Acrylic_Threshold(2), boolean2PassFail(res.HU_Acrylic_Pass)};
            t = FormalTable(header,t);
            t.Header.Children.Entries(3).ColSpan=2;
            t.Header.Children.Entries(3).Style={HAlign('center')};
            t.Border='hidden';
            t.Body.ColSep='single';
            t.TableEntriesHAlign='center';
            t.TableEntriesInnerMargin='2pt';
            t.Header.Style={Bold};
            t.HAlign = 'center';
            t.Width = '6in';
            t.Header.RowSep='single';
            t.Header.ColSep='single';
            append(d,t);
            append(d,HorizontalRule());
            %Add the HU image
            io=makeDocImageObject(res.im_HU_ROIs,output);
            io.Style = {Width('6in'),HAlign('center')};
            append(d,io);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            
            
            %CNR Section
            h=Heading1('Contrast-to-noise Ratio');
            append(d,h);
            append(d,HorizontalRule());
            header = {'Signal [HU]', 'Bkg [HU]', 'Noise [HU]', 'Contrast [HU]', 'CNR'};
            t = { sprintf('%3.2f',res.LCR_Signal), sprintf('%3.2f',res.LCR_Background), sprintf('%3.2f',res.LCR_Noise), sprintf('%3.2f',res.LCR_Contrast), sprintf('%3.2f',res.LCR_CNR)};
            t = FormalTable(header,t);
            t.Border='hidden';
            t.Body.ColSep='single';
            t.TableEntriesHAlign='center';
            t.TableEntriesInnerMargin='2pt';
            t.Header.Style={Bold};
            t.HAlign = 'center';
            t.Width = '6in';
            t.Header.RowSep='single';
            t.Header.ColSep='single';
            append(d,t);
            append(d,HorizontalRule());
            %Add the CNR image
            io=makeDocImageObject(res.im_LCR_ROIs,output);
            io.Style = {Width('6in'),HAlign('center')};
            append(d,io);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            %Uniformity Section
            h=Heading1('Uniformity');
            append(d,h);
            append(d,HorizontalRule());
            header = {'Position', 'Mean [HU]', 'Diff from center [HU]', 'Tolerance [HU]', 'Pass/Fail?','Noise [HU]'};
            t       =   {'Center', sprintf('%3.1f',res.HU_Center), '-', '-', '-', sprintf('%3.1f',res.STD_Center);...
                '12:00', sprintf('%3.1f',res.HU_12), sprintf('%3.1f',abs(res.HU_12-res.HU_Center)), res.Uniformty_Threshold, boolean2PassFail(abs(res.HU_12-res.HU_Center)<=res.Uniformty_Threshold), sprintf('%3.1f',res.STD_12);...
                '03:00', sprintf('%3.1f',res.HU_3), sprintf('%3.1f',abs(res.HU_3-res.HU_Center)), res.Uniformty_Threshold, boolean2PassFail(abs(res.HU_3-res.HU_Center)<=res.Uniformty_Threshold), sprintf('%3.1f',res.STD_3);...
                '06:00', sprintf('%3.1f',res.HU_6), sprintf('%3.1f',abs(res.HU_6-res.HU_Center)), res.Uniformty_Threshold, boolean2PassFail(abs(res.HU_6-res.HU_Center)<=res.Uniformty_Threshold), sprintf('%3.1f',res.STD_6);...
                '09:00', sprintf('%3.1f',res.HU_9), sprintf('%3.1f',abs(res.HU_9-res.HU_Center)), res.Uniformty_Threshold, boolean2PassFail(abs(res.HU_9-res.HU_Center)<=res.Uniformty_Threshold), sprintf('%3.1f',res.STD_9);...
                'Overall', '-','-','-',boolean2PassFail(res.Uniformity_Pass), '-'...
                };
            t = FormalTable(header,t);
            t.Border='hidden';
            t.Body.ColSep='single';
            t.TableEntriesHAlign='center';
            t.TableEntriesInnerMargin='2pt';
            t.Header.Style={Bold};
            t.HAlign = 'center';
            t.Width = '7in';
            t.Header.RowSep='single';
            t.Header.ColSep='single';
            append(d,t);
            t=Text('');
            append(d,t);
            append(d,HorizontalRule());
            %Add the Uniformity image
            io=makeDocImageObject(res.im_Uniformity_ROIs,output);
            io.Style = {Width('6in'),HAlign('center')};
            append(d,io);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            %HCR Section
            h=Heading1('High Contrast Resolution');
            append(d,h);
            append(d,HorizontalRule());
            %Add the HCR ROIs image
            io=makeDocImageObject(res.im_HCR,output);
            io.Style = {Width('6in'),HAlign('center')};
            append(d,io);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            %TTF section
            h=Heading1('Task Transfer Function');
            append(d,h);
            append(d,HorizontalRule());
            header = {'Material', 'Contrast [HU]','Bkg. Noise [HU]', 'f50 [1/mm]  ', 'f10 [1/mm]'};
            t       =   {'Poly', sprintf('%3.1f',res.TTF_Poly.contrast), sprintf('%3.1f',res.TTF_Poly.noise), sprintf('%3.2f',res.TTF_Poly.f50), sprintf('%3.2f',res.TTF_Poly.f10);...
                'Bone', sprintf('%3.1f',res.TTF_Bone.contrast), sprintf('%3.1f',res.TTF_Bone.noise), sprintf('%3.2f',res.TTF_Bone.f50), sprintf('%3.2f',res.TTF_Bone.f10);...
                'Air', sprintf('%3.1f',res.TTF_Air.contrast), sprintf('%3.1f',res.TTF_Air.noise), sprintf('%3.2f',res.TTF_Air.f50), sprintf('%3.2f',res.TTF_Air.f10);...
                'Acrylic', sprintf('%3.1f',res.TTF_Acrylic.contrast), sprintf('%3.1f',res.TTF_Acrylic.noise), sprintf('%3.2f',res.TTF_Acrylic.f50), sprintf('%3.2f',res.TTF_Acrylic.f10);...
                };
            t = FormalTable(header,t);
            t.Border='hidden';
            t.Body.ColSep='single';
            t.TableEntriesHAlign='center';
            t.TableEntriesInnerMargin='2pt';
            t.Header.Style={Bold};
            t.HAlign = 'center';
            t.Width = '6in';
            t.Header.RowSep='single';
            t.Header.ColSep='single';
            append(d,t);
            append(d,HorizontalRule());
            %Add the TTF  image
            io=makeDocImageObject(res.im_TTFs,output);
            io.Style = {Width('6in'),HAlign('center')};
            append(d,io);
            toDelete{end+1}=io.Path;
            
            f10s = [res.TTF_Poly.stats.f10,res.TTF_Bone.stats.f10,res.TTF_Air.stats.f10,res.TTF_Acrylic.stats.f10];
            ny = 1/(2*res.psize);
            if any(f10s>ny)
                 t=Text('Warning: At least one f10% value was measured to be greater than the Nyquist frequency. Take results with a grain of salt');
                 append(d,t);
            end
            append(d,PageBreak);
            
            %NPS section
            h=Heading1('Noise Power Spectrum');
            append(d,h);
            append(d,HorizontalRule());
            header = {'Noise Magnitude [HU]', 'fpeak [1/mm]','fav [1/mm]'};
            t =         {sprintf('%3.1f',res.NPS.noise), sprintf('%3.2f',res.NPS.fpeak), sprintf('%3.2f',res.NPS.fav)};
            t = FormalTable(header,t);
            t.Border='hidden';
            t.Body.ColSep='single';
            t.TableEntriesHAlign='Center';
            t.TableEntriesInnerMargin='2pt';
            t.Header.Style={Bold};
             t.HAlign = 'center';
            t.Width = '5in';
            t.Header.RowSep='single';
            t.Header.ColSep='single';
            append(d,t);
            append(d,HorizontalRule());
            %Add the NPS  image
            io=makeDocImageObject(res.im_NPS,output);
            t=Table({io}); t.TableEntriesHAlign='Center'; t.Border='hidden';
            append(d,t);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            %Detectability section
            h=Heading1('Detectability Index');
            append(d,h);
            append(d,HorizontalRule());
            %Add the NPS  image
            io=makeDocImageObject(res.im_Dprime,output);
            io.Style = {Width('6in'),HAlign('center')};
            append(d,io);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            %Imquest version info
            h=Heading1('imQuest Version Info');
            append(d,h);
            append(d,HorizontalRule());
            tab = res.tool.versionInfoTable;
            clear t
            t{1}=Text(['Git branch: ' tab.BranchName ', Commit ID: ' tab.GitID]);
            t{1}.FontSize = '14';
            t = UnorderedList(t);
            append(d,t);
            
            %Close the document (This saves it to a file)
            close(d);
            
            %Delete the temp files
            for i=1:length(toDelete)
                delete(toDelete{i});
            end
            
            %View the document
            res.tool.status = 'ACR Analysis: Opening Report';
            rptview(output,'pdf');
            showTempStatus(res.tool,'ACR Analysis: Done Generating Report!')
            res.tool.status = res.tool.defaultStatus;
            
        end
        
        function tab = get.resultsSummaryTable(res)
            materials = {'Water','Poly','Bone','Air','Acrylic'};
            for i=1:length(materials)
                tab(i).Test = ['HU ' materials{i}];
                tab(i).Result = boolean2PassFail(res.(['HU_' materials{i} '_Pass']));
            end
            tab(end+1).Test = 'Uniformity';
            tab(end).Result = boolean2PassFail(res.Uniformity_Pass);
            tab(end+1).Test = 'CNR';
            tab(end).Result = sprintf('%3.2f',res.LCR_CNR);
            materials = {'Poly','Bone','Air','Acrylic'};
            for i=1:length(materials)
                tab(end+1).Test = ['TTF f50% ' materials{i}];
                tab(end).Result = [sprintf('%3.2f',res.(['TTF_' materials{i}]).f50) ' cycles/mm'];
            end
            tab(end+1).Test = 'Noise magnitude';
            tab(end).Result = [sprintf('%3.2f',res.NPS.noise) ' HU'];
            tab(end+1).Test = 'NPS fav';
            tab(end).Result = [sprintf('%3.2f',res.NPS.fav) ' cycles/mm'];
            tab(end+1).Test = 'NPS fpeak';
            tab(end).Result = [sprintf('%3.2f',res.NPS.fpeak) ' cycles/mm'];
            
            
            tab=struct2table(tab);
        end
        
        function t = get.SeriesDescription(res)
            try
                t = res.DICOMheaders(1).SeriesDescription;
            catch
                t = 'UnknownSeriesDescription';
            end
        end
        
        %Regsistration-----------------------------------------------------
        %------------------------------------------------------------------
        function I = createRegistrationTemplate(res)
            %Define voxelization settings
            N = res.Template_N;
            FOV = res.Template_FOV;
            ST = res.Template_ST;
            psize = res.Template_psize;
            
            %Create coordinate system
            x=res.Template_xy;
            [X,Y]=meshgrid(x,x);
            z=res.Template_z;
            %Create rotated coordinate system
            T = [1/sqrt(2) 1/sqrt(2); -1/sqrt(2) 1/sqrt(2)];
            P = [X(:) Y(:)]*T;
            Xp=reshape(P(:,1),size(X));
            Yp=reshape(P(:,2),size(Y));
            
            %Initialize image
            I = zeros(N,N,length(z));
            
            %Define phantom shape
            R = sqrt(X.^2+Y.^2);
            mask = R<=100;
            mask = repmat(mask,[1 1 length(z)]);
            I(~mask)=-1000;
            
            %Make individual slices
            for i=1:length(z)
                im = I(:,:,i);
                if z(i)<=20 && z(i)>=-20 %Module 1

                    %Poly insert
                    R = sqrt((X-res.Pos_Poly(1)).^2 + (Y-res.Pos_Poly(2)).^2);
                    mask = R<=res.TTF_ROI_Diameter/2;
                    im(mask) = -95;
                    %Bone insert
                    R = sqrt((X-res.Pos_Bone(1)).^2 + (Y-res.Pos_Bone(2)).^2);
                    mask = R<=res.TTF_ROI_Diameter/2;
                    im(mask) = 900;
                    %Air insert
                    R = sqrt((X-res.Pos_Air(1)).^2 + (Y-res.Pos_Air(2)).^2);
                    mask = R<=res.TTF_ROI_Diameter/2;
                    im(mask) = -1000;
                    %Acrylic insert
                    R = sqrt((X-res.Pos_Acrylic(1)).^2 + (Y-res.Pos_Acrylic(2)).^2);
                    mask = R<=res.TTF_ROI_Diameter/2;
                    im(mask) = 125;
                    
                elseif z(i)> 20 && z(i)<=60 %Module 2
                    R = sqrt(X.^2+Y.^2);
                    mask = R<=100-9.75;
                    im(mask) = 90;
                elseif z(i)==80 %Module 3
                    R =  sqrt((X-20/sqrt(2)).^2 + (Y+20/sqrt(2)).^2);
                    mask = R<=1.75;
                    im(mask)=1200;
                    R =  sqrt((X+80/sqrt(2)).^2 + (Y-80/sqrt(2)).^2);
                    mask = R<=1.75;
                    im(mask)=1200;
                elseif z(i) > 100 && z(i) <=140 %Module 4
                    r = 70;
                    w = 15;
                    phi = 0:45:315;
                    for j=1:length(phi)
                        p = phi(j)*pi/180;
                        [xp,yp]=pol2cart(p,r);
                        mask = Xp >= xp-w/2 & Xp <= xp+w/2 & Yp >= yp-w/2 & Yp <= yp+w/2;
                        im(mask) = 1200;
                    end
                elseif z(i)<-20 || z(i)>140
                    im(:)=-1000;
                    
                end
                %Add bb's
                if (z(i)>=-1.75/2 && z(i) <= 1.75/2) || (z(i)>=120-1.75/2 && z(i)<=120+1.75/2)
                    R =  sqrt((X).^2 + (Y+100).^2);
                    mask = R<=1.75;
                    im(mask)=1200;
                    R =  sqrt((X).^2 + (Y-100).^2);
                    mask = R<=1.75;
                    im(mask)=1200;
                    R =  sqrt((X+100).^2 + (Y).^2);
                    mask = R<=1.75;
                    im(mask)=1200;
                    R =  sqrt((X-100).^2 + (Y).^2);
                    mask = R<=1.75;
                    im(mask)=1200;
                end
                I(:,:,i)=im;
            end
        end
        
        function Rtemp = get.Rtemp(res)
            %Make spatial reference object
            psize = res.Template_psize;
            x = res.Template_xy;
            z = res.Template_z;
            ST = res.Template_ST;
            Rtemp = imref3d(res.Template_S,[x(1)-psize/2 x(end)+psize/2],[x(1)-psize/2 x(end)+psize/2],[z(1)-ST/2 z(end)+ST/2]);
        end
        
        function S = get.Template_S(res)
            S = [res.Template_N res.Template_N length(res.Template_z)];
        end
        
        function x = get.Template_xy(res)
            x=(0:res.Template_N-1)*res.Template_psize - res.Template_FOV/2;
        end
        
        function psize = get.Template_psize(res)
            N = res.Template_N;
            FOV = res.Template_FOV;
            psize = FOV/N;
        end
        
        function z = get.Template_z(res)
            ST = res.Template_ST;
            z=-20-ST:ST:140+ST;
        end
        
        function Rim = get.Rim(res)
            x=res.x;
            y=res.y;
            z=res.z;
            z = unique(z);
            ST = diff(z);
            ST=mean(ST);
            psize = res.psize;
            Rim = imref3d(res.S,[x(1)-psize/2 x(end)+psize/2],[y(1)-psize/2 y(end)+psize/2],[z(1)-ST/2 z(end)+ST/2]);
        end
        
        function tform = registerToTemplate(res)
            %Get the template image
            moving = createRegistrationTemplate(res);
            Rmoving = res.Rtemp;
            
            %Get the spatial referencing object of the image
            Rfixed = res.Rim;
            
            %Get the fixed image
            [~, ~, zR] = intrinsicToWorld(Rfixed,(1:res.S(3))',(1:res.S(3))',(1:res.S(3))'); %Get the coordinates in z
            if any(abs(zR-res.z)>1e4*eps(min(abs(zR),abs(res.z))))
                %Need to resample the image at locations of zR
                fixed = getImage(res.tool.handles.imtool);
                fixed = permute(fixed,[3 2 1]);
                %Get the unique z positions (this throws out repeated
                %slices that are in the same z location
                [z,ind]=unique(res.z);
                fixed=fixed(ind,:,:);
                %Resample image at the locations of the referencing object
                fixed = interp1(res.z,fixed,zR,'linear','extrap');
                fixed = permute(fixed,[3 2 1]);
            else
                fixed = getImage(res.tool.handles.imtool);
            end
            
            %Get initial poor-man's 2D registration
            center = findPhantomCenter(res,fixed);
            center = pixelPos2WorldPos(res,center);
            T = eye(4);
            T(end,1:2)=center;
            tform = affine3d(T);
            
            %Perform the registration
            switch res.registrationMethod
                case 'Full'
                    [optimizer, metric] = imregconfig('monomodal'); %Define settings
                    optimizer.MinimumStepLength = 1e-4;
                    optimizer.MaximumStepLength=.1;
                    optimizer.MaximumIterations = 25;
                    try
                        tform = imregtform(moving,Rmoving, fixed,Rfixed, 'rigid', optimizer, metric,...
                            'InitialTransformation',tform,'DisplayOptimization',false);
                    catch ME
                        res.tool.logError(ME)
                        showTempStatus(res.tool,'ACR Analysis: Error performing registration, using identity transformation')
                        tform = affine3d(T);
                    end
                case 'Quick'
                    tform = tform;
                case 'None'
                    tform = affine3d();
                otherwise
                    warning(['Did not understand ''' res.registrationMethod ''' registration method, using ''Quick'''])
                    
            end
            
        end
        
        function center = findPhantomCenter(res,im)
            slices = false(size(im,3),1);
            for i=1:size(im,3)
                %Get mask of the phantom
                m = segmentPhantomInSlice(res,im(:,:,i));
                %Calculate the area
                area = sum(m(:))*res.psize^2;
                if area >= res.PhantomArea*.9 && area <= res.PhantomArea*1.1
                    slices(i)=true;
                end
                mask(:,:,i)=m;
            end
            %remove slices that don't have the phantom
            im = im(:,:,slices);
            mask = mask(:,:,slices);
            
            %Create a coordinate system (in pixels)
            [X, Y]=meshgrid(1:size(im,2),1:size(im,1));
            Cx = size(im,2)/2; Cy = size(im,1)/2;
            X=X-Cx; Y=Y-Cy;
            R = sqrt(X.^2+Y.^2);
            
            %Get diameter in pixels
            d = res.PhantomDiameter/res.psize(1);
            
            %Make the circle image
            ind = R<=d/2;
            Circle=zeros(size(im,1),size(im,2));
            
            
            for i=1:size(im,3)
                %Shift the image such that its min is 0;
                imt = im(:,:,i);
                maskt = mask(:,:,i);
                HU_min = min(imt(:));
                imt=imt-HU_min;
                circle = Circle;
                circle(ind) = -HU_min;
                
                %Run filter in Fourier Domain
                MV = mean(imt(:));
                imt = imt-MV;
                F=fftn(imt);
                F(1)=0;
                H = fftn(circle);
                H=F.*H;
                cc = fftshift(real(ifftn(H)) + MV );
                cc(~maskt)=0;
                [~, imax] = max(cc(:));
                [ypeak, xpeak] = ind2sub(size(cc),imax(1));
                center(i,:) = [xpeak ypeak];
                
            end
            center = mean(center,1);
            
            %center = findCircleCenter(res,im,d,res.HU_bkg,mask);
            
        end
        
        function mask = segmentPhantomInSlice(res,im)
            mask = im>res.T_size;
            NHOOD = ones(9);
            mask = imdilate(imerode(mask,NHOOD),NHOOD); %Dilate errode the mask
            mask = imfill(mask,'holes'); %Fill holes
        end
        
    end
    
end

function pos_mm = pixel2world(xi,x) %xi is pixel coordinate of interest, x is world coordinates
%Convert from pixel coordinates to world coordinates
pos_mm = interp1(1:length(x),x,xi);
end

function pos_pix = world2pixel(xi,x) %xi is world coordinate of interest, x is world coordinates
%Convert from world coordinates to pixel coordinates
pos_pix = interp1(x,1:length(x),xi);
end

function str = boolean2PassFail(bool)
if bool
    str='Pass';
else
    str='Fail';
end
end

function io = makeDocImageObject(im,output)

[PATHSTR,NAME,EXT] = fileparts(output);

import mlreportgen.dom.*;
fname=[PATHSTR filesep dicomuid '.png']; %generate a uid
imwrite(im,fname); %Write the image
io = Image(fname); %Make the object
io.Style={ScaleToFit};

end

function im = getImageFrame(h)
try %Only works in modes where the figure is acutally displayed
    frame = getframe(h);
    im=frame.cdata;
catch
    try
        switch class(h)
            case 'matlab.ui.Figure'
                fig=copyobj(h,0);
                set(fig,'Visible','off');
                set(fig,'Units','Pixels');
            otherwise
                %make a new figure
                fig=figure('Visible','off');
                colormap gray
                %copy the object to the new figure
                h=copyobj(h,fig);
                set(h,'Units','Pixels');
                set(fig,'Units','Pixels');
                set(fig,'Position',h.Position);
        end
        %get Screen DPI
        DPI=get(0,'ScreenPixelsPerInch');
        pos = fig.Position(3:4); %get pixel size of the figure;
        pos = pos/DPI; %now pixel size is in inches
        
        %Set paper position propery of figure;
        set(fig,'PaperUnits','inches','PaperSize',pos);
        set(fig,'PaperPosition',[0 0 pos],'InvertHardcopy','off');
        
        %Print the figure to an image
        im = print(fig,'-RGBImage');
        close(fig);
    catch
        im=[];
    end
    
end
end
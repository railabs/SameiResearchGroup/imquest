% Copyright 20xx - 2019. Duke University
classdef imquest_MercuryPhantomAutoAnalyze < handle
    
    properties (SetAccess = immutable)
        
        %Meta data about the CT series
        CTSeriesSummary
        psize
        SliceThickness
        
        
    end
    
    properties
        sliceSize   %Vector of physical phantom size (nominal) for each slice (0 means background or transition slice)
        diameter    %Vector of estimated physical diameters for each slice
        NPSslice    %Logical array describing if each slice is suitible for NPS measurement
        TTFslice    %Logical array describing if each slice is suitible for TTF measurement
        orientation %Either "SmallToLarge" or "LargeToSmall". "SmallToLarge" means that the phantom sizes get larger as z increases. This is independent of slice ordering.
        z
        config      %Input configuration struct
        hasConfig = false
        
        TTFs        %nSizes X nInserts array of TTF objects
        TTFz        %TTFz object
        NPSs        %1 X nSizes array of NPS objects
        Dprimes     %nSizes X nInserts array of dprime objects
        measuredNPS %1 X nSizes boolean array saying if the NPS was measured at a given size
        measuredTTF
        measuredDprime
        measuredTTFz = false
        measuredFits = false
        fits        %1 x nInserts array of dprime vs size exponential fits
        gofs        %1 x nInserts array of goodness-of-fit objects from exponential fits
        
        %Phantom version
        phantomVersion = '3.0';     %This determines the geometry of the phantom, different versions have slightly different geometry which can be adjusted by simply changing this property
        phantomDiameterOverride=[]
        %Geometry constants
        Tol_diameter = 3;                           %�tolerance (mm) for classifying slices into a size
                                    
        
        %Insert constants
        InsertTheta = (0:360/5:360-360/5)*pi/180;   %Angle of inserts
        InsertDiameter = 25;                        %Diamter of the inserts
        T_D_TTFClassify = 3;                        %�tolerance (mm) used when classifying a slice as a TTF slice based segmenting the Landmark insert. (if estimated landmark insert diameter is within this tolerance, then the slice is assumed to be a TTF slice)
        
        
        %NPS constants
        NPS_ROI_Diameter = 30;                      %Size of NPS ROIs (mm)
        CenterHoleDiameter =   15;                  %Diameter of the central hole (needs to be avoided when making NPS measurements)
        NPS_ROI_Buffer = 10;                        %Amount of buffer (mm) between end of rod and edge of ROI
        nNPSROIs = 8;                               %Number of NPS ROIs (will distribute at constant angular increments at the same radius)
    
        %Radiodensity constants
        T_size = -500;                              %Threshold used to segment the phantom from the background
        HU_bkg  = -75;                              %Approximate background HU of the phantom
        InsertLandmark = 'Bone';                    %This insert's location will be automatically identified and then used to identify other inserts
        T_landmark = 200;                           %�HU threshold to segment out landmark insert before finding its center
        T_NPS = 100;                                %�HU threshold used to classify a slice as NPS or not (classifier looks at mean HU in an annulus and compares to HU_bkg � this threshold);
        T_NPS_Range = 190;                          %Max range of HU values allowed within NPS slice for it to be classified as NPS slice
        
        %zWedgeConstants
        zWedgeDistance = 33;                        %Distance from edge of TTF section of 2nd module to middle of wedge (MP 4.0 only)
        zWedgeROILength = 30;                       %Extent of the zWedge ROI in the z direction
        innerRodBuffer = 25;
        
    end
    
    properties (Constant = true)
        supportedPhantomVersions = {'3.0','4.0','Bare'};
    end
    
    properties (Transient = true)
        tool        %handle to the imquest object
    end
    
    properties (SetAccess = immutable, Hidden = true)
        x   %World coordinates
        y
        S   %3D size of image (in voxels)
        mA  %vector of mA values from each slice
        DICOMheaders
        
    end
    
    properties (Dependent = true, Hidden = true)
        
        parea  %Area (mm^2) of voxels (x-y plane)
        pvol   %volume (mm^3 of voxels
        iLand  %Index of landmark insert
        TTF_ROI_Diameter_pix
        Pos_NPS_ROI %Positions of NPS ROIs (in mm, before being shifted according to the actual phantom center)
        NPS_ROI_Diameter_pix
        TTFz_ROI_Diameter_pix
    end
    
    properties (Dependent = true)
        PhantomDiameters
        Inserts
        InsertLocationRadius
        HU_inserts
        hasZWedge
        
        insertArea                  %Area (mm^2) of the TTF inserts
        
        detectabilityIndex          %nSizes X nInserts array of dprime values
        alphas                      %1xnInserts array of alpha values from expoential fit of d' vs size d' = a*exp(b);
        betas                       %1xnInserts array of beta values from expoential fit of d' vs size
        R2s                         %1xnInserts array of R^2 values from expoential fit of d' vs size
        RMSEs                       %1xnInserts array of rmse values from expoential fit of d' vs size
        residuals                   %1xnInserts array of % absolute residuals from expoential fit of d' vs size
        PhantomDiametersForPlot
        resultsSummaryTable         %Puts dprime results in a table that can be copied into spreadsheet
        noiseSummaryTable           %Puts noise measurements in a table
        resolutionSummaryTable      %Puts TTF measurements in a table
        metaDataStr
        ny                          %Nyquist frequency
        
        SeriesDescription                               %Gets the series description from dicom header
    end
    
    events
    end
    
    
    methods
        
        function res = imquest_MercuryPhantomAutoAnalyze(tool,varargin) %Constructor
            oldStatus = tool.status;
            tool.status = 'Mercury Phantom Analysis:';
            
            switch length(varargin)
                case 1
                    if isa(varargin{1},'char')
                        res.phantomVersion = varargin{1};
                    elseif isa(varargin{1},'struct')
                        res.hasConfig=true;
                        res.config=varargin{1};
                        res.phantomVersion=res.config.phantomVersion;
                        res.phantomDiameterOverride=res.config.diameters';
                    end
                    
            end
            
            %Set the tool property
            res.tool=tool;
            
            %Get the meta data
            res.CTSeriesSummary=tool.CTSeriesSummary;
            res.DICOMheaders=tool.DICOMheaders;
            res.SliceThickness = tool.CTsliceThickness;
            res.psize=tool.CTpsize;
            res.S=tool.CTImageSize;
            [res.x,res.y,res.z]=getCTcoordinates(res.DICOMheaders);
            for i=1:res.S(3)
                mA(i,1) = res.DICOMheaders(i).XRayTubeCurrent;
            end
            res.mA = mA;
            
            %Set the window and level
            WL = get(res.tool.handles.CT_WLbutton(5),'UserData'); %This corresponds to the mercury phantom setting
            setWindowLevel(res.tool.handles.imtool,WL(1),WL(2));
            
            switch res.phantomVersion
                case {'3.0','4.0'}
                    if res.hasConfig
                        [res.sliceSize,res.NPSslice,res.TTFslice]= configClassifySlices(res);
                    else
                        %Classify the image slices
                        [res.sliceSize,res.NPSslice,res.TTFslice, res.diameter]=autoClassifySlices(res);
                    end
                    
                    
                    %Make measurements
                    res.makeMeasurements()
                case 'Bare'
            end
            
            showTempStatus(res.tool,'Mercury Phantom Analsysis: Done!')
            res.tool.status = oldStatus;
        end
        
        function makeMeasurements(res)
            %Make the d-prime measurements
            [res.TTFs, res.NPSs, res.Dprimes, res.measuredNPS, res.measuredTTF, res.measuredDprime] = makeDprimeMeasurements(res);
            
            %Make the exponential fits
            if sum(res.measuredDprime)>2
                try
                    [res.fits, res.gofs] = makeDprimevsSizeFits(res);
                    res.measuredFits = true;
                end
            end
            
            %Measure TTFz if needed
            if res.hasZWedge
                try
                    res.TTFz = res.makeTTFzmeasurement();
                    res.measuredTTFz = true;
                end
            end
        end
        
        function set.phantomVersion(res,phantomVersion)
            ind = find(strcmp(res.supportedPhantomVersions,phantomVersion));
            if ~isempty(ind)
                res.phantomVersion=phantomVersion;
            else
                warning(['Phantom version ' phantomVersion ' not supported, using version ' res.phantomVersion]); 
            end
            
        end
        
        function PhantomDiameters = get.PhantomDiameters(res)
            if res.phantomDiameterOverride
                PhantomDiameters=res.phantomDiameterOverride;
            else
                switch res.phantomVersion
                case '3.0'
                    PhantomDiameters = [120 185 230 300 370];
                case '4.0'
                   PhantomDiameters = [160 210 260 310 360];
                case 'Bare'
                    PhantomDiameters = [120 185 230 300 370];
                otherwise
                    PhantomDiameters = [];
                end
            end
            
        end
        
        function Inserts = get.Inserts(res)
            switch res.phantomVersion
                case '3.0'
                    Inserts  = {'Air','Iodine','Polystyrene','Bone','Water'};
                case '4.0'
                    Inserts = {'Air','Water','Bone','Polystyrene','Iodine'};
                case 'Bare'
                    Inserts  = {'Air','Iodine','Polystyrene','Bone','Water'};
                otherwise
                    Inserts = {};
            end
        end
        
        function InsertLocationRadius = get.InsertLocationRadius(res)
            switch res.phantomVersion
                case '3.0'
                    InsertLocationRadius = [33 50 50 50 50];
                case '4.0'
                    InsertLocationRadius = [45 45 45 45 45];
                case 'Bare'
                    InsertLocationRadius = [33 50 50 50 50];
                otherwise
                    InsertLocationRadius = [];
            end
        end
        
        function HU_inserts = get.HU_inserts(res)
            switch res.phantomVersion
                case '3.0'
                    HU_inserts= [-1000 200 -35 950 0];
                case '4.0'
                    HU_inserts = [-1000 0 950 -35 200];
                case 'Bare'
                    HU_inserts= [-1000 200 -35 950 0];
                otherwise
                    HU_inserts = [];
            end
        end
        
        function hasZWedge = get.hasZWedge(res)
            hasZWedge = false;
            if res.hasConfig
                if isfield(res.config,'TTFz')
                    hasZWedge=true;
                end
            else
                switch res.phantomVersion
                case '4.0'
                    hasZWedge = true;
                end
            end
            
        end
        
        function getOrientation(res)
            zs = [res.z(res.NPSslice); res.z(res.TTFslice)];
            zmin = min(zs); zmax = max(zs);
            ind = (res.z >= zmin) & (res.z<=zmax);
            zs = res.z(ind);
            ds = res.diameter(ind);
            c = polyfit(zs,ds,1);
            if c(1)<0
                orientation = "LargeToSmall";
            else
                orientation = "SmallToLarge";
            end
            res.orientation = orientation;
        end
        
        function [sliceSize,NPSslice,TTFslice]= configClassifySlices(res)
            config = res.config;
            sliceSize = zeros(size(res.z));
            NPSslice = false(size(res.z));
            TTFslice = false(size(res.z));
            z = res.z - config.landmark;
            if strcmp(config.orientation,'+z:-p')
                z=z*-1;
            end
            ST = res.DICOMheaders(1).SliceThickness;
            for j = 1:length(config.diameters)
                zmin = min([config.NPS.sliceRanges(j,1) config.TTF.sliceRanges(j,1)]);
                zmax = max([config.NPS.sliceRanges(j,2) config.TTF.sliceRanges(j,2)]);
                ind = z >= (zmin + ST/2) & z <= (zmax - ST/2);
                sliceSize(ind) = config.diameters(j);
                
                zmin=config.NPS.sliceRanges(j,1);
                zmax=config.NPS.sliceRanges(j,2);
                ind = z >= (zmin + ST/2) & z <= (zmax - ST/2);
                NPSslice(ind)=true;
                
                zmin = config.TTF.sliceRanges(j,1);
                zmax = config.TTF.sliceRanges(j,2);
                ind = z >= (zmin + ST/2) & z <= (zmax - ST/2);
                TTFslice(ind) = true;
                
            end
        end
        
        function [sliceSize,NPSslice,TTFslice,diameter] = autoClassifySlices(res) %Classifies slices
            oldStatus = res.tool.status;
            res.tool.status = 'Mercury Phantom Analysis: Classifying Slices';
            
            N = res.S(3); %Number of slices
            %Loop over slices
            for i=1:N
                %Get the ith image slice
                im = getImageSlices(res.tool.handles.imtool,i,i);
                
                %Segment the phantom from the background
                mask = segmentPhantomInSlice(res,im);
                
                %Calculate the area
                area = sum(mask(:))*res.parea;
                
                %Calculate diameter
                diameter(i,1) = 2*sqrt(area/pi);
                
            end
            sliceSize=diameter;
            
            %classify the slices by size
            t = res.Tol_diameter;
            classified = false(N,1);
            for i=1:length(res.PhantomDiameters)
                d=res.PhantomDiameters(i);
                
                %Classify by size
                ind = diameter > d-t & diameter <= d+t;
                sliceSize(ind) = d;
                classified(ind) = true;
                
            end
            
            %classify by TTF or NPS slice
            MTFslice = false(N,1);
            NPSslice = false(N,1);
            %Set up neural net slice classifier if its available
            useNeuralClassifier = false;
            dirname = 'MercurySliceClassifierNetwork';
            fnames = {'VGG19_architecture.json','VGG19_modelWeight.h5'};
            for i=1:length(fnames)
                useNeuralClassifier = true;
                if ~isfile([res.tool.paths.root dirname filesep fnames{i}])
                    useNeuralClassifier = false;
                end
            end
            if useNeuralClassifier
                res.tool.status = 'Mercury Phantom Analysis: Classifying Slices: Loading Neural Net Classifier';
                catNames = {'Airgap or Partial Volume','NPS','Outside Phantom','TTF','Tapered Section'};
                wmin = -1024;
                wmax = 1187;
                %warning('off','all') % the import utilities are still pretty rough
                net = importKerasNetwork([res.tool.paths.root dirname filesep fnames{1}],...
                    'WeightFile',[res.tool.paths.root dirname filesep fnames{2}],...
                    'OutputLayerType','classification',...
                    'Classes',categorical(1:length(catNames)));
                %warning('on','all')
                res.tool.status = 'Mercury Phantom Analysis: Classifying Slices';
            end
           
            
            for i=1:N
                if classified(i)
                    %Get the ith image slice
                    im = getImageSlices(res.tool.handles.imtool,i,i);
                    if useNeuralClassifier %Deep learning based classification
                        prev = i - 1;
                        if(prev==0)
                            prev = 1;
                        end
                        next = i + 1;
                        if(next>N)
                            next = N;
                        end
                        %Pre-processing
                        im_prev = getImageSlices(res.tool.handles.imtool,prev,prev);
                        im_next = getImageSlices(res.tool.handles.imtool,next,next);
                        img = cat(3,im_prev, im, im_next);
                        img = imresize(img,[256 256]);
                        img(img>wmax) = wmax;
                        img(img<wmin) = wmin;
                        img = uint8(((img-wmin)*255)/(wmax-wmin));
                        %Classify
                        label = classify(net,img);
                        k = renamecats(label,catNames);
                        isTTFslice = false;
                        isNPSslice = false;
                        switch k
                            case 'NPS'
                                isNPSslice = true;
                            case 'TTF'
                                isTTFslice = true;
                        end
                        
                    else
                        [isTTFslice,isNPSslice] = classifyTTFslice(res,im,sliceSize(i)); %Rule based classification
                    end
                    if isTTFslice
                        MTFslice(i) = true;
                    end
                    
                    if isNPSslice
                        NPSslice(i) = true;
                    end
                    
                end
                
            end
            
            sliceSize(~classified)=0;
            TTFslice=MTFslice;
            
            %Prune slices (only if neural classifier was not used)
            if ~useNeuralClassifier
                switch res.phantomVersion
                    case {'4.0', '3.0'}
                        validNPSSlicesExistAtDiameters = checkForValidSlicesAtEachDiameter(res,NPSslice, sliceSize);
                        slicetemp = conv(NPSslice, [1 1 1]/3.0,'same');
                        testNPSslice = (slicetemp >= 0.99); % allow for eps
                        testValidNPSSlicesExistAtDiameters = checkForValidSlicesAtEachDiameter(res,testNPSslice, sliceSize);
                        if sum(testValidNPSSlicesExistAtDiameters) >= sum(validNPSSlicesExistAtDiameters)
                            % winnowing process has not removed last valid slices at each diam, so use it
                            NPSslice = testNPSslice;
                            % try a second winnowing for NPS
                            slicetemp = conv(NPSslice, [1 1 1]/3.0,'same');
                            testNPSslice = (slicetemp >= 0.99); % allow for eps
                            testValidNPSSlicesExistAtDiameters = checkForValidSlicesAtEachDiameter(res,testNPSslice, sliceSize);
                            if sum( testValidNPSSlicesExistAtDiameters) >= sum (validNPSSlicesExistAtDiameters)
                                % winnowing process has not removed last valid slices at each diam, so use it
                                NPSslice = testNPSslice;
                            end
                        end
                        
                        validTTFSlicesExistAtDiameters = checkForValidSlicesAtEachDiameter(res,TTFslice, sliceSize);
                        slicetemp = conv(TTFslice, [1 1 1]/3.0,'same');
                        testTTFslice = (slicetemp >= 0.99); % allow for eps
                        testValidTTFSlicesExistAtDiameters = checkForValidSlicesAtEachDiameter(res,testTTFslice, sliceSize);
                        if sum(testValidTTFSlicesExistAtDiameters) >= sum(validTTFSlicesExistAtDiameters)
                            % winnowing process has not removed last valid slices at each diam, so use it
                            TTFslice = testTTFslice;
                        end
                end
            end
            
            res.tool.status = oldStatus;
        end
        
        function [validSlicesExistAtDiameters] = checkForValidSlicesAtEachDiameter(res,classifiedSliceVector, sliceSize)
            validSlicesExistAtDiameters = zeros(size(res.PhantomDiameters));
            for i=1:length(res.PhantomDiameters)
                d=res.PhantomDiameters(i);
                slicesAtDiameter = sliceSize == d;
                classifiedSlicesAtDiameter = classifiedSliceVector(slicesAtDiameter);
                validSlicesExistAtDiameters(i) = sum(classifiedSlicesAtDiameter) >=1;
            end 
        end
        
        function [TTFslice, NPSslice] = classifyTTFslice(res,im,d)
            diam=d;
            %Get center of phantom
            phantomCenter = findPhantomCenter(res,im,d);
            %Get index of phantom size
            iSize = find(d==res.PhantomDiameters);
            %Get the radius of insert locations
            r = res.InsertLocationRadius(iSize);
            %make a annulus mask
            [X,Y]=meshgrid(res.x-res.x(phantomCenter(1)),res.y-res.y(phantomCenter(2)));
            R=sqrt(X.^2+Y.^2);
            mask = R>=res.InsertLocationRadius(iSize)-(res.InsertDiameter/2+5) & R<=res.InsertLocationRadius(iSize)+res.InsertDiameter/2+5;
            HU=res.HU_inserts(res.iLand);
            mask2 = im>=HU-res.T_landmark & im<=HU+res.T_landmark & mask;
            
            %Estimate diameter of segmented circle
            a = sum(mask2(:))*res.parea;
            d = 2*sqrt(a/pi);
            
            %Classify as TTF slice or not
            if d>=res.InsertDiameter-res.T_D_TTFClassify && d<=res.InsertDiameter+res.T_D_TTFClassify
                TTFslice = true;
            else
                TTFslice = false;
            end
            
            %Set defualt NPS slice to true
            NPSslice = true;
            
            %Cannot be both NPS and TTF slice
            if TTFslice
                NPSslice = false;
            end
            
            %Rule out slice if part of the landmark insert is visible
            if d>res.T_D_TTFClassify
                NPSslice = false;
            end

            %Rule out slices whose average HU is too far off from expected
            mask3 = segmentPhantomInSlice(res,im);
            vals = im(mask3);
            m = mean(vals);
            if m <= res.HU_bkg - res.T_NPS
                NPSslice = false;
            end
            
            if strcmp(res.phantomVersion,"4.0")
                if diam==210
                    %Rule out wedge slices
                    if (m<15) && (m>-15)
                        NPSslice=false;
                    end
                end
            end
            
            %Rule out slices who's STD within annulus is much higher
            %compared to the Global noise index
            GNI = getGlobalNoiseIndexForImquest(im);
            vals = im(mask);
            if std(vals)>2*GNI
                NPSslice=false;
            end
            
            %Rule out slices which seem to have other inserts inside the
            %annulus
%             vals = im(mask);
%             if range(vals) >= res.T_NPS_Range
%                 NPSslice = false;
%             end
           
            
            
        end
        
        function [TTFs, NPSs, Dprimes, measuredNPS, measuredTTF, measuredDprime] = makeDprimeMeasurements(res)
            oldStatus = res.tool.status;
            
            %Loop over phantom sizes
            NPSs=imquest_NPS.empty;
            TTFs=imquest_TTF.empty;
            measuredNPS=false(size(res.PhantomDiameters));
            measuredTTF=false(size(res.PhantomDiameters));
            measuredDprime=false(size(res.PhantomDiameters));
            for i=1:length(res.PhantomDiameters)
                %Clear out last iteration's ROIs and results
                removeTTFROI(res.tool);
                removeNPSROI(res.tool);
                removeTTF(res.tool);
                removeNPS(res.tool);
                removeDprime(res.tool);
                
                %Make NPS measurements
                try
                    NPS = makeNPSmeasurement(res,res.PhantomDiameters(i));
                    NPSs(end+1,1)=NPS;
                    measuredNPS(i)=true;
                end
                try
                    TTF =  makeTTFmeasurements(res,res.PhantomDiameters(i));
                    TTFs(end+1,:) = TTF;
                    measuredTTF(i)=true;
                catch ME
                    res.tool.logError(ME)
                    removeTTFROI(res.tool)
                    removeTTF(res.tool)
                end


                %Make dprime measurements
                if measuredNPS(i) && measuredTTF(i)
                    res.tool.status = 'Mercury Phantom Analysis: Calculating d-prime';
                    measureDprime(res.tool);
                    clear dp
                    for j=1:length(res.Inserts)
                        dp(1,j) = copy(res.tool.Dprimes(j));
                    end
                    if all(~measuredDprime)
                        Dprimes=dp;
                    else
                        Dprimes = [Dprimes;dp];
                    end
                     measuredDprime(i)=true;
                end
                
                
                
            end
            if all(~measuredDprime)
                Dprimes=imquest_Dprime.empty;
            end
            
            res.tool.status = oldStatus;
            
        end
        
        function NPS = makeNPSmeasurement(res,d)
            oldStatus = res.tool.status;
            res.tool.status = 'Mercury Phantom Analysis: Measuring NPS';
            %get the slices of interest
            slices = find(res.NPSslice & res.sliceSize==d);
            %Set the slice range
            setNPSSliceRange(res.tool,[min(slices) max(slices)]);
            %change the displayed slice
            setCurrentSlice(res.tool.handles.imtool,slices(1));
            im = getImageSlices(res.tool.handles.imtool,min(slices),max(slices));
            %Take average image
            im = mean(im,3);
            
            %Place the ROIs
            findCenter=true;
            if res.hasConfig
                if isfield(res.config.NPS,'centers')
                    if any(res.config.NPS.centers.diameters==d)
                        row = find(res.config.NPS.centers.diameters==d);
                        cx = res.config.NPS.centers.centers(row,1);
                        cy = res.config.NPS.centers.centers(row,2);
                        findCenter=false;
                    end
                end
            end
            %Get the phantom center location (pixels)
            if findCenter
                center = findPhantomCenter(res,im,d);
                %Convert to world coordinates;
                cx = pixel2world(center(1),res.x);
                cy = pixel2world(center(2),res.y);
            end
            
            %Get the NPS ROI locations (mm)
            pos = res.Pos_NPS_ROI;
            %Shift the locations according to the center of the phantom
            pos(:,1)=pos(:,1)+cx;
            pos(:,2)=pos(:,2)+cy;
            %Convert to pixel coordinates
            pos(:,1)=world2pixel(pos(:,1),res.x);
            pos(:,2)=world2pixel(pos(:,2),res.y);
            for i=1:size(pos,1)
                addNPSROI(res.tool,[pos(i,:) res.NPS_ROI_Diameter_pix res.NPS_ROI_Diameter_pix]);
            end
            
            %Make NPS measurement
            measureNPS(res.tool);
            
            %Save measured NPS
            NPS = copy(res.tool.NPSs(end));
            
            %Remove NPS ROIs
            removeNPSROI(res.tool);
            
            res.tool.status = oldStatus;
            
        end
        
        function TTFs = makeTTFmeasurements(res,d)
            %This gets all TTF measurements for a give phantom size
            %d is the phantom diameter of interest
            oldStatus = res.tool.status;
            res.tool.status = 'Mercury Phantom Analysis: Measuring TTF';
            
            %Get the insert centers
            pos = getInsertCenters(res,d);
            
            %Set the slice range
            slices = find(res.TTFslice & res.sliceSize==d);
            setTTFSliceRange(res.tool,[min(slices) max(slices)]);
            
            %change the displayed slice
            setCurrentSlice(res.tool.handles.imtool,slices(1));
            
            %Loop over inserts, make ROI, measure TTF
            for i=1:size(pos,1)
                addTTFROI(res.tool,[pos(i,:) res.TTF_ROI_Diameter_pix res.TTF_ROI_Diameter_pix]);
                measureTTF(res.tool)
                TTFs(1,i)=copy(res.tool.TTFs(end));
                removeTTFROI(res.tool);
            end
            
            res.tool.status = oldStatus;
        end
        
        function TTFz = makeTTFzmeasurement(res)
            oldStatus = res.tool.status;
            res.tool.status = 'Mercury Phantom Analysis: Measuring TTF';
            removeTTFzROI(res.tool)
            removeTTFz(res.tool);
            
            findZwedge=true;
            if res.hasConfig
                if isfield(res.config,'TTFz')
                    config=res.config;
                    findZwedge=false;
                    zWedge = config.TTFz.zWedge;
                    z = res.z - config.landmark;
                    if strcmp(config.orientation,'+z:-p')
                        z=z*-1;
                    end
                    zMin=zWedge-res.zWedgeDistance/2;
                    zMax = zWedge + res.zWedgeDistance/2;
                    slices = find((z>=zMin) & (z <= zMax));
                    setTTFzSliceRange(res.tool,[min(slices) max(slices)]);
                    %Set the slice to the middle of the wedge
                    [~, ind] = min(abs(z - zWedge));
                    setCurrentSlice(res.tool.handles.imtool,ind);
                end
                
            end
            
            if findZwedge
                %Get phantom orientation
                res.getOrientation()

                %Get the location of the wedge
                d = res.PhantomDiameters;
                slices = find(res.TTFslice & res.sliceSize==d(2));
                z = res.z(slices);
                switch res.orientation
                    case "SmallToLarge"
                        zWedge = max(z) + res.zWedgeDistance;
                    case "LargeToSmall"
                        zWedge = min(z) - res.zWedgeDistance;
                end

                %Get the slices of interest
                zMin = zWedge - res.zWedgeDistance/2;
                zMax = zWedge + res.zWedgeDistance/2;
                slices = find((res.z>=zMin) & (res.z <= zMax));
                setTTFzSliceRange(res.tool,[min(slices) max(slices)]);

                %Set the slice to the middle of the wedge
                [~, ind] = min(abs(res.z - zWedge));
                setCurrentSlice(res.tool.handles.imtool,ind);
            end
            
            %Find the center of the phantom
            findCenter=true;
            if res.hasConfig
                if isfield(res.config,'TTFz')
                    if isfield(res.config.TTFz,'center')
                        cx=res.config.TTFz.center(1);
                        cy=res.config.TTFz.center(2);
                        cxp = world2pixel(cx,res.x);
                        cyp = world2pixel(cy,res.y);
                        center=[cxp,cyp];
                        findCenter=false;
                    end
                end
            end
            if findCenter
                im = getImageSlices(res.tool.handles.imtool,ind,ind);
                center = findPhantomCenter(res,im,res.diameter(ind));
            end

            %Make the ROI
            addTTFzROI(res.tool,[center res.TTFz_ROI_Diameter_pix res.TTFz_ROI_Diameter_pix])
            
            %Make measurement
            measureTTFz(res.tool);
            TTFz = copy(res.tool.TTFzs);
            
            %Clean up
            removeTTFzROI(res.tool);
            removeTTFz(res.tool);
            res.tool.status = oldStatus;
            
            
        end
        
        function [fits gofs] = makeDprimevsSizeFits(res)
            X = res.PhantomDiametersForPlot;
            Y = res.detectabilityIndex;
            for i=1:size(X,2)
                [fits{1,i}, gofs{1,i}]=fit(X(:,i),Y(:,i),'exp1');
            end
            
        end
        
        function TTF_ROI_Diameter_pix = get.TTF_ROI_Diameter_pix(res)
           TTF_ROI_Diameter_pix=res.InsertDiameter/res.tool.TTF_R/res.psize(1);
        end
        
        function TTFz_ROI_Diameter_pix = get.TTFz_ROI_Diameter_pix(res)
           TTFz_ROI_Diameter_pix=res.innerRodBuffer/res.tool.TTFz_R/res.psize(1);
        end
        
        function NPS_ROI_Diameter_pix = get.NPS_ROI_Diameter_pix(res)
            NPS_ROI_Diameter_pix=res.NPS_ROI_Diameter/res.psize(1);
        end
        
        function center = findCircleCenter(res,im,d,HU,mask)
            %Create a coordinate system (in pixels)
            [X, Y]=meshgrid(1:size(im,2),1:size(im,1));
            Cx = size(im,2)/2; Cy = size(im,1)/2;
            X=X-Cx; Y=Y-Cy;
            R = sqrt(X.^2+Y.^2);
            
            %Convert diameter to pixel units
            d=d/res.psize(1);
            
            %Shift the image such that its min is 0;
            HU_min = min(im(:));
            im=im-HU_min;
            
            %make circle image
            ind = R<=d/2;
            circle=zeros(size(im));
            circle(ind)=HU-HU_min;
            
            
            %Run filter in Fourier Domain
            MV = mean(im(:));
            im = im-MV;
            F=fftn(im);
            F(1)=0;
            H = fftn(circle);
            H=F.*H;
            cc = fftshift(real(ifftn(H)) + MV );
            cc(~mask)=0;
            [~, imax] = max(cc(:));
            [ypeak, xpeak] = ind2sub(size(cc),imax(1));
            center = ([xpeak ypeak]);
            
        end
        
        function center = findPhantomCenter(res,im,d)
            %Get mask of the phantom
            mask = segmentPhantomInSlice(res,im);
            %Find the phantom center
            center = findCircleCenter(res,im,d,res.HU_bkg,mask);
        end
        
        function center = getLandmarkCenter(res,im,phantomCenter,phantomDiameter)
            %Get index of phantom size
            iSize = find(phantomDiameter==res.PhantomDiameters);
            %Get the radius of insert locations
            r = res.InsertLocationRadius(iSize);
            %make an annulus mask
            [X,Y]=meshgrid(res.x-res.x(phantomCenter(1)),res.y-res.y(phantomCenter(2)));
            R=sqrt(X.^2+Y.^2);
            mask = R>=res.InsertLocationRadius(iSize)-(res.InsertDiameter/2+5) & R<=res.InsertLocationRadius(iSize)+res.InsertDiameter/2+5;
            %find the center of the landmark insert
            HU=res.HU_inserts(res.iLand);
            mask2 = im>=HU-res.T_landmark & im<=HU+res.T_landmark;
            mask2=imdilate(mask2,ones(7));
            im2=im;
            im2(~(mask2 & mask))=0; %Mask out everything not close to the bone HU
            center = findCircleCenter(res,im2,res.InsertDiameter,HU,mask);
            
        end
        
        function pos = getInsertCenters(res,d)
            %Get the slices of interest
            slices = find(res.TTFslice & res.sliceSize==d);
            im = getImageSlices(res.tool.handles.imtool,min(slices),max(slices));
            %Take average image
            im = mean(im,3);
                
            %Get the phantom center location
            findCenter=true;
            if res.hasConfig
                if isfield(res.config.TTF,'centers')
                    if any(res.config.TTF.centers.diameters==d)
                        row = find(res.config.TTF.centers.diameters==d);
                        cx = res.config.TTF.centers.centers(row,1);
                        cy = res.config.TTF.centers.centers(row,2);
                        cxp = world2pixel(cx,res.x);
                        cyp = world2pixel(cy,res.y);
                        center=ceil([cxp,cyp]);
                        findCenter=false;
                    end
                end
            end
            if findCenter
                center = findPhantomCenter(res,im,d);
            end
            
            
            %Get the landmark center
            findLandmark=true;
            if res.hasConfig
                if isfield(res.config.TTF,'landmarks')
                    if any(res.config.TTF.landmarks.diameters==d)
                        row = find(res.config.TTF.landmarks.diameters==d);
                        cx = res.config.TTF.landmarks.centers(row,1);
                        cy = res.config.TTF.landmarks.centers(row,2);
                        cxp = world2pixel(cx,res.x);
                        cyp = world2pixel(cy,res.y);
                        pos_land=ceil([cxp,cyp]);
                        findLandmark=false;
                    end
                end
            end
            if findLandmark
                pos_land = getLandmarkCenter(res,im,center,d);
            end
            
            
            %Calculate angular rotation of landmark compared to template
            x=res.x(pos_land(1))-res.x(center(1));
            y=res.y(pos_land(2))-res.y(center(2));
            [theta,~]=cart2pol(x,y);
            dth = theta-res.InsertTheta(res.iLand);
            
            %Calculate positions of each insert
            thetas = res.InsertTheta+dth;
            iSize = find(d==res.PhantomDiameters);
            rpix = res.InsertLocationRadius(iSize)/res.psize(1);
            for i=1:length(thetas)
                [x,y]=pol2cart(thetas(i),rpix);
                x=x+center(1); y=y+center(2);
                pos(i,:)=[x y];
            end
            
            
            
            
        end
        
        function mask = segmentPhantomInSlice(res,im)
            mask = im>res.T_size;
            NHOOD = ones(9);
            mask = imdilate(imerode(mask,NHOOD),NHOOD); %Dilate errode the mask
            mask = imfill(mask,'holes'); %Fill holes
        end
        
        function area = get.insertArea(res)
            r = res.InsertDiameter/2; %Radius
            area = pi*(r^2);
        end
        
        function parea = get.parea(res)
            parea = prod(res.psize);
        end
        
        function pvol = get.pvol(res)
            pvol = res.parea*res.SliceThickness;
        end
        
        function iLand = get.iLand(res)
            iLand = find(strcmp(res.Inserts,res.InsertLandmark));
        end
        
        function pos = get.Pos_NPS_ROI(res)
            %Get the radius of the locations for the ROIs
            r = res.CenterHoleDiameter/2 + res.NPS_ROI_Buffer + res.NPS_ROI_Diameter/2;
            dth = 360/res.nNPSROIs;
            theta = (0:dth:360-dth)'; theta = theta*pi/180;
            r=repmat(r,size(theta));
            [x,y]=pol2cart(theta,r);
            pos=[x y];
        end
        
        function dp = get.detectabilityIndex(res)
            for i=1:size(res.Dprimes,1)
                for j=1:size(res.Dprimes,2)
                    dp(i,j)=res.Dprimes(i,j).detectabilityIndex;
                end
            end
        end
        
        function a = get.alphas(res)
            a=[];
            for i=1:length(res.fits)
                a(1,i) = res.fits{i}.a;
            end
        end
        
        function b = get.betas(res)
            b=[];
            for i=1:length(res.fits)
                b(1,i) = res.fits{i}.b;
            end
        end
        
        function r2 = get.R2s(res)
            for i=1:length(res.gofs)
                r2(1,i) = res.gofs{i}.rsquare;
            end
        end
        
        function RMSEs = get.RMSEs(res)
            for i=1:length(res.gofs)
                RMSEs(1,i) = res.gofs{i}.rmse;
            end
        end
        
        function residuals = get.residuals(res)
            X = res.PhantomDiametersForPlot;
            Y = res.detectabilityIndex;
            Y2=zeros(size(Y));
            if isempty(res.fits)
                residuals = nan(1,size(Y,2));
            else
                for i=1:size(X,2)
                    Y2(:,i) = feval(res.fits{i},X(:,i));
                end
                residuals = mean(100*abs(Y-Y2)./Y,1);
            end
            
        end
       
        function d = get.PhantomDiametersForPlot(res)
            d = res.PhantomDiameters(res.measuredDprime)';
            d = repmat(d,[1 length(res.Inserts)]);
        end
        
        function ny = get.ny(res)
            ny = 1./(2*res.psize);
        end
        
        function tab = get.resultsSummaryTable(res)
            
            dp = res.detectabilityIndex; %nSize x nInserts
            alphas = res.alphas;
            if isempty(alphas)
                alphas=nan(1,size(dp,2));
            end
            betas = res.betas;
            if isempty(betas)
                betas=nan(1,size(dp,2));
            end
            resids = res.residuals;
            
            for i=1:length(res.Inserts)
               tab(i).Insert = res.Inserts{i};
               tab(i).a=alphas(i);
               tab(i).b=betas(i);
               tab(i).residual=resids(i);
               for j = 1:size(dp,1)
                   tab(i).(['Dprime_' num2str(res.PhantomDiameters(j))])=dp(j,i);
               end
            end
            
            tab = struct2table(tab);
            
        end
        
        function tab = get.noiseSummaryTable(res)
            d = res.PhantomDiameters(res.measuredNPS);
            for i=1:length(d)
                tab(i).PhantomDiameter = d(i);
                tab(i).noise = res.NPSs(i).noise;
                tab(i).fpeak = res.NPSs(i).fpeak;
                tab(i).fav = res.NPSs(i).fav;
            end
            tab = struct2table(tab);
        end
        
        function tab = get.resolutionSummaryTable(res)
            d = res.PhantomDiameters(res.measuredTTF);
            n = 0;
            for j=1:size(res.TTFs,2)
                for i=1:size(res.TTFs,1)
                    n=n+1;
                    tab(n).Insert = res.Inserts{j};
                    tab(n).PhantomDiameter = d(i);
                    tab(n).MeasuredContrast = res.TTFs(i,j).contrast;
                    tab(n).f10 = res.TTFs(i,j).f10;
                    tab(n).f50 = res.TTFs(i,j).f50;
                    tab(n).CNR_total=res.TTFs(i,j).stats.CNR_total;
                    
                end
            end
            tab = struct2table(tab);
        end
        
        function im = plotDprimeVsSize(res)
            xSize = 7.5;
            ySize = xSize*.5;
            fig = createFigForPrinting(xSize,ySize);
            set(fig,'Visible','off');
            X = res.PhantomDiametersForPlot;
            Y = res.detectabilityIndex;
            h=plot(X,Y,'o'); hold on;
            
            if res.measuredFits
                alphas = res.alphas;
                betas = res.betas;
                r2 = res.R2s;
                x = linspace(res.PhantomDiameters(1),res.PhantomDiameters(end));
                for i=1:length(h)
                    y = feval(res.fits{i},x);
                    plot(x,y,'-','Color',h(i).Color);
                    leg{i} = [res.Inserts{i} ', a = ' sprintf('%2.0f',alphas(i)) ', b = ' sprintf('%.3e',betas(i)) ', R^2 = '  sprintf('%.3f',r2(i))];
                end
            else
                for i = 1:length(h)
                    leg{i} = res.Inserts{i};
                end  
            end
            legend(leg);
            
            set(gca,'XTick',res.PhantomDiameters);
            grid on;
            xlabel('Phantom Diameter [mm]');
            ylabel('Detectability Index');
            
            im = print(fig, '-RGBImage');
            delete(fig);
        end
        
        function im = plotmAProfile(res)
            %Get mA profile image
            fig = savemAProfileReportFigure(res.tool);
            
            ylims = get(gca,'YLim');
            %Show NPS and MTF locations
            for i=1:length(res.PhantomDiameters)
                %Get the MTF slices
                if res.measuredTTF(i)
                    ind = res.TTFslice & res.sliceSize == res.PhantomDiameters(i);
                    z = res.z(ind); w = range(z);
                    p = patch([min(z) min(z) max(z) max(z)],[min(ylims) max(ylims) max(ylims) min(ylims)],'g','FaceAlpha',.1);
                end
                
                if res.measuredNPS(i)
                    %Get the NPS slices
                    ind = res.NPSslice & res.sliceSize == res.PhantomDiameters(i);
                    z = res.z(ind); w = range(z);
                    p = patch([min(z) min(z) max(z) max(z)],[min(ylims) max(ylims) max(ylims) min(ylims)],'y','FaceAlpha',.1);
                end
                
                
            end
            
            if res.measuredTTFz
                slices = res.TTFz.slices(1):res.TTFz.slices(2);
                z = res.z(slices);
                w = range(z);
                p = patch([min(z) min(z) max(z) max(z)],[min(ylims) max(ylims) max(ylims) min(ylims)],'b','FaceAlpha',.1);
                
            end
            
            title('');
            im = print(fig, '-RGBImage');
            delete(fig);
        end
        
        function im = plotNPS(res)
            %Make figure
            xSize = 7.5;
            ySize = xSize*.6;
            fig = createFigForPrinting(xSize,ySize);
            set(fig,'Visible','off');
            
            %Make 2D NPS plots
            n = length(res.NPSs);
            ny = res.ny;
            d = res.PhantomDiameters(res.measuredNPS);
            for i=1:n
                subplot(2,n,i)
                h=imshow(mat2gray(res.NPSs(i).NPSinfo.NPS_2D),'XData',res.NPSs(i).NPSinfo.fx,'YData',res.NPSs(i).NPSinfo.fy);
                if i==1
                    axis on
                    set(gca,'XTick',get(gca,'Xlim'),'YTick',get(gca,'Ylim'))
                end
                
                X(:,i) = res.NPSs(i).f;
                Y(:,i) = res.NPSs(i).NPS;
                Y2(:,i) = res.NPSs(i).NPS./(res.NPSs(i).noise^2);
                leg{i} = [num2str(d(i)) ' mm'];
                title(leg{i});
            end
            
            %Make 1-D NPS plots
            subplot(2,2,3)
            plot(X,Y);
            legend(leg);
            ylabel('NPS [mm^2HU^2]')
            xlabel('Spatial Frequency [mm^-^1]')
            title('Noise Power Spectra');
            xlim([0 ny(1)]);
            subplot(2,2,4)
            plot(X,Y2);
            legend(leg);
            ylabel('nNPS [mm^2]')
            xlabel('Spatial Frequency [mm^-^1]')
            title('Normalized Noise Power Spectra');
            xlim([0 ny(1)]);
            
            im = print(fig, '-RGBImage');
            delete(fig);
        end
        
        function im = plotTTF(res)
            
            %Make Figure
            xSize = 7.5;
            ySize = xSize*1.2;
            fig = createFigForPrinting(xSize,ySize);
            set(fig,'Visible','off');
            
            n=0;
            ny = res.ny(1);
            ymax = 1;
            d = res.PhantomDiameters(res.measuredTTF);
            for j=1:size(res.TTFs,2)
                a(j)=subplot(size(res.TTFs,2),1,j);
                leg = {};
                for i=1:size(res.TTFs,1)
                    
                    n=n+1;
                    %a(i,j) = subplot(size(res.TTFs,2),size(res.TTFs,1),n);
                    x = res.TTFs(i,j).f; y = res.TTFs(i,j).TTF;
                    plot(x,y);
                    hold on
                    
                    if max(y) > ymax
                        ymax = max(y);
                    end
                    xlim([0 ny]);
                    
                    set(gca,'XTick',0:.2:ny,'YTick',0:.25:1)
                    leg{end+1}=[num2str(d(i)) ' mm'];
                    
%                     if j==1
%                         title([num2str(d(i)) ' mm']);
%                     end
                    
                    if i == 1
                        str = {[res.Inserts{j}],'TTF'};
                        ylabel(str)
                    else
                        set(gca,'YTickLabel','');
                    end
                    
                    if j ~= size(res.TTFs,2)
                        set(gca,'XTickLabel','')
                    else
                    end
                    
                    grid on
                    
                    
                end
                legend(leg)
            end
            for i=1:numel(a)
                set(a(i),'Ylim',[0 ymax]);
            end
            
            annotation('textbox',[.05 .025 .95 .975],'String','Spatial Frequency [mm^-^1]',...
                'HorizontalAlignment','Center','VerticalAlignment','bottom',...
                'EdgeColor','none','FontSize',12)
            
            im = print(fig, '-RGBImage');
            delete(fig);
            
        end
        
        function im = plotTTFz(res)
            %Make figure
            xSize = 7.5;
            ySize = xSize*.4;
            fig = createFigForPrinting(xSize,ySize);
            set(fig,'Visible','off');
            
            %Get nyquist
            ny = 1/(2*res.TTFz.sliceThickness);
            
            %Make plots
            subplot(1,2,1)
            plot(res.TTFz.f,res.TTFz.TTF)
            hold on;
            plot(res.TTFz.f,res.TTFz.IdealTTF)
            xlim([0 ny])
            ylabel('TTFz')
            xlabel('Spatial Frequency [mm^-^1]')
            legend({'TTFz','Ideal sinc(f)'})
            title('z-direction TTF');
            
            subplot(1,2,2)
            plot(res.TTFz.stats.ESF.distance,res.TTFz.stats.ESF.ESF);
            ylabel('Edge spread function [HU]')
            xlabel('Distance from edge [mm]')
            title('z-direction ESF');
            
            im = print(fig, '-RGBImage');
            delete(fig);
        end
        
        function t = get.metaDataStr(res)
            t={};
            t{end+1}=['Manufacturer: ' res.Manufacturer];
            t{end+1}=['Model: ' res.ManufacturerModelName];
            t{end+1}=['Series Description: ' res.SeriesDescription];
            t{end+1}=['Slice Thickness: ' num2str(res.SliceThickness) ' mm'];
            t{end+1}=['DFOV: ' num2str(res.ReconstructionDiameter) ' mm'];
            t{end+1}=['Kernel: ' res.ConvolutionKernel];
            t{end+1}=['Acquisition Date: ' datestr(datenum(res.AcquisitionDate,'yyyymmdd'))];
            t{end+1}=['Series Date: ' datestr(datenum(res.SeriesDate,'yyyymmdd'))];
            t{end+1}=['Irradiation Event UID: ' res.IrradiationEventUID];
            t{end+1}=['Series Instance UID: ' res.SeriesInstanceUID];
            t=t';
        end
        
        function t = get.SeriesDescription(res)
            try
                t = res.DICOMheaders(1).SeriesDescription;
            catch
                t = 'UnknownSeriesDescription';
            end
        end
        
        function generateReport(res,output)
            res.tool.status = 'Mercury Analysis: Generating report';
            
            %Make it compiler compatable if needed
            if isdeployed
                makeDOMCompilable
            end
            
            %Import the API
            import mlreportgen.dom.*;
            
            %Set some constants
            ROIimSize={Width('65%'),Height('65%')};
            toDelete={};
            %Create the page margins object
            margins=PageMargins;
            margins.Top='0.5in';
            margins.Bottom='0.5in';
            margins.Left='0.5in';
            margins.Right='0.5in';
            margins.Header='0.0in';
            margins.Footer='0.0in';
            
            %Create the document object
            d = Document(output,'pdf');
            
            %Create title
            t=Text('imQuest Mercury Phantom Report');
            t.Bold=true;
            t.FontSize='24';
            append(d,t);
            t=Text(['Report generated on ' date]);
            t.FontSize='11';
            append(d,t);
            append(d,HorizontalRule());
            d.CurrentPageLayout.PageMargins=margins;
            %Table of contents
            t = TOC(2,' ');
            t.Style = {FontSize('11pt')};
            append(d,t);
            
                        
            %Series info section
            h=Heading1('Series Info');
            append(d,h);
            append(d,HorizontalRule());
            str=res.CTSeriesSummary;
            clear t
            for i=1:length(str)
                t{i} = Text(str{i});
                t{i}.FontSize = '12';
            end
            t = UnorderedList(t);
            t.Style = {LineSpacing(1)};
            append(d,t);
            append(d,PageBreak);
            
            
            %Dprime vs size section
            h=Heading1('Detectability Index vs Phantom Size');
            append(d,h);
            append(d,HorizontalRule());
            if any(res.measuredDprime)
                im = plotDprimeVsSize(res);
                io=makeDocImageObject(im,output);
                io.Style = {Width('7in'),HAlign('left')};
                append(d,io);
                toDelete{end+1}=io.Path;
                d.CurrentPageLayout.PageMargins=margins;
                tab = res.resultsSummaryTable;
                header = tab.Properties.VariableNames;
                for i=1:size(tab,1)
                    for j=1:size(tab,2)
                        switch j
                            case 1
                                str = tab.Insert{i};
                            case 2
                                str = sprintf('%.1f',tab{i,j});
                            case 3
                                str = sprintf('%0.3e',tab{i,j});
                            case 4
                                str = sprintf('%.2f',tab{i,j});
                            otherwise
                                str = sprintf('%.1f',tab{i,j});
                        end
                        out{i,j} = str;
                    end
                end
                t=FormalTable(header,out);
                t.Body.Style = {FontSize('8')};
                t.Border='hidden';
                t.Body.ColSep='dashed';
                t.TableEntriesHAlign='Right';
                t.TableEntriesInnerMargin='6pt';
                t.Header.Style={Bold,FontSize('8')};
                append(d,t);
            else
                t=Text('Could not measure any detectability indices');
                append(d,t);
            end
            append(d,PageBreak);
            
            %mA profile section
            h=Heading1('Tube Current Profile');
            append(d,h);
            append(d,HorizontalRule());
            im = plotmAProfile(res);
            io=makeDocImageObject(im,output);
            io.Style = {Width('7in'),HAlign('left')};
            append(d,io);
            toDelete{end+1}=io.Path;
            append(d,PageBreak);
            
            %NPS section
            h=Heading1('Noise');
            append(d,h);
            append(d,HorizontalRule());
            if any(res.measuredNPS)
                im = plotNPS(res);
                io=makeDocImageObject(im,output);
                io.Style = {Width('7in'),HAlign('left')};
                append(d,io);
                toDelete{end+1}=io.Path;
                tab = res.noiseSummaryTable;
                header = {'Phantom Diameter [mm]','Noise [HU]','fpeak [mm^-1]','fav [mm^-1]'};
                clear out
                for i=1:size(tab,1)
                    for j=1:size(tab,2)
                        switch j
                            case 1
                                str = tab{i,j};
                            case 2
                                str = sprintf('%.1f',tab{i,j});
                            case {3,4}
                                str = sprintf('%.2f',tab{i,j});
                        end
                        out{i,j} = str;
                    end
                end
                t=FormalTable(header,out);
                t.Body.Style = {FontSize('8')};
                t.Border='hidden';
                t.Body.ColSep='dashed';
                t.TableEntriesHAlign='Right';
                t.TableEntriesInnerMargin='6pt';
                t.Header.Style={Bold,FontSize('8')};
                append(d,t);
            else
                t=Text('Could not measure any NPSs');
                append(d,t);
            end
            append(d,PageBreak);
            
            %TTF section
            h=Heading1('In-plane Spatial Resolution');
            append(d,h);
            append(d,HorizontalRule());
            if any(res.measuredTTF)
                im = plotTTF(res);
                io=makeDocImageObject(im,output);
                io.Style = {Width('7in'),HAlign('left')};
                append(d,io);
                toDelete{end+1}=io.Path;
                append(d,PageBreak);
                tab=res.resolutionSummaryTable;
                header = {'Insert','Phantom Diameter [mm]','Contrast [HU]','f10 [mm^-1]','f50 [mm^-1]','Total_CNR'};
                clear out
                TTFwarning=false;
                for i=1:size(tab,1)
                    for j=1:size(tab,2)
                        switch j
                            case 1
                                str = tab.Insert{i};
                                if tab{i,6}<15
                                    str = [str '*'];
                                    TTFwarning=true;
                                end
                            case 2
                                str = tab{i,j};
                            case {3,6}
                                str = sprintf('%.1f',tab{i,j});
                            otherwise
                                str = sprintf('%.2f',tab{i,j});

                        end
                        out{i,j} = str;
                    end
                end
                t=FormalTable(header,out);
                t.Body.Style = {FontSize('8')};
                t.Border='hidden';
                t.Body.ColSep='dashed';
                t.TableEntriesHAlign='Right';
                t.TableEntriesInnerMargin='6pt';
                t.Header.Style={Bold,FontSize('8')};
                append(d,t);
                
                if TTFwarning
                    t = Text('* Warning: Total CNR was below 15 for this TTF measurement. Result could be unreliable');
                    append(d,t);
                    t=Text('See Chen, B., Christianson, O., Wilson, J. M., & Samei, E. (2014). Assessment of volumetric noise and resolution performance for linear and nonlinear CT reconstruction methods. Medical Physics, 41(7)');
                    append(d,t);
                end
                
            else
                t=Text('Could not measure any TTFs');
                append(d,t);
            end
            append(d,PageBreak);
            
            h=Heading1('Z-direction spatial resolution');
            append(d,h);
            append(d,HorizontalRule());
            if res.measuredTTFz
                im = res.plotTTFz();
                io=makeDocImageObject(im,output);
                io.Style = {Width('7in'),HAlign('left')};
                append(d,io);
                toDelete{end+1}=io.Path;
                t = res.TTFz.fullLabel;
                t(1)=[];
                t(7)=[];
                t = UnorderedList(t);
                t.Style = {LineSpacing(1.5), FontSize('11pt')};
                append(d,t);
                
            else
                 t=Text('Could not measure any TTFzs');
                 append(d,t);
            end
            append(d,PageBreak);
            
            %Imquest version info
            h=Heading1('imQuest Version Info');
            append(d,h);
            append(d,HorizontalRule());
            tab = res.tool.versionInfoTable;
            clear t
            t{1}=Text(['Git branch: ' tab.BranchName ', Commit ID: ' tab.GitID]);
            t{1}.FontSize = '14';
            t = UnorderedList(t);
            append(d,t);
            
            %Close the document (This saves it to a file)
            close(d);
            
            %Delete the temp files
            for i=1:length(toDelete)
                delete(toDelete{i});
            end
            
            res.tool.status = 'Mercury Analysis: Opening Report';
            rptview(output,'pdf');
            showTempStatus(res.tool,'Mercury Analysis: Done Generating Report!')
            res.tool.status = res.tool.defaultStatus;
            
        end
        
        
    end
    
end

function pos_mm = pixel2world(xi,x) %xi is pixel coordinate of interest, x is world coordinates
%Convert from pixel coordinates to world coordinates
pos_mm = interp1(1:length(x),x,xi);
end

function pos_pix = world2pixel(xi,x) %xi is world coordinate of interest, x is world coordinates
%Convert from world coordinates to pixel coordinates
pos_pix = interp1(x,1:length(x),xi);
end

function io = makeDocImageObject(im,output)

[PATHSTR,NAME,EXT] = fileparts(output);

import mlreportgen.dom.*;
fname=[PATHSTR filesep dicomuid '.png']; %generate a uid
imwrite(im,fname); %Write the image
io = Image(fname); %Make the object
io.Style={ScaleToFit};

end

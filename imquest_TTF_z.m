% Copyright 20xx - 2019. Duke University
classdef imquest_TTF_z < imquest_TTF
    
    methods
        function TTFhandle = imquest_TTF_z(varargin)
            switch nargin
                case 1 %This is the constructor used when making copies of the object
                    TTF = varargin{1};
                    TTFhandle.withGraphics = false;
                    TTFhandle.type=TTF.type;
                    TTFhandle.f=TTF.f;
                    TTFhandle.TTF=TTF.TTF;
                    TTFhandle.contrast = TTF.contrast;
                    TTFhandle.graphicHandle=[];
                    TTFhandle.tool=[];
                    TTFhandle.settings=TTF.settings;
                    TTFhandle.stats=TTF.stats;
                    TTFhandle.slices = TTF.slices;
                    TTFhandle.psize=TTF.psize;
                    TTFhandle.sliceThickness=TTF.sliceThickness;
                    TTFhandle.SeriesInstanceUID=TTF.SeriesInstanceUID;
                    TTFhandle.versionInfoTable=TTF.versionInfoTable;
                    
                case 2 %This is the normal constructor
                    tool = varargin{1};
                    TTFROI = varargin{2};
                    TTFhandle.withGraphics = true;
                    TTFhandle.type='Z';
                    TTFhandle.tool=tool;
                    
                    %Get the slices of interest
                    [im, TTFhandle.slices] = getImageSlices(tool,tool.handles.TTFzsliceSelector);
                    
                    %Get the DICOM series UID
                    TTFhandle.SeriesInstanceUID = tool.DICOMheaders(TTFhandle.slices(1)).SeriesInstanceUID;
                    
                    %Get the 3D pixel size
                    psize = [tool.CTpsize' tool.CTsliceThickness];
                    TTFhandle.psize=psize(1);
                    TTFhandle.sliceThickness=tool.CTsliceThickness;
                    
                    
                    %Define the ROI
                    InnerCircleROI = getMatchingTTFzInnerCircleROI(tool,TTFROI);
                    pos = getPosition(TTFROI);
                    cx = pos(1); cy = pos(2); cz = getCurrentSlice(tool.handles.imtool)-TTFhandle.slices(1)+1;
                    p= InnerCircleROI.getInnerPosition;
                    p=[p(3)*psize(1) p(4)*psize(2)]/2;
                    Rmin = mean(p);
                    Rmax = mean(psize(1:2).*pos(3:4)/2); %size of outer ROI in mm
                    Rz = (diff(TTFhandle.slices)+1)*psize(3)/2;
                    ROI = [cx cy cz Rmin Rmax Rz];
                    
                    %Compute the TTF
                    [TTFhandle.f, TTFhandle.TTF, TTFhandle.contrast, TTFhandle.settings, TTFhandle.stats] = getZTTF(im,ROI,psize,'Verbose',false);
                    
                    %make the plot
                    TTFhandle.withGraphics = true;
                    TTFhandle.graphicHandle = plot(TTFhandle.f,TTFhandle.TTF,'Parent',tool.handles.TTFzaxis,'LineWidth',TTFhandle.lineWidth(1),'MarkerSize',12);
                    
                    %Get the imquest version info
                    TTFhandle.versionInfoTable=tool.versionInfoTable;
                    
                    %add to the imquest list of measured TTFs
                    addTTFz(tool,TTFhandle);
            end
        end
        
        function newTTF = copy(TTF)
            newTTF = imquest_TTF_z(TTF);
        end
    end
end
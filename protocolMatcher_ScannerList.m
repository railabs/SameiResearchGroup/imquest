% Copyright 20xx - 2019. Duke University
classdef protocolMatcher_ScannerList < handle
    %A self-contained dropdown box and listbox for letting the user pick a
    %scanner model and recon setting

    
    properties
        tool            %handle to the parent protocolMatcher object
        handles         %structured variable with all the graphics handles
        rows            %Vector of rows correpsonding to the rows in tool.table, sorted by min to max distance
        referenceList   %the reference list that this list is being compared to (if empty then this list is the reference list)
        model           %currently selected scanner model
        color
        barWidth = .5;
        n
        TTF
        NPS
    end
    
    
    
    properties (Dependent = true)
        value           %integer denoting the currently selected item in the listbox
        row             %integer of the table row corresponding to the currently selected item
        referenceRow    %integer of the table row corresponding to the currerntly selected item in the reference list (empty if the object is a refernce list)
        distance        %Value of the distance between the currently selected item and the reference
        isReference     %boolean that's true if this list is a reference
        TTFMetric
        NPSMetric
        dose            %Dose at which the noise is equal to the reference condition at the reference dose
        noise           %Noise of reference condition at reference dose
        
        NPS1D
        label           %String label for the currently selected item in the list
        DoseLabel       
        noise1D         %1D vector of noise as a function of dose (dose vector comes from the doses property of the tool obj). 
    end
    
    
    events
        newModel        %happens with the user chooses a difference model
        newRows
        newSelection
        
    end
    
    methods
        
        function list = protocolMatcher_ScannerList(tool,parent,model,referenceList,position,color,n,varargin) %Constructor
            %set the main properties
            list.tool=tool;
            list.handles.parent=parent;
            
            list.referenceList=referenceList;
            
            %Create the panel
            list.handles.panel = uipanel(parent,'Position',...
                position,'Title','','BackgroundColor','k',...
                'ForegroundColor',color,'HighlightColor','k','TitlePosition',...
                'lefttop','FontSize',12,'ShadowColor',color);
            
            %Create the pulldown menu
            ind=find(strcmp(tool.models,model));
            fun=@(hObject,evnt) callbacks(hObject,evnt,list);
            list.handles.menu= uicontrol(list.handles.panel,'Style','popupmenu',...
                'String',tool.models,'Value',ind,'Units','normalized','Position',...
                [0 .95 1 .05],'Callback',fun);
            list.model=model;
            
            %Get the rows
            updateRows(list);
            
            %Create the list
            list.handles.list= uicontrol(list.handles.panel,'Style','listbox','String',...
                tool.tab{list.rows,'ReconLabel'},'Value',1,'Units','normalized','Position',[0 0 1 .95],...
                'BackgroundColor','k','ForegroundColor','w','Callback',fun);
            
            %Create the scatter object
            list.color=color;
            if list.isReference
                sizeData=200;
            else
                sizeData=100;
            end
            list.handles.scatter = scatter(tool.handles.TTFNPSAxis,list.TTFMetric,list.NPSMetric,'o',...
                'MarkerEdgeColor','none','MarkerFaceColor',list.color,'SizeData',sizeData);
            
            %Create the bar object (and text object to go along with it
            list.n=n;
            list.handles.bar = bar(tool.handles.DoseAxis,n,list.dose,list.barWidth,'EdgeColor','w','FaceColor',list.color);
            list.handles.bar_text = text(tool.handles.DoseAxis,n,list.dose+.5,list.DoseLabel,...
                'Color','w','HorizontalAlignment','center','VerticalAlignment','bottom',...
                'EdgeColor','w','BackgroundColor','k');
            
            %Set the NPS and TTF objects
            updateTTFNPSObjects(list);
            
            %Create the TTF, NPS and dose vs noise plots
            list.handles.TTFplot = plot(list.TTF.f,list.TTF.TTF,'Color',list.color,'LineWidth',2,'Parent',tool.handles.TTFAxis);
            list.handles.NPSplot = plot(list.NPS.f,list.NPS1D,'Color',list.color','LineWidth',2,'Parent',tool.handles.NPSAxis);
            list.handles.Noiseplot = plot(list.tool.doses,list.noise1D,'Color',list.color,'LineWidth',2,'Parent',tool.handles.NoiseDose);
            list.handles.Noiseplot_l = plot([0 list.dose list.dose],[list.noise list.noise 0 ],'-','Color',[.5 .5 .5],'LineWidth',1,'Parent',tool.handles.NoiseDose);
            
            %Create the image objects
            list.handles.im = imshow(mat2gray(list.tool.CDim.im),'Parent',tool.handles.imAxis(list.n));
            set(tool.handles.imAxis(list.n),'Visible','on');
            
            
            %Add the listeners
            addlistener(list,'newModel',@list.handlePropEvents);
            addlistener(list,'newRows',@list.handlePropEvents);
            addlistener(list,'newSelection',@list.handlePropEvents);
            if ~list.isReference
                addlistener(list.referenceList,'newSelection',@list.handlePropEvents); %Listen for changes to the reference list
            end
            addlistener(tool,'distanceUpdated',@list.handlePropEvents); %Listen for new distance metric
            addlistener(tool.CDim,'ImageUpdated',@list.handlePropEvents);%Listen for changes to the CD image
            addlistener(tool,'refValuesChaged',@list.handlePropEvents); %Listen for new reference settings (e.g., ref dose, size, or slice thickness)
            
            %Update image
            updateCDImage(list)
        end
        
        function handlePropEvents(list,src,evnt,varargin)
            switch evnt.EventName
                case 'newModel'
                    %make sure the graphical pulldown menu matches the ne
                    %model
                    ind=find(strcmp(list.tool.models,list.model));
                    set(list.handles.menu,'Value',ind);
                    
                    %update the rows
                    updateRows(list);
                case 'newRows'
                    set(list.handles.list,'Value',1,'String',list.tool.tab{list.rows,'ReconLabel'})
                    notify(list,'newSelection')
                case 'newSelection'
                    switch evnt.Source
                        case list.referenceList %The reference list has a new selection
                            %Need to re-order the list
                            updateRows(list);
                        case list               %This list has a new selection
                            updateTTFNPSObjects(list);
                            updatePlot(list)
                            if list.isReference
                                %update the scatter plot in the
                                %protocolMatcher
                                updateScatterPlot(list.tool)
                            end
                            updateCDImage(list)
                    end
                case 'refValuesChaged'
                    updatePlot(list)
                    updateCDImage(list)
                case 'distanceUpdated'
                    updateRows(list);
                    
                case 'ImageUpdated'
                    updateCDImage(list)
            end
        end
        
        function updateRows(list)
            rows=find(strcmp(list.tool.tab.ManufacturerModelName,list.model));
            %Now sort rows if needed (if this list is non-reference)
            if ~list.isReference
                D=list.tool.distance(rows,list.referenceRow); %now D is nRowsx1 vector
                [~,ind]=sort(D);
                rows=rows(ind);
            end
            list.rows=rows;
                
        end
        
        function updatePlot(list)
            %Scatter plot
            set(list.handles.scatter,'XData',list.TTFMetric,'YData',list.NPSMetric);
            %Bar plot
            set(list.handles.bar,'YData',list.dose);
            set(list.handles.bar_text,'Position',[list.n list.dose+.5],'String',list.DoseLabel);
            list.tool.handles.DoseAxis.XTickLabel{list.n} = list.model;
            %TTF plot
            set(list.handles.TTFplot,'XData',list.TTF.f,'YData',list.TTF.TTF);
            %NPS plot
            set(list.handles.NPSplot,'XData',list.NPS.f,'YData',list.NPS1D);
            %Noise plot
            set(list.handles.Noiseplot,'XData',list.tool.doses,'YData',list.noise1D);
            set(list.handles.Noiseplot_l,'XData',[0 list.dose list.dose],'YData',[list.noise list.noise 0 ]);
            
        end
        
        function updateCDImage(list)
            %Get Window and level
            L = mean(list.tool.CDim.CRange);
            W = 4*diff(list.tool.CDim.CRange);
            %Get the NPS object
            noise = noiseAtGivenDose(list.tool,list.dose,list.row);
            NPS = copy(list.NPS);
            NPS.noise=noise;
            
            S=size(list.tool.CDim.im);
            %Set the image 
            set(list.handles.im,'CData',mat2gray(getBlurredNoisyImage(list.tool.CDim,list.TTF,NPS),[L-W/2 L+W/2]));
            
            %Set the title of the image axis
            title(list.tool.handles.imAxis(list.n),list.label,'Color','w');
           set(list.tool.handles.imAxis(list.n),'XLim',[0 S(2)],'YLim',[0 S(1)]);
            
            
        end
        
        function set.rows(list,rows)
            list.rows=rows;
            notify(list,'newRows');
        end
        
        function set.model(list,model)
            ind=find(strcmp(list.tool.models,model));
            if ind
                list.model=model;
                notify(list,'newModel');
            else
                warning(['Model not found. Leaving model as ' list.model])
            end
        end
        
        function value = get.value(list)
            value=get(list.handles.list,'Value');
        end
        
        function row = get.row(list)
            row=list.rows(list.value);
        end
        
        function row = get.referenceRow(list)
            if list.isReference
                row=[];
            else
                row = list.referenceList.row;
            end
        end
        
        function distance = get.distance(list)
            
            if ~list.isReference
                distance=list.tool.distance(list.row,list.referenceRow);
            else
                distance =[];
            end
            
        end
        
        function isReference = get.isReference(list)
            isReference = isempty(list.referenceList);
        end
        
        function stat = get.TTFMetric(list)
            stat = list.tool.TTFMetric(list.row);
        end
        
        function stat = get.NPSMetric(list)
            stat = list.tool.NPSMetric(list.row);
        end
        
        function dose = get.dose(list)
            if list.isReference
                dose = list.tool.refDose;
            else
                dose = doseAtGivenNoise(list.tool,list.noise,list.row);
            end
        end
        
        function noise = get.noise(list)
            if list.isReference
                noise = noiseAtGivenDose(list.tool,list.tool.refDose,list.row);
            else
                noise = noiseAtGivenDose(list.tool,list.tool.refDose,list.referenceRow);
            end
            
        end
        
        function updateTTFNPSObjects(list)
            %This function updates the current NPS and TTF for this list.
            %This function is triggered whenever a new selection is made
            list.NPS = getNPSs(list.tool,list.row);
            list.TTF = getTTFs(list.tool,list.row);
        end
        
        function NPS = get.NPS1D(list)
            NPS=list.NPS.NPS;
            NPS = NPS*(list.noise/list.NPS.noise)^2;
        end
        
        function str = get.label(list)
            str = list.tool.tab.Label{list.row};
        end
        
        function str = get.DoseLabel(list)
            
            str={};
            if list.isReference
                str{end+1} = [sprintf('%2.1f',list.dose) ' mGy'];;
                str{end+1} = ['Noise = ' sprintf('%2.1f',list.noise) ' HU'];
            else
                dose=list.dose;
                refDose=list.tool.refDose;
                PC = 100*(dose-refDose)/refDose;
                str{end+1} = [sprintf('%2.1f',dose) ' mGy'];
                str{end+1} = [sprintf('%+2.0f',PC) '%'];
                
            end
            
        end
        
        function noise1D = get.noise1D(list)
            noise1D = noiseAtGivenDose(list.tool,list.tool.doses,list.row);
        end
        
    end
    
end

function callbacks(hObject,evnt,list)
switch hObject
    case list.handles.menu
        str=get(hObject,'String');
        str=str{get(hObject,'Value')};
        list.model=str;
    case list.handles.list
        notify(list,'newSelection')
end
end
    


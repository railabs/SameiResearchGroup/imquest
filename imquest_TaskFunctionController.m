% Copyright 20xx - 2019. Duke University
classdef imquest_TaskFunctionController < handle
    %This is a GUI panel that lets you visualize and adjust properties of
    %a task function object
    
    properties
        handles
        position
        units = 'normalized';
        buff = 30; %buffer of graphics elements in pixels
        task =imquest_TaskFunction; %handle of the underlying task function object
    end
    
    methods
        
        function taskController = imquest_TaskFunctionController(parent,position,varargin) %constructor
            
            switch length(varargin)
                case 1
                    taskController.task = varargin{1};
            end
            
            taskController.handles.parent = parent;
            
            
            %make the panel
            taskController.handles.panel = uipanel(taskController.handles.parent,'Units',taskController.units,'Position',position,'Title','Task Function',...
                'BackgroundColor','k','ForegroundColor','w','HighlightColor','k',...
                'TitlePosition','lefttop','FontSize',14);
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,taskController);
            set(taskController.handles.panel,'ResizeFcn',fun)
            
            %make the axis
            taskController.handles.axes = axes('Parent',taskController.handles.panel,'Units','Normalized','Position',[.5 0 .5 1],...
                'Color','k','XColor','w','YColor','w','Visible','off'); hold on
            %make the image object
            taskController.handles.image = imshow(taskController.task.im,[0 taskController.task.Contrast],'XData',taskController.task.x,'YData',taskController.task.x,'Parent',taskController.handles.axes);
            lims = 1.05*[-taskController.task.FOV/2 taskController.task.FOV/2];
            set(taskController.handles.axes,'Xlim',lims,'Ylim',lims);
            
            %draw the FOV line
            taskController.handles.FOVline = plot([-taskController.task.FOV/2 taskController.task.FOV/2],[-taskController.task.FOV/2 -taskController.task.FOV/2],...
                's-r','MarkerFaceColor','r');
            taskController.handles.FOVtext = text(0, -taskController.task.FOV/2,[num2str(taskController.task.FOV) ' mm FOV'],'Color','w',...
                'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',12,'EdgeColor','r','BackgroundColor','k');
            
            %Draw the diameter line
            taskController.handles.Diameterline = plot([-taskController.task.diameter/2 taskController.task.diameter/2],[taskController.task.FOV/2 taskController.task.FOV/2],...
                's-r','MarkerFaceColor','r');
            taskController.handles.Diametertext = text(0, taskController.task.FOV/2,[num2str(taskController.task.diameter) ' mm'],'Color','w',...
                'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',12,'EdgeColor','r','BackgroundColor','k','Clipping','off');
            
            %Make the popup menu for profile type
            fun=@(hObject,evnt) callbacks(hObject,evnt,taskController);
            taskController.handles.TaskProfilePopup = uicontrol(taskController.handles.panel,'Style','popupmenu','String',{'Designer','Gaussian','Flat'},'Units','Pixel',...
                'TooltipString','Contrast profile of the task function','HorizontalAlignment','center','Enable','On','Callback',fun);
            
            %make edit box for the lesion size
            taskController.handles.TaskSizeEdit = uicontrol(taskController.handles.panel,'Style','edit','String',num2str(taskController.task.diameter),'Units','Pixel',...
                'TooltipString','Size of the task (mm)','HorizontalAlignment','center','Enable','On','UserData',taskController.task.diameter,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            taskController.handles.TaskSizeText = uicontrol(taskController.handles.panel,'Style','text','String',' mm diameter','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %make edit box for lesion profile
            taskController.handles.TaskProfileEdit = uicontrol(taskController.handles.panel,'Style','edit','String',num2str(taskController.task.n),'Units','Pixel',...
                'TooltipString','Profile parameter','HorizontalAlignment','center','Enable','On','UserData',taskController.task.n,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            taskController.handles.TaskProfileText = uicontrol(taskController.handles.panel,'Style','text','String',' profile exponent','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %make edit box for psize
            taskController.handles.PsizeEdit = uicontrol(taskController.handles.panel,'Style','edit','String',num2str(taskController.task.psize),'Units','Pixel',...
                'TooltipString','Task Pixel Size','HorizontalAlignment','center','Enable','On','UserData',taskController.task.psize,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            taskController.handles.PsizeText = uicontrol(taskController.handles.panel,'Style','text','String',' mm pixel size','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %make edit box for number of pixels
            taskController.handles.NEdit = uicontrol(taskController.handles.panel,'Style','edit','String',num2str(taskController.task.N),'Units','Pixel',...
                'TooltipString','Number of pixels','HorizontalAlignment','center','Enable','On','UserData',taskController.task.N,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            taskController.handles.NText = uicontrol(taskController.handles.panel,'Style','text','String',' pixels','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %add listener for changing task properties
            addlistener(taskController.task,'taskFunctionChanged',@taskController.handlePropEvents);
            
            taskController.position = position;
        end
        
        function handlePropEvents(taskController,src,evnt,varargin)
            switch evnt.EventName
                case 'taskFunctionChanged'
                    updateTaskImage(taskController);
            end
        end
        
        function updateTaskImage(taskController)
            %This is run automatically whenever a task property is changed
            lims = 1.05*[-taskController.task.FOV/2 taskController.task.FOV/2];
            clim = [min([0 taskController.task.Contrast]) max([0 taskController.task.Contrast])];
            set(taskController.handles.image,'CData',taskController.task.im,'XData',taskController.task.x,'YData',taskController.task.x);
            set(taskController.handles.axes,'Clim',clim,'Xlim',lims,'Ylim',lims);
            set(taskController.handles.FOVline,'XData',[-taskController.task.FOV/2 taskController.task.FOV/2],'YData',[-taskController.task.FOV/2 -taskController.task.FOV/2]);
            set(taskController.handles.FOVtext,'String',[num2str(taskController.task.FOV) ' mm FOV'],'Position',[0 -taskController.task.FOV/2]);
            set(taskController.handles.Diameterline,'XData',[-taskController.task.diameter/2 taskController.task.diameter/2],'YData',[taskController.task.FOV/2 taskController.task.FOV/2]);
            set(taskController.handles.Diametertext,'String',[num2str(taskController.task.diameter) ' mm'],'Position',[0 taskController.task.FOV/2]);
            
            switch taskController.task.profileType
                case 'Designer'
                    t=' profile exponent';
                    en = 'on';
                case 'Gaussian'
                    t = ' profile STD';
                    en ='on';
                case 'Flat'
                    t='';
                    en = 'off';
                otherwise
                    t='?';
                    en = 'on';
            end
            set(taskController.handles.TaskProfileText,'String',t)
            set(taskController.handles.TaskProfileEdit,'Visible',en)
            %now make sure all graphics are synced to the properties of the
            %task (in case the properties were changed programatically
            %instead of manually by the user
            updateGUIValues(taskController)
        end
        
        function updateGUIValues(taskController)
            %this syncs the graphical elements to be the same as the
            %taskFunction object. (needed in case the task function is
            %changed programatically)
            str=get(taskController.handles.TaskProfilePopup,'String');
            val = find(strcmp(str,taskController.task.profileType));
            set(taskController.handles.TaskProfilePopup,'Value',val);
            set(taskController.handles.TaskSizeEdit,'String',num2str(taskController.task.diameter),'UserData',taskController.task.diameter)
            set(taskController.handles.TaskProfileEdit,'String',num2str(taskController.task.n),'UserData',taskController.task.n)
            set(taskController.handles.PsizeEdit,'String',num2str(taskController.task.psize),'UserData',taskController.task.psize)
            set(taskController.handles.NEdit,'String',num2str(taskController.task.N),'UserData',taskController.task.N)
        end
        
        function set.position(taskController,position)
            taskController.position=position;
            set(taskController.handles.panel,'Position',position);
        end
        
        function set.units(taskController,units)
            taskController.units=units;
            set(taskController.handles.units,'Position',units);
        end
    end
    
    
end

function callbacks(hObject,evnt,taskController)
    switch hObject
        case taskController.handles.TaskProfilePopup
            val = get(hObject,'Value');
            str = get(hObject,'String');
            profileType = str{val};
            taskController.task.profileType= profileType;
        case taskController.handles.TaskSizeEdit
            new = checkEditBoxValue(hObject,'GreaterThanZero');
            taskController.task.diameter=new;
        case taskController.handles.TaskProfileEdit
            new = checkEditBoxValue(hObject,'GreaterThanZero');
            taskController.task.n=new;
        case taskController.handles.PsizeEdit
            new = checkEditBoxValue(hObject,'GreaterThanZero');
            taskController.task.psize=new;
        case taskController.handles.NEdit
            new = checkEditBoxValue(hObject,'GreaterThanZeroInt');
            taskController.task.N=new;
    end
end

function panelResizeFunction(hObject,events,taskController)
pos = getPixelPosition(hObject);
buff=taskController.buff;
set(taskController.handles.TaskProfilePopup,'Position',[buff pos(4)-2*buff 4*buff buff])
set(taskController.handles.TaskSizeEdit,'Position',[buff pos(4)-3*buff buff buff])
set(taskController.handles.TaskSizeText,'Position',[2*buff pos(4)-3*buff 3*buff buff/1.5])
set(taskController.handles.TaskProfileEdit,'Position',[buff pos(4)-4*buff buff buff])
set(taskController.handles.TaskProfileText,'Position',[2*buff pos(4)-4*buff 3*buff buff/1.5])
set(taskController.handles.PsizeEdit,'Position',[buff pos(4)-5*buff 2*buff buff])
set(taskController.handles.PsizeText,'Position',[3*buff pos(4)-5*buff 3*buff buff/1.5])
set(taskController.handles.NEdit,'Position',[buff pos(4)-6*buff buff buff])
set(taskController.handles.NText,'Position',[2*buff pos(4)-6*buff 3*buff buff/1.5])


end


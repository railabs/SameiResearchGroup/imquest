% Copyright 20xx - 2019. Duke University
classdef protocolMatcher_ContrastDetailImage_Controler < handle
    properties
        handles
        position
        units = 'normalized';
        tool %handle to tool object
        CDim %handle to the contrast detail image object
    end
    
    methods
        function obj = protocolMatcher_ContrastDetailImage_Controler(tool,position)
            %Set the tool property
            obj.tool=tool;
            obj.CDim=tool.CDim;
            
            %Create the panel
            obj.handles.panel = uipanel(tool.handles.fig,...
                'Title','Contrast Detail Image Settings','BackgroundColor','k',...
                'ForegroundColor','w','HighlightColor','k','TitlePosition',...
                'lefttop','FontSize',12);
            set(obj.handles.panel,'Units',obj.units,'Position',position)
            obj.position=position;
            
            %Create the profile pulldown menu
            fun=@(hObject,evnt) callbacks(hObject,evnt,obj);
            ind=find(strcmp(obj.CDim.possibleProfileTypes,obj.CDim.profileType));
            obj.handles.popupProfileType= uicontrol(obj.handles.panel,'Style','popupmenu',...
                'String',obj.CDim.possibleProfileTypes,'Value',ind,'Units','normalized','Position',...
                [0 6/7 .5 1/7],'Callback',fun);
            obj.handles.popupCriteria_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Profile Type',...
                'Units','normalized','Position',[.5 6/7 .5 1/7],'HorizontalAlignment','left');
            
            %Create the n edit box
            obj.handles.editn = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.n),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 5/7 .5 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.n);
            obj.handles.editn_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Profile Exponent',...
                'Units','normalized','Position',[.5 5/7 .5 1/7],'HorizontalAlignment','left');
            
            %Create the pixel size edit box
            obj.handles.editPsize = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.psize),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 4/7 .5 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.psize);
            obj.handles.editPsize_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Pixel Size [mm]',...
                'Units','normalized','Position',[.5 4/7 .5 1/7],'HorizontalAlignment','left');
            
            %Create Npix edit box
            obj.handles.editNpix = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.Npix),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 3/7 .5 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.Npix);
            obj.handles.editNpix_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','N Pixels per Signal',...
                'Units','normalized','Position',[.5 3/7 .5 1/7],'HorizontalAlignment','left');
            
            %Create CRange edit boxes
            obj.handles.editCmin = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.CRange(1)),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 2/7 .25 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.CRange(1));
            obj.handles.editCmax = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.CRange(2)),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[.25 2/7 .25 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.CRange(2));
            obj.handles.editC_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Contrast Range [HU]',...
                'Units','normalized','Position',[.5 2/7 .5 1/7],'HorizontalAlignment','left');
            
            %Create SRange edit boxes
            obj.handles.editSmin = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.SRange(1)),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 1/7 .25 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.SRange(1));
            obj.handles.editSmax = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.SRange(2)),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[.25 1/7 .25 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.SRange(2));
            obj.handles.editS_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','Diameter Range [mm]',...
                'Units','normalized','Position',[.5 1/7 .5 1/7],'HorizontalAlignment','left');
            
            %Create N signals edit box
            obj.handles.editN = uicontrol(obj.handles.panel,'Style','edit',...
                'String',num2str(obj.CDim.N),'BackgroundColor','k','ForegroundColor','w',...
                'Units','normalized','Position',[0 0 .5 1/7],'HorizontalAlignment','right',...
                'Callback',fun,'UserData',obj.CDim.N);
            obj.handles.editN_text = uicontrol(obj.handles.panel,'Style','text',...
                'BackgroundColor','k','ForegroundColor','w','String','N Signals',...
                'Units','normalized','Position',[.5 0 .5 1/7],'HorizontalAlignment','left');
            
        end
        
        function handlePropEvents(obj,src,evnt,varargin)
            switch evnt.EventName
                case 'ImageUpdated'
                    updateGraphics(obj);
            end
        end
        
        function updateGraphics(obj)
        end
        
    end
end

function callbacks(hObject,evnt,obj)
switch hObject
    case obj.handles.popupProfileType
        str = get(hObject,'String'); str=str{get(hObject,'Value')};
        obj.CDim.profileType = str;
    case obj.handles.editn
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.CDim.n
            obj.CDim.n=new;
        end
    case obj.handles.editPsize
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.CDim.psize
            obj.CDim.psize=new;
        end
    case obj.handles.editNpix
        new = checkEditBoxValue(hObject,'GreaterThanZeroInt');
        if new~=obj.CDim.Npix
            obj.CDim.Npix=new;
        end
    case obj.handles.editCmin
        new = checkEditBoxValue(hObject,'AnyRealValue');
        if new~=obj.CDim.CRange(1)
            obj.CDim.CRange(1)=new;
        end
    case obj.handles.editCmax
        new = checkEditBoxValue(hObject,'AnyRealValue');
        if new~=obj.CDim.CRange(2)
            obj.CDim.CRange(2)=new;
        end
    case obj.handles.editSmin
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.CDim.SRange(1)
            obj.CDim.SRange(1)=new;
        end
    case obj.handles.editSmax
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        if new~=obj.CDim.SRange(2)
            obj.CDim.SRange(2)=new;
        end
    case obj.handles.editN
        new = checkEditBoxValue(hObject,'GreaterThanZeroInt');
        if new~=obj.CDim.N
            obj.CDim.N=new;
        end
end
end
% Copyright 20xx - 2019. Duke University
classdef imquest_Dprime < handle
    %This object does dprime calculations and can be used to generate
    %example images of the blurred task with correlated noise
    
    properties
        task
        TTF
        NPS
        model = 'NPW'; %Can be 'NPW','NPWE', 'NPWi', and 'NPWEi';
        noiseGenerationMode = '2D'; %Can be '2D' or 'Radial'
        pixelPitch = 0.2;  %Physical size of display pixels (mm, used in eye filter). This defualt matches (almost) a Barco MDCC 6230 display
        zoom = 3;   %Image display zoom factor, used in eye filter. Default roughly based on viewing a 512x512 image at full screen on a 3280x2048 display, assuming the image occupies half the display horizontaly (3280/2/512 ~=3)
        viewingDistance = 500; %assumed viewing distance (mm) for the eye filter
        EyeFilterMode = 'Saunders'; %There are two eye filter models with slightly different parameters, Can choose 'Saunders' or 'Eckstein'; See getVisualResponseFunction() below for citations
        alpha = 5;  %Internal noise scaling factor
    end
    
    properties (SetAccess = protected)
        detectabilityIndex %This will only be updated if the task, TTF, NPS, model, or noiseGenerationMode changes
    end
    
    properties (GetAccess = protected, Dependent = true)
        dp %This is the internal d' variable (the public detectabilityIndex is basically synced to this. I do it this way so I don't have to re-compute d' everytime I want it, only when the dprimes objects properties change
    end
    
    properties (Dependent = true)
        AUC         %Computes AUC from given d' (assumed test statistics are normally distributed)
        useEyeFilter    %boolean which determines if an eye filter is used in dprime calculations. 
        addInternalNoise %boolean which determines if internal noise is added (dependent on model type)
        EyeFilter       %Returns the eye filter, matched in size to the task function
        InternalNoise    %Returns the internal noise power spectrum, matched in size to the task function
        im  %Image of the blurred task with correlated noise added (Different noise field is generated each time this property is accessed)
        Contrast %Returns contrast from the task function
        noise %Returns pixel STD based on NPS
        CNR  %Contrast/noise
        diameter %Diameter of the task (just returns the diameter of the task object)
        N   %Number of pixels (just returns N from the task object)
        psize %pixel size of the task (just returns psize from the task object)
        FOV     %FOV of the task (just returns FOV from the task object)
        fx      %Spatial frequency axis (return fx from the task object)
        df      %Spacing of spatial frequency bins (just returns df from task object);
        Fr      %Radial spatial frequency (just returns Fr from the task object)
        displayedImageSize %Returns the physical size (mm) of the displayed image according to the task function size and viewing conditions (pixel pitch, zoom, viewing distance)
        label   %Returns a string with the detectability index
        fullLabel %Returns a string cell array with a lot of info about the d-prime calculation
        Dprimeinfo%Returns a structured array of info about the dprime calculations
        args    %Returns a cell array that can be used to construct a new dprime object based on the current dprime object (used in the copy method)
        statsTable %Returns a table of statistics about the dprime calculation
        versionInfoTable %Returns a table with information about the git repo versions used in this calculation.
    end
    
    events
        propertyChanged
    end
    
    methods
        function Dprime = imquest_Dprime(task,TTF,NPS,varargin)  %Constructor
            %Set the required properties
            Dprime.task=copy(task);
            Dprime.TTF=copy(TTF);
            Dprime.NPS=copy(NPS);
            
            %set the optional properties
            switch length(varargin)
                case 1
                    setOptionalSettings(Dprime,varargin{1})
                otherwise
                    setOptionalSettings(Dprime,varargin)
            end
            
            %do the d-prime calculation
            Dprime.detectabilityIndex=Dprime.dp;
            
            %Add the listener for properties changing that could affect the
            %d-prime calculation
            addlistener(Dprime,'propertyChanged',@Dprime.handlePropEvents);
            addlistener(Dprime.task,'taskFunctionChanged',@Dprime.handlePropEvents);
        end
        
        function handlePropEvents(Dprime,src,evnt,varargin)
            switch evnt.EventName
                case {'propertyChanged','taskFunctionChanged'}
                    %Need to re-do the d-prime calculation
                    Dprime.detectabilityIndex=Dprime.dp;
            end
        end
        
        function label = get.label(Dprime)
            label = ['d'': ' sprintf('%3.2f',Dprime.detectabilityIndex)];
        end
        
        function t = get.fullLabel(Dprime)
            t={};
            t{end+1,1} = Dprime.label;
            t{end+1,1} = ['AUC: ' sprintf('%1.3f',Dprime.AUC) ' HU'];
            t{end+1,1} = ['Noise: ' sprintf('%3.1f',Dprime.noise) ' HU'];
            t{end+1,1} = ['NPS fav: ' sprintf('%1.2f',Dprime.NPS.fav) ' 1/mm'];
            t{end+1,1} = ['Contrast: ' sprintf('%3.1f',Dprime.Contrast) ' HU'];
            t{end+1,1} = ['CNR: ' sprintf('%3.2f',Dprime.CNR)];
            t{end+1,1} = ['TTF f50: ' sprintf('%1.2f',Dprime.TTF.f50) ' 1/mm'];
            t{end+1,1} = ['fav: ' sprintf('%1.2f',Dprime.NPS.fav) ' 1/mm'];
            t{end+1,1} = ['Model: ' Dprime.model];
        end
        
        function Dprimeinfo = get.Dprimeinfo(Dprime)
            
            %Now add the Dprime specific info
            Dprimeinfo.detectabilityIndex=Dprime.detectabilityIndex;
            Dprimeinfo.AUC=Dprime.AUC;
            Dprimeinfo.CNR=Dprime.CNR;
            Dprimeinfo.model=Dprime.model;
            Dprimeinfo.noiseGenerationMode=Dprime.noiseGenerationMode;
            Dprimeinfo.pixelPitch=Dprime.pixelPitch;
            Dprimeinfo.zoom=Dprime.zoom;
            Dprimeinfo.viewingDistance=Dprime.viewingDistance;
            Dprimeinfo.EyeFilterMode=Dprime.EyeFilterMode;
            Dprimeinfo.alpha=Dprime.alpha;
            
            %Get merge with the Task, TTF, and NPS
            Dprimeinfo = mergeStructs({Dprimeinfo,Dprime.task.taskinfo,Dprime.TTF.TTFinfo,Dprime.NPS.NPSinfo});
            
            tab = Dprime.versionInfoTable;
            for i=1:size(tab,1)
                try Dprimeinfo = rmfield(Dprimeinfo,tab{i,1}{1}); end
                Dprimeinfo.([tab{i,1}{1} '_' tab{i,4}{1}])=['Git branch: ' tab{i,3}{1} ', Commit ID: ' tab{i,2}{1}];
            end
            
        end
        
        function showDprimeinfo(Dprime)
            h = imageinfo(Dprime.Dprimeinfo);
            set(h,'Name',['d'' info']);
        end
        
        function AUC = get.AUC(Dprime)
            AUC = normcdf(Dprime.detectabilityIndex/sqrt(2));
        end
        
        function dp = get.dp(Dprime)
            %This is the function that computes Dprime
            
            %Get the task function
            W = Dprime.task.W; %This is returned from the task object as the squared abs of the FFT of the task image (i.e., don't need to square when putting into d' calc)
            
            %Get the TTF
            TTFup = getResampled2DTTF(Dprime.TTF,Dprime.psize,Dprime.N);
            
            %Get the NPS
            NPSup = getResampled2DNPS(Dprime.NPS,Dprime.psize,Dprime.N,Dprime.noiseGenerationMode);
            
            %Get the EyeFilter
            E = Dprime.EyeFilter;
            
            %Get the internal noise
            I = Dprime.InternalNoise;
            
            %Compute d'
            df = Dprime.df;     %Spacing of spatial frequencies
            num=(sum(sum(   (W).*(TTFup.^2).*(E.^2)  )).*df*df).^2;
            den=(sum(sum(   (W).*(TTFup.^2).*(E.^4).*(NPSup) + I )).*df*df);
            dp = sqrt(num./den);
        end
        
        function E = get.EyeFilter(Dprime)
            
            if Dprime.useEyeFilter 
                E = getVisualResponseFunction(Dprime.Fr,Dprime.FOV,Dprime.displayedImageSize,Dprime.viewingDistance,Dprime.EyeFilterMode);
            else
                E = ones(Dprime.N);
            end
            
            
        end
        
        function display = get.displayedImageSize(Dprime)
            Ndisplay = Dprime.zoom*Dprime.N; %This is the number of displayed pixels;
            display = Ndisplay * Dprime.pixelPitch; %physical size of the displayed image;
        end
        
        function I = get.InternalNoise(Dprime)
            
            if Dprime.addInternalNoise
                 I=Dprime.alpha.*((Dprime.viewingDistance./1000).^2).*(Dprime.NPS.noise^2).*ones(Dprime.N); %based on Richard, S., & Siewerdsen, J. H. (2008). Comparison of model and human observer performance for detection and discrimination tasks using dual-energy x-ray images. Medical Physics, 35(11), 5043. doi:10.1118/1.2988161
            else %No internal noise, return array of zeros
                I = zeros(Dprime.N);
            end   
            
        end
        
        function Contrast = get.Contrast(Dprime)
            Contrast = Dprime.task.Contrast;
        end
        
        function noise = get.noise(Dprime)
            noise = Dprime.NPS.noise;
        end
        
        function CNR = get.CNR(Dprime)
            CNR = Dprime.Contrast/Dprime.noise;
        end
        
        function diameter = get.diameter(Dprime)
            diameter=Dprime.task.diameter;
        end
        
        function N = get.N(Dprime)
            N = Dprime.task.N;
        end
        
        function psize = get.psize(Dprime)
            psize = Dprime.task.psize;
        end
        
        function fx = get.fx(Dprime)
            fx = Dprime.task.fx;
        end
        
        function df = get.df(Dprime)
            df = Dprime.task.df;
        end
        
        function Fr = get.Fr(Dprime)
            Fr = Dprime.task.Fr;
        end
        
        function FOV = get.FOV(Dprime)
            FOV = Dprime.task.FOV;
        end
        
        function useEyeFilter = get.useEyeFilter(Dprime)
            switch Dprime.model
                case {'NPWE','NPWEi'}
                    useEyeFilter=true;
                otherwise
                    useEyeFilter=false;
            end
        end
        
        function addInternalNoise = get.addInternalNoise(Dprime)
            switch Dprime.model
                case {'NPWi','NPWEi'}
                    addInternalNoise=true;
                otherwise
                    addInternalNoise=false;
            end
        end
        
        function im = get.im(Dprime)
            
            %get image of the task
            im = Dprime.task.im;
            %Blur the image
            im = blurWithTTF(Dprime.TTF,im,Dprime.task.psize);
            %Add noise
            noise = getCorrelatedNoise(Dprime.NPS,Dprime.task.psize,[size(im,2) size(im,1)],Dprime.noiseGenerationMode);
            im = im + noise;
            
        end
        
        function set.task(Dprime,task)
            Dprime.task=task;
            notify(Dprime,'propertyChanged');
        end
        
        function set.TTF(Dprime,TTF)
            Dprime.TTF=TTF;
            notify(Dprime,'propertyChanged');
        end
        
        function set.NPS(Dprime,NPS)
            Dprime.NPS=NPS;
            notify(Dprime,'propertyChanged');
        end
        
        function set.model(Dprime,model)
            Dprime.model=model;
            notify(Dprime,'propertyChanged');
        end
        
        function set.noiseGenerationMode(Dprime,noiseGenerationMode)
            Dprime.noiseGenerationMode=noiseGenerationMode;
            notify(Dprime,'propertyChanged');
        end
        
        function set.pixelPitch(Dprime,pixelPitch)
            Dprime.pixelPitch=pixelPitch;
            notify(Dprime,'propertyChanged');
        end
        
        function set.zoom (Dprime,zoom )
            Dprime.zoom =zoom ;
            notify(Dprime,'propertyChanged');
        end
        
        function set.viewingDistance (Dprime,viewingDistance )
            Dprime.viewingDistance =viewingDistance ;
            notify(Dprime,'propertyChanged');
        end
        
        function set.EyeFilterMode(Dprime,EyeFilterMode)
            Dprime.EyeFilterMode=EyeFilterMode;
            notify(Dprime,'propertyChanged');
        end
        
        function set.alpha (Dprime,alpha )
            Dprime.alpha =alpha ;
            notify(Dprime,'propertyChanged');
        end
        
        function setOptionalSettings(Dprime,args)
            
            if ~isempty(args)
                for i=1:2:length(args)
                    Dprime.(args{i})=args{i+1};
                end
            end
            
        end
        
        function args = get.args(Dprime)
            args={};
            args{end+1}='model'; args{end+1}=Dprime.model;
            args{end+1}='model'; args{end+1}=Dprime.model;
            args{end+1}='noiseGenerationMode'; args{end+1}=Dprime.noiseGenerationMode;
            args{end+1}='pixelPitch'; args{end+1}=Dprime.pixelPitch;
            args{end+1}='zoom'; args{end+1}=Dprime.zoom;
            args{end+1}='viewingDistance'; args{end+1}=Dprime.viewingDistance;
            args{end+1}='EyeFilterMode'; args{end+1}=Dprime.EyeFilterMode;
            args{end+1}='alpha'; args{end+1}=Dprime.alpha;
        end
        
        function newDprime = copy(Dprime)
            
            newDprime = imquest_Dprime(Dprime.task,Dprime.TTF,Dprime.NPS,Dprime.args);
            
        end
        
        function out = get.statsTable(Dprime)
            out.Dprime = Dprime.detectabilityIndex;
            out.AUC=Dprime.AUC;
            out.model = {Dprime.model};
            out.TaskContrast=Dprime.Contrast;
            out.TaskCNR=Dprime.CNR;
            out.NPSnoise=Dprime.noise;
            out.NPSfpeak = Dprime.NPS.fpeak;
            out.NPSfav = Dprime.NPS.fav;
            out.TTFContrast = Dprime.TTF.contrast;
            out.TTFnoise = Dprime.TTF.noise;
            out.TTFf10 = Dprime.TTF.f10;
            out.TTFf50 = Dprime.TTF.f50;
            out.TTFSeriesInstanceUID = Dprime.TTF.SeriesInstanceUID;
            out.TTFpsize = Dprime.TTF.psize;
            out.TTFSliceThickness = Dprime.TTF.sliceThickness;
            out.NPSSeriesInstanceUID = Dprime.NPS.SeriesInstanceUID;
            out.NPSpsize = Dprime.NPS.psize(1);
            out.NPSSliceThickness = Dprime.NPS.sliceThickness;
            out = struct2table(out);
            
        end
        
        function tab = get.versionInfoTable(Dprime)
            TTFtab = Dprime.TTF.versionInfoTable;
            TTFlab = repmat({'TTF'},[size(TTFtab,1),1]);
            NPStab = Dprime.NPS.versionInfoTable;
            NPSlab = repmat({'NPS'},[size(NPStab,1),1]);
            tab = [TTFtab;NPStab];
            tab.Component = [TTFlab; NPSlab];
        end
        
        function  writeToCSVFile(Dprime,filename)
            for i=1:length(Dprime)
                out(i,:)=Dprime(i).statsTable;
            end
            
            writetable(out,filename);
        end
        
    end
end



%%
%Define slices ranges
NPSlimits = [-405 -371.25; -281.25 -270; -161.25 -150;-1607.7 -1592.7; -1742.7 -1712.7];
TTFlimits = [-431.25 -423.75; -318.75 -300; -198.75 -176.25; -1577.7 -1557.7; -1697.7 -1682.7];
%%
% Create bare analysis
res = imquest_MercuryPhantomAutoAnalyze(tool,'Bare');


sliceSize = zeros(size(res.z));
NPSslice = false(size(res.z));
TTFslice = false(size(res.z));
for j = 1:length(res.PhantomDiameters)
    zmin = NPSlimits(j,1);
    zmax = TTFlimits(j,2);
    ind = res.z >= zmin & res.z <= zmax;
    sliceSize(ind) = res.PhantomDiameters(j);
    
    zmin = NPSlimits(j,1);
    zmax = NPSlimits(j,2);
    ind = res.z >= zmin & res.z <= zmax;
    NPSslice(ind)=true;
    
    zmin = TTFlimits(j,1);
    zmax = TTFlimits(j,2);
    ind = res.z >= zmin & res.z <= zmax;
    TTFslice(ind) = true;
end
res.sliceSize = sliceSize;
res.NPSslice = NPSslice;
res.TTFslice = TTFslice;
[res.TTFs, res.NPSs, res.Dprimes] = makeDprimeMeasurements(res);
[res.fits, res.gofs] = makeDprimevsSizeFits(res);
res.resultsSummaryTable
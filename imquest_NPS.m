% Copyright 20xx - 2019. Duke University
classdef imquest_NPS < handle
    
    properties
        f
        NPS
        noise
        graphicHandle
        tool
        settings
        stats
        slices
        psize
        sliceThickness
        SeriesInstanceUID
        selected = false;
        marker = '.';
        lineWidth = [1 2];
        visible = true;
        withGraphics %boolean that is needed to create copies of the object without graphics components
        versionInfoTable
    end
    
    properties (Dependent = true)
        label
        fullLabel
        NPSinfo
        fpeak
        fav
    end
    
    
    events
        NPSdeleted
    end
    
    methods
        
        function NPShandle = imquest_NPS(varargin)
            
            
            switch nargin
                case 1 %NPShandle = imquest_NPS(NPS) This makes a copy of the NPS without grapihcs
                    NPS=varargin{1};
                    NPShandle.withGraphics=false;
                    NPShandle.f=NPS.f;
                    NPShandle.NPS=NPS.NPS;
                    NPShandle.noise=NPS.noise;
                    NPShandle.graphicHandle=[];
                    NPShandle.tool=[];
                    NPShandle.settings=NPS.settings;
                    NPShandle.stats=NPS.stats;
                    NPShandle.slices=NPS.slices;
                    NPShandle.psize=NPS.psize;
                    NPShandle.sliceThickness=NPS.sliceThickness;
                    NPShandle.SeriesInstanceUID=NPS.SeriesInstanceUID;
                    NPShandle.versionInfoTable=NPS.versionInfoTable;
                case 2 %NPShandle = imquest_NPS(tool,NPSROIs) or NPShandle = imquest_NPS(tool,NPS)
                    
                    tool=varargin{1};
                    NPSROIs=varargin{2};
                    
                    if isa(NPSROIs,'imquest_NPS')
                        NPShandle = copy(NPSROIs);
                        NPShandle.tool = tool;
                        %make the plot
                        NPShandle.withGraphics = true;
                        NPShandle.graphicHandle = plot(NPShandle.f,NPShandle.NPS,'Parent',tool.handles.NPSaxis,'LineWidth',NPShandle.lineWidth(1),'MarkerSize',12);
                    else
                        
                        %set the NPShandle tool property
                        NPShandle.tool=tool;
                        
                        %Get the slices of interest
                        [im, NPShandle.slices] = getImageSlices(tool,tool.handles.NPSsliceSelector);
                        
                        %Get the DICOM series UID
                        NPShandle.SeriesInstanceUID = tool.DICOMheaders(NPShandle.slices(1)).SeriesInstanceUID;
                        
                        %Get the pixel size
                        psize = tool.CTpsize;
                        NPShandle.psize=psize;
                        NPShandle.sliceThickness=tool.CTsliceThickness;
                        
                        %set up the ROIs
                        for i=1:length(NPSROIs)
                            pos = getPosition(NPSROIs(i));
                            pos(1) = pos(1)-pos(3)/2;
                            pos(2) = pos(2)-pos(4)/2;
                            ROIs(i,:)=round(pos);
                        end
                        
                        %measure the NPS
                        [NPShandle.f, NPShandle.NPS,NPShandle.noise,NPShandle.settings,NPShandle.stats] = get2DNPS(im,ROIs,psize);
                        
                        %make the plot
                        NPShandle.withGraphics = true;
                        NPShandle.graphicHandle = plot(NPShandle.f,NPShandle.NPS,'Parent',tool.handles.NPSaxis,'LineWidth',NPShandle.lineWidth(1),'MarkerSize',12);
                        
                        %Get the imquest version info
                        NPShandle.versionInfoTable=tool.versionInfoTable;
                        
                    end
                    
                    %add to the imquest list of measured NPS
                    addNPS(tool,NPShandle);
                    
            end
            
            
            
        end
        
        function label = get.label(NPS)
            label = ['Noise: ' sprintf('%3.1f',NPS.noise) ' HU'];
        end
        
        function t = get.fullLabel(NPS)
            t{1,1} = NPS.label;
            t{2,1} = ['fpeak: ' sprintf('%1.2f',NPS.fpeak) ' 1/mm'];
            t{3,1} = ['fav: ' sprintf('%1.2f',NPS.fav) ' 1/mm'];
            t{4,1} = ['Slices: ' num2str(NPS.slices(1)) ' to ' num2str(NPS.slices(2))];
            
        end
        
        function fpeak = get.fpeak(NPS)
            fpeak = NPS.stats.fpeak;
        end
        
        function fav = get.fav(NPS)
            fav = NPS.stats.fav;
        end
        
        function set.selected(NPS,selected)
            if NPS.withGraphics
                NPS.selected = selected;
                if selected
                    set(NPS.graphicHandle,'Marker',NPS.marker,'LineWidth',NPS.lineWidth(2));
                    %uistack(NPS.graphicHandle,'top');
                else
                    set(NPS.graphicHandle,'Marker','none','LineWidth',NPS.lineWidth(1));
                end
            end
        end
        
        function set.marker(NPS,marker)
            if NPS.withGraphics
                NPS.marker = marker;
                if NPS.selected
                    set(NPS.graphicHandle,'Marker',marker)
                end
            end
        end
        
        function showNPSinfo(NPS)
            h = imageinfo(NPS.NPSinfo);
            set(h,'Name',['NPS Info']);
        end
        
        function NPSinfo = get.NPSinfo(NPS)
            %The puts some of the key meta data about the NPS into a
            %structured variable so it can be easily shown with an
            %imageinfo figure
            NPSinfo.noise=NPS.noise;
            NPSinfo.slices = NPS.slices;
            NPSinfo.SeriesInstanceUID=NPS.SeriesInstanceUID;
            %get the stats fieldnames
            names = fieldnames(NPS.stats);
            for i=1:length(names)
                NPSinfo.(names{i})=NPS.stats.(names{i});
            end
            names = fieldnames(NPS.settings);
            for i=1:length(names)
                NPSinfo.(names{i})=NPS.settings.(names{i});
            end
            tab = NPS.versionInfoTable;
            NPSinfo.(tab.RepositoryName)=['Git branch: ' tab.BranchName ', Commit ID: ' tab.GitID];  
            
        end
        
        function varargout = getResampled2DNPS(NPS,psize,N,varargin)
            %This function resamples the 2D NPS
            if length(varargin)>0
                mode = varargin{1};
            else
                mode = '2D'; %Mode can be 'Radial' or '2D' and determines if the resampling is done based on the radially averaged NPS or the full 2D NPS
            end
            
            %Create a coordinate system
            fx = getFFTfrequency(psize(1),N(1));
            if length(psize)>1 && length(N)>1
                fy = getFFTfrequency(psize(2),N(2));
            elseif length(psize)>1 && length(N)==1
                fy = getFFTfrequency(psize(2),N);
            else
                fy=fx;
            end
            dfx=fx(2)-fx(1);
            dfy = fy(2)-fy(1);
            [Fx,Fy]=meshgrid(fx,fy);
            
            switch mode
                case 'Radial' %1D interpolation on the radially averaged NPS
                    [~,Fr]=cart2pol(Fx,Fy);
                    NPSup = interp1(NPS.f,NPS.NPS,Fr,'linear',0);
                case '2D'       %2D interpolation on the full 2D NPS
                    NPSup = interp2(NPS.stats.fx,NPS.stats.fy,NPS.stats.NPS_2D,Fx,Fy,'linear',0);
            end
            
            %Scale the NPS as needed to maintain the noise variance from
            %the original NPS
            NPSup = NPSup*(NPS.noise^2)/(sum(NPSup(:))*dfx*dfy);
            
            switch nargout
                case 1
                    varargout{1}=NPSup;
                case 3
                    varargout{1}=fx;
                    varargout{2}=fy;
                    varargout{3}=NPSup;
            end
            
        end
        
        function noise = getCorrelatedNoise(NPS,psize,N,varargin)
            
            %This function resamples the 2D NPS
            if length(varargin)>0
                mode = varargin{1};
            else
                mode = '2D'; %Mode can be 'Radial' or '2D' and determines if the resampling is done based on the radially averaged NPS or the full 2D NPS
            end
            
            %Get the sqrt of the resampled NPS
            NPSup = sqrt(getResampled2DNPS(NPS,psize,N,mode));
            
            %Generate white noise image
            if length(N)>1
                N=[N(2) N(1)];
            else
                N=[N N];
            end
            im = random('norm',0,1,N);
            
            MV=mean(im(:)); %get mean value
            im=im-MV; %remove mean
            F=fftn(im); %Fourier Transform
            F(1)=0;     %Remove DC component (same as filtering with a notch filter)
            NPSup=fftshift(NPSup); %Shift MTF
            H=F.*NPSup; %Apply filtering
            noise = real(ifftn(H))+MV; %Inverse fourier transform
            
            %Scale to get the right STD
            noise = noise*NPS.noise/std(noise(:));
            
        end
        
        function newNPS=copy(NPS)
            newNPS = imquest_NPS(NPS);
        end
        
        function writeToCSVFile(NPS,filename)
            out = [NPS.f NPS.NPS];
            out = array2table(out);
            out.Properties.VariableNames = {'f','NPS'};
            writetable(out,filename);
        end
        
        function delete(NPShandle)
            delete(NPShandle.graphicHandle);
            notify(NPShandle,'NPSdeleted');
        end
    end
end
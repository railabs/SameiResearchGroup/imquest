 % Copyright 20xx - 2019. Duke University
classdef imquest < handle
    
    properties
        handles
        DICOMheaders=[];
        TTFROIs = [];
        TTFInnerCircleROIs = []; %This is always linked to the TTF ROIs (1to1 relationship)
        TTFzROIs = []
        TTFzInnerCircleROIs = []
        NPSROIs = [];
        TTF_R = .5; %Default Ratio of inner to outer ring diameter
        TTF_rod = 25; %Default size of the inner circle ROI in mm
        TTFz_R = .25;
        TTFz_rod = 25;
        TTFs = [];
        TTFzs= [];
        NPSs = [];
        ACRZeroOffset=0;
        taskFunction=imquest_TaskFunction;
        Dprimes = [];
        showCTInfo = true;
        status = 'Ready';
        defaultStatus='Ready'
        Visible = true;
        BannerColor = [4 65 156;1 18 43]/256;
        CTDataLoaded = false;
        lastDir
        log={}
        
    end
    
    properties (Dependent = true)
        nTTFROIs
        nTTFzROIs
        nTTFs
        nTTFzs
        nNPSROIs
        nNPSs
        canComputeDetectability
        nDprimes
        CTpsize
        CTsliceThickness
        CTImageSize
        CTPath      %Direcory containing the dicoms
        sliceLabel
        bannerImage
        VisibleString
        versionInfoTable
        DICOMFileNames
        keyDICOMHeaderInfoTable
        CTSeriesSummary
    end
    
    properties (Constant = true)
        dependentRepositories = {'imquest'};
        versionInfoTableFileName = 'imquestVersionInfo.csv';
    end
    
    properties (SetAccess = immutable)
        paths           %Structured variable containing several important paths
        gitSHA1CheckSums %Cell array of git branch unique id's corresponding to each dependent repository
        gitBranchNames   %Cell array of git branch names corresponding  to each dependent repository
    end
    
    events
        newCTDataLoaded
        nTTFROIsChanged
        nTTFzROIsChanged
        nNPSROIsChanged
        nTTFsChanged
        nTTFzsChanged
        nNPSsChanged
        nDprimesChanged
    end
    
    methods
        function tool = imquest(varargin)   %constructor
            tool.logMessage('Info','Starting imquest')
            %Get the root directory
            p = mfilename('fullpath');
            [root, ~, ~] = fileparts(p);
            root = [root filesep];
            paths.root=root;
            tool.paths=paths;

            %Set the directory to the user's home directory
            tool.lastDir=getHomeDirectory;
            
            %Get git info (only when running within Matlab environment);
            if ~isdeployed
                %Check to make sure that dependent repositories are available
                [proceed,RepoPaths] = checkForDependentRespositories(tool.dependentRepositories);
                if ~proceed
                    warning('Missing one or more dependent repositories')
                end
                
                %Get the repository information
                for i=1:length(RepoPaths)
                    [ids{i,1},branchNames{i,1}] = getGitCommitID(RepoPaths{i});
                end
                
                %Set the version info properties
                tool.gitSHA1CheckSums=ids;
                tool.gitBranchNames=branchNames;
                
                %Save the version info file
                try
                    writetable(tool.versionInfoTable,[root tool.versionInfoTableFileName]);
                end
            else %This run if imquest is running as a deployed (i.e. standalone) program
                ids=repmat({'UNKOWN'},size(tool.dependentRepositories));
                branchNames=repmat({'UNKOWN'},size(tool.dependentRepositories));
                RepoPaths=repmat({''},size(tool.dependentRepositories));
                try %Read in the version table
                    tab = readtable([root tool.versionInfoTableFileName]);
                    for i=1:length(tool.dependentRepositories)
                        ind = find(strcmp(tab.RepositoryName,tool.dependentRepositories{i}));
                        ids{i} = tab.GitID{ind};
                        branchNames{i} = tab.BranchName{ind};
                    end       
                end
                %Set the version info properties
                tool.gitSHA1CheckSums=ids;
                tool.gitBranchNames=branchNames;

            end
            
            
            %Re-set the paths property
            paths.repos=RepoPaths;
            tool.paths=paths;
            
            
            %make the figure and menu items--------------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,[],'fig');
            tool.handles.fig = figure('Units','Normalized','Position',...
                [.075 .075 .85 .85],'Color','k','MenuBar','none','Name','imquest',...
                'Tag','tool.handles.fig','NumberTitle','off','ResizeFcn',fun,'Visible','off');
            tool.handles.menu.file.base=uimenu(tool.handles.fig,'Label','File');
            tool.handles.menu.file.loadImage=uimenu(tool.handles.menu.file.base,... %Load image menu
                'Label','Load CT Images','Callback', {@callbacks,tool});
            tool.handles.menu.file.sortDICOMS=uimenu(tool.handles.menu.file.base,... %Sort dicoms menus
                'Label','Sort all DICOM files in folder by series','Callback', {@callbacks,tool});
            tool.handles.menu.file.export.base=uimenu(tool.handles.menu.file.base,... %Export base
                'Label','Export...','Callback', {@callbacks,tool});
            tool.handles.menu.file.export.TTFselected=uimenu(tool.handles.menu.file.export.base,... %Export TTF
                'Label','Selected TTF','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.file.export.TTFzselected=uimenu(tool.handles.menu.file.export.base,... %Export TTF
                'Label','Selected TTFz','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.file.export.NPSselected=uimenu(tool.handles.menu.file.export.base,... %Export NPS
                'Label','Selected NPS','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.file.export.Dprimeselected=uimenu(tool.handles.menu.file.export.base,... %Export Dprime
                'Label','Selected D-prime','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.file.export.DprimeAll=uimenu(tool.handles.menu.file.export.base,... %Export Dprimes
                'Label','All D-primes','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.file.viewLog=uimenu(tool.handles.menu.file.base,... %Sort dicoms menus
                'Label','View log','Callback', {@callbacks,tool});
            tool.handles.menu.view.base=uimenu(tool.handles.fig,...         %View menu
                'Label','View','Callback', {@callbacks,tool},'Enable','on','Visible','off');
            tool.handles.menu.view.DprimeSizeContrast=uimenu(tool.handles.menu.view.base,...  %Dprime vs size and contrast menu
                'Label','Show d'' vs contrast and task size','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.auto.base=uimenu(tool.handles.fig,...         %Auto process menu
                'Label','Auto Process','Callback', {@callbacks,tool},'Enable','on','Visible','on');
            tool.handles.menu.auto.ACRPhantom=uimenu(tool.handles.menu.auto.base,... 
                'Label','Analyze ACR Phantom','Callback', {@callbacks,tool},'Enable','on');
            tool.handles.menu.auto.MercPhantom=uimenu(tool.handles.menu.auto.base,... 
                'Label','Analyze Mercury Phantom','Callback', {@callbacks,tool},'Enable','on');
            tool.handles.menu.auto.MercPhantom3=uimenu(tool.handles.menu.auto.MercPhantom,... 
                'Label','Analyze Mercury Phantom 3.0','Callback', {@callbacks,tool},'Enable','on');
            tool.handles.menu.auto.MercPhantom4=uimenu(tool.handles.menu.auto.MercPhantom,... 
                'Label','Analyze Mercury Phantom 4.0','Callback', {@callbacks,tool},'Enable','on');
            tool.handles.menu.auto.MercPhantomConfig=uimenu(tool.handles.menu.auto.MercPhantom,... 
                'Label','Analyze Mercury Phantom w/ config file','Callback', {@callbacks,tool},'Enable','on');
            tool.handles.menu.figures.base = uimenu(tool.handles.fig,...         %Save Figure
                'Label','Save Figure','Callback', {@callbacks,tool},'Enable','on','Visible','on');
            tool.handles.menu.figures.PhysReport=uimenu(tool.handles.menu.figures.base,... 
                'Label','Physics Report Figure','Callback', {@callbacks,tool},'Enable','off');
            tool.handles.menu.figure.AECProfile = uimenu(tool.handles.menu.figures.base,... 
                'Label','Tube Current Profile Figure','Callback', {@callbacks,tool},'Enable','off');
            %--------------------------------------------------------------
            
            %Add the drag and drop controller------------------------------
            try
                dndcontrol.initJava();
                fun = @(src,evnt) fileDropCallback(tool,src,evnt);
                tool.handles.DragObject = dndcontrol(tool.handles.fig);
                tool.handles.DragObject.DropFileFcn = fun;
            end
            %--------------------------------------------------------------
            
            %make the imtool3D object--------------------------------------
            tool.handles.imtool_Panel = uipanel(tool.handles.fig,'Position',...
                [0 .15 .5 .85],'Title','Image viewer (Select "File"->"Load CT Images" or drag/drop image folder)','BackgroundColor','k',...
                'ForegroundColor','w','HighlightColor','k','TitlePosition',...
                'lefttop','FontSize',14);
            tool.handles.imtool = imtool3D(zeros(512),[0 0 1 1],tool.handles.imtool_Panel);
            tool.handles.imtool.Visible=false;
            h=getHandles(tool.handles.imtool);
            %Blank invisible axis for showing the CT slice info
            tool.handles.CTtextAxes = axes('Parent',h.Panels.Image,'Units','Normalized','Position',[0 0 1 1],...
                'Visible','off'); hold on
            tool.handles.CTInfotext=text(0,1,'','Parent',tool.handles.CTtextAxes,'Color','y','FontUnits','normalized',...
                'FontSize',1/65,'HorizontalAlignment','left','VerticalAlignment','top',...
                'Interpreter','none','PickableParts','none');
            uistack(tool.handles.CTtextAxes,'top')
            addlistener(tool.handles.imtool,'newSlice',@tool.handlePropEvents);
            h=getHandles(tool.handles.imtool);
            set(h.Tools.SmartBrush,'Visible','off');
            set(h.Tools.PaintBrush,'Visible','off');
            set(h.Tools.Crop,'Visible','off');
            set(h.Tools.Help,'Visible','off');
            set(h.Tools.Save,'Visible','off');
            set(h.Tools.SaveOptions,'Visible','off');
            set(h.Tools.Color,'Visible','off');
            set(h.Tools.Mask,'Visible','off');
            tool.handles.imtool.worldUnits='mm';
            tool.handles.imtool.intensityUnits='HU';
            tool.handles.imtool.showWorldCoordinates=true;
            %--------------------------------------------------------------
            
            %make the panel for the WL buttons-----------------------------
            buff = 30; %buffer (in pixels) used when seperating graphical elements
            tool.handles.WLPanel = uipanel(tool.handles.fig,'Position',...
                [0 .1 .5 .05],'Title','','BackgroundColor','k',...
                'ForegroundColor','w','HighlightColor','k','TitlePosition',...
                'lefttop','FontSize',14);
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'WL');
            set(tool.handles.WLPanel,'ResizeFcn',fun)
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool);
            tool.handles.CTInfoVisibleCheck = uicontrol(tool.handles.WLPanel,'Style','Checkbox','String','Show CT info?',...
                'Units','Pixels','HorizontalAlignment','center','Enable','On','Value',true,'BackgroundColor','k','ForegroundColor','w','Callback',fun);
            tool.handles.CTInfoButton  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','Header','Units','Pixels',...
                'TooltipString','Show DICOM header of current slice','HorizontalAlignment','center','Enable','Off','Callback',fun);
            tool.handles.SetZeroSliceButton = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','Set zero slice','Units','Pixels',...
                'TooltipString','Set current slice as the zero offset slice for ACR analysis','HorizontalAlignment','center','Enable','Off','Callback',fun);
            tool.handles.SetZeroSliceButtonText = uicontrol(tool.handles.WLPanel,'Style','text','String','0 mm','Units','Pixels',...
                'HorizontalAlignment','center','Enable','On','Callback',fun,...
                'BackgroundColor','k','ForegroundColor','w');
            tool.handles.CT_WLbutton(1)  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','ACR-1','Units','Normalized',...
                'TooltipString','Window/Level for ACR Phantom Module 1','HorizontalAlignment','center','Enable','Off','UserData',[400 0],'Callback',fun);
            tool.handles.CT_WLbutton(2)  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','ACR-2','Units','Normalized',...
                'TooltipString','Window/Level for ACR Phantom Module 2','HorizontalAlignment','center','Enable','Off','UserData',[100 100],'Callback',fun);
            tool.handles.CT_WLbutton(3)  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','ACR-3','Units','Normalized',...
                'TooltipString','Window/Level for ACR Phantom Module 3','HorizontalAlignment','center','Enable','Off','UserData',[100 0],'Callback',fun);
            tool.handles.CT_WLbutton(4)  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','ACR-4','Units','Normalized',...
                'TooltipString','Window/Level for ACR Phantom Module 4','HorizontalAlignment','center','Enable','Off','UserData',[100 1100],'Callback',fun);
            tool.handles.CT_WLbutton(5)  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','Mercury','Units','Normalized',...
                'TooltipString','Window/Level for Mercury Phantom','HorizontalAlignment','center','Enable','Off','UserData',[200 -100],'Callback',fun);
            tool.handles.CT_WLbutton(6)  = uicontrol(tool.handles.WLPanel,'Style','pushbutton','String','Default','Units','Normalized',...
                'TooltipString','Default Window/Level','HorizontalAlignment','center','Enable','Off','UserData',[],'Callback',fun);
            %--------------------------------------------------------------
            
            %make the bottom banner for the logo---------------------------
            tool.handles.bannerAxis=axes(tool.handles.fig,'Units','Normalized','Position',[0 0 1 .1]);
            set(tool.handles.bannerAxis,'Units','Pixels');
            tool.handles.bannerBKGImage=imshow(tool.bannerImage,'Parent',tool.handles.bannerAxis);
            tool.handles.bannerLogoAxis=axes(tool.handles.fig,'Units','Normalized','Position',[0 0 .15 .1]);
            [logo, ~ ,alpha]=imread([tool.paths.root 'CIPG Logo.png'],'png');
            tool.handles.bannerLogoImage = imshow(logo,'Parent',tool.handles.bannerLogoAxis);
            tool.handles.bannerLogoImage.AlphaData=alpha;
            
            
            %--------------------------------------------------------------
            
            %make invisible axis for status text
            tool.handles.statusAxis = axes(tool.handles.fig,'Units','Normalized','Position',[0 0 1 .1],'Visible','off','Xlim',[0 1],'Ylim',[0 1]);
            hold on;
            tool.handles.statusText = text(1,0,tool.status,'HorizontalAlignment','right','VerticalAlignment','bottom','Color','r','FontSize',14);
            %--------------------------------------------------------------
            
            %Make the tab group--------------------------------------------
            tool.handles.tabs.base = uitabgroup(tool.handles.fig,'Units',...
                'Normalized','Position',[.5 .1 .5 .9],'TabLocation','top');
            tool.handles.tabs.TTF = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','TTF');
            tool.handles.tabs.TTFz = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','TTFz');
            tool.handles.tabs.NPS = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','NPS');
            tool.handles.tabs.Dprime = uitab(tool.handles.tabs.base,...
                'BackgroundColor','k','ForegroundColor','k','Title','Detectability');
            %--------------------------------------------------------------
            
            %Make the TTF tab----------------------------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'TTF');
            set(tool.handles.tabs.TTF,'ResizeFcn',fun)
            %Axis
            tool.handles.TTFaxis = axes('Parent',tool.handles.tabs.TTF,'Units','Pixels');
            set(tool.handles.TTFaxis,'Color','k','XColor','w','YColor','w','XLim',[0 1.2],'XTick',0:.1:1.2,'Ylim',[0 1.1],'YTick',0:.1:1);
            xlabel('f (mm^-^1)'); ylabel('TTF'); hold on; box on; grid on;
            %Add ROI button
            tool.handles.AddTTFROIButton = uicontrol(tool.handles.tabs.TTF,'Style','pushbutton','String','Add TTF ROI','Units','Pixel',...
                'TooltipString','Add TTF ROI','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool);
            set(tool.handles.AddTTFROIButton,'Callback',fun)
            addlistener(tool,'newCTDataLoaded',@tool.handlePropEvents);
            %Add check box for TTF roi visibility
            tool.handles.TTFVisibleCheck = uicontrol(tool.handles.tabs.TTF,'Style','Checkbox','String','Show TTF ROIs?',...
                'Units','Pixel','HorizontalAlignment','center','Enable','Off','Value',true,'BackgroundColor','k','ForegroundColor','w');
            set(tool.handles.TTFVisibleCheck,'Callback',fun)
            %Add clear ROI button
            tool.handles.ClearTTFROIButton = uicontrol(tool.handles.tabs.TTF,'Style','pushbutton','String','Clear TTF ROIs','Units','Pixel',...
                'TooltipString','Clear all TTF ROIs','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.ClearTTFROIButton,'Callback',fun)
            addlistener(tool,'nTTFROIsChanged',@tool.handlePropEvents);
            %Add the slice selector
            tool.handles.TTFsliceSelector = minMaxSlider(tool.handles.tabs.TTF,[0.5000 0.5500 0.1000 0.4000]);
            tool.handles.TTFsliceSelector.enable = false; tool.handles.TTFsliceSelector.units = 'pixels';
            tool.handles.TTFsliceSelector.label = 'Select Slice Range';
            %Add Measure TTF Button
            tool.handles.MeasureTTFROIButton = uicontrol(tool.handles.tabs.TTF,'Style','pushbutton','String','Measure TTF','Units','Pixel',...
                'TooltipString','Measured TTF','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.MeasureTTFROIButton,'Callback',fun)
            %Add   for measured TTFs
            tool.handles.MeasureTTFList = uicontrol(tool.handles.tabs.TTF,'Style','listbox','Units','Pixel',...
                'TooltipString','Select a TTF to see more info','Enable','Off','BackgroundColor','k','ForegroundColor','w');
            addlistener(tool,'nTTFsChanged',@tool.handlePropEvents);
            set(tool.handles.MeasureTTFList,'Callback',fun)
            %Add delete and info buttons for measured TTF
            tool.handles.DeleteTTFButton = uicontrol(tool.handles.tabs.TTF,'Style','pushbutton','String','-','Units','Pixel',...
                'TooltipString','Delete TTF','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.DeleteTTFButton,'Callback',fun)
            tool.handles.ShowTTFInfoButton = uicontrol(tool.handles.tabs.TTF,'Style','pushbutton','String','?','Units','Pixel',...
                'TooltipString','Show Full TTF Info','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.ShowTTFInfoButton,'Callback',fun)
            %Add the text object for displaying the TTF properties
            tool.handles.TTFInfoText = uicontrol(tool.handles.tabs.TTF,'Style','text','String','','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            %Add the text object for labeling TTF listbox
            tool.handles.TTFListText = uicontrol(tool.handles.tabs.TTF,'Style','text','String','Measured TTFs','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            %--------------------------------------------------------------
            
            %Make TTFz tab-------------------------------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'TTFz');
            set(tool.handles.tabs.TTFz,'ResizeFcn',fun)
            %Axis
            tool.handles.TTFzaxis = axes('Parent',tool.handles.tabs.TTFz,'Units','Pixels');
            set(tool.handles.TTFzaxis,'Color','k','XColor','w','YColor','w','XLim',[0 1.2],'XTick',0:.1:1.2,'Ylim',[0 1.1],'YTick',0:.1:1);
            xlabel('f (mm^-^1)'); ylabel('TTFz'); hold on; box on; grid on;
            %Add ROI button
            tool.handles.AddTTFzROIButton = uicontrol(tool.handles.tabs.TTFz,'Style','pushbutton','String','Add TTFz ROI','Units','Pixel',...
                'TooltipString','Add TTFz ROI','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool);
            set(tool.handles.AddTTFzROIButton,'Callback',fun)
            %Add check box for TTF roi visibility
            tool.handles.TTFzVisibleCheck = uicontrol(tool.handles.tabs.TTFz,'Style','Checkbox','String','Show TTFz ROIs?',...
                'Units','Pixel','HorizontalAlignment','center','Enable','Off','Value',true,'BackgroundColor','k','ForegroundColor','w');
            set(tool.handles.TTFVisibleCheck,'Callback',fun)
            %Add clear ROI button
            tool.handles.ClearTTFzROIButton = uicontrol(tool.handles.tabs.TTFz,'Style','pushbutton','String','Clear TTFz ROIs','Units','Pixel',...
                'TooltipString','Clear all TTFz ROIs','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.ClearTTFzROIButton,'Callback',fun)
            addlistener(tool,'nTTFzROIsChanged',@tool.handlePropEvents);
            %Add the slice selector
            tool.handles.TTFzsliceSelector = minMaxSlider(tool.handles.tabs.TTFz,[0.5000 0.5500 0.1000 0.4000]);
            tool.handles.TTFzsliceSelector.enable = false; tool.handles.TTFzsliceSelector.units = 'pixels';
            tool.handles.TTFzsliceSelector.label = 'Select Slice Range';
            %Add Measure TTF Button
            tool.handles.MeasureTTFzROIButton = uicontrol(tool.handles.tabs.TTFz,'Style','pushbutton','String','Measure TTFz','Units','Pixel',...
                'TooltipString','Measured TTFz','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.MeasureTTFzROIButton,'Callback',fun)
            %Add   for measured TTFs
            tool.handles.MeasureTTFzList = uicontrol(tool.handles.tabs.TTFz,'Style','listbox','Units','Pixel',...
                'TooltipString','Select a TTFz to see more info','Enable','Off','BackgroundColor','k','ForegroundColor','w');
            addlistener(tool,'nTTFzsChanged',@tool.handlePropEvents);
            set(tool.handles.MeasureTTFzList,'Callback',fun)
            %Add delete and info buttons for measured TTF
            tool.handles.DeleteTTFzButton = uicontrol(tool.handles.tabs.TTFz,'Style','pushbutton','String','-','Units','Pixel',...
                'TooltipString','Delete TTF','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.DeleteTTFzButton,'Callback',fun)
            tool.handles.ShowTTFzInfoButton = uicontrol(tool.handles.tabs.TTFz,'Style','pushbutton','String','?','Units','Pixel',...
                'TooltipString','Show Full TTF Info','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.ShowTTFzInfoButton,'Callback',fun)
            %Add the text object for displaying the TTF properties
            tool.handles.TTFzInfoText = uicontrol(tool.handles.tabs.TTFz,'Style','text','String','','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            %Add the text object for labeling TTF listbox
            tool.handles.TTFzListText = uicontrol(tool.handles.tabs.TTFz,'Style','text','String','Measured TTFzs','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            %--------------------------------------------------------------
            
            %Make the NPS tab----------------------------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'NPS');
            set(tool.handles.tabs.NPS,'ResizeFcn',fun)
            %Axis
            tool.handles.NPSaxis = axes('Parent',tool.handles.tabs.NPS,'Units','Pixels');
            set(tool.handles.NPSaxis,'Color','k','XColor','w','YColor','w','XLim',[0 1.2],'XTick',0:.1:1.2);
            xlabel('f (mm^-^1)'); ylabel('NPS (HU^2mm^2)'); hold on; box on; grid on;
            %Add ROI button
            tool.handles.AddNPSROIButton = uicontrol(tool.handles.tabs.NPS,'Style','pushbutton','String','Add NPS ROI','Units','Pixel',...
                'TooltipString','Add NPS ROI','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool);
            set(tool.handles.AddNPSROIButton,'Callback',fun)
            %Add check box for NPS roi visibility
            tool.handles.NPSVisibleCheck = uicontrol(tool.handles.tabs.NPS,'Style','Checkbox','String','Show NPS ROIs?',...
                'Units','Pixel','HorizontalAlignment','center','Enable','Off','Value',true,'BackgroundColor','k','ForegroundColor','w');
            set(tool.handles.NPSVisibleCheck,'Callback',fun)
            %Add clear ROI button
            tool.handles.ClearNPSROIButton = uicontrol(tool.handles.tabs.NPS,'Style','pushbutton','String','Clear NPS ROIs','Units','Pixel',...
                'TooltipString','Clear all NPS ROIs','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.ClearNPSROIButton,'Callback',fun)
            addlistener(tool,'nNPSROIsChanged',@tool.handlePropEvents);
            %Add the slice selector
            tool.handles.NPSsliceSelector = minMaxSlider(tool.handles.tabs.NPS,[0.5000 0.5500 0.1000 0.4000]);
            tool.handles.NPSsliceSelector.enable = false; tool.handles.NPSsliceSelector.units = 'pixels';
            tool.handles.NPSsliceSelector.label = 'Select Slice Range';
            %Add Measure NPS Button
            tool.handles.MeasureNPSROIButton = uicontrol(tool.handles.tabs.NPS,'Style','pushbutton','String','Measure NPS','Units','Pixel',...
                'TooltipString','Measured NPS','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool);
            set(tool.handles.MeasureNPSROIButton,'Callback',fun)
            %Add listbox for measured NPSs
            tool.handles.MeasureNPSList = uicontrol(tool.handles.tabs.NPS,'Style','listbox','Units','Pixel',...
                'TooltipString','Select a NPS to see more info','Enable','Off','BackgroundColor','k','ForegroundColor','w');
            addlistener(tool,'nNPSsChanged',@tool.handlePropEvents);
            set(tool.handles.MeasureNPSList,'Callback',fun)
            %Add delete and info buttons for measured NPS
            tool.handles.DeleteNPSButton = uicontrol(tool.handles.tabs.NPS,'Style','pushbutton','String','-','Units','Pixel',...
                'TooltipString','Delete NPS','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.DeleteNPSButton,'Callback',fun)
            tool.handles.ShowNPSInfoButton = uicontrol(tool.handles.tabs.NPS,'Style','pushbutton','String','?','Units','Pixel',...
                'TooltipString','Show Full NPS Info','HorizontalAlignment','center','Enable','Off');
            set(tool.handles.ShowNPSInfoButton,'Callback',fun)
            %Add the text object for displaying the NPS properties
            tool.handles.NPSInfoText = uicontrol(tool.handles.tabs.NPS,'Style','text','String','','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            %Add the text object for labeling NPS listbox
            tool.handles.NPSListText = uicontrol(tool.handles.tabs.NPS,'Style','text','String','Measured NPSs','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            %Add axis for showing the 2D NPS
            tool.handles.NPS2DAxis = axes('Parent',tool.handles.tabs.NPS,'Units','Pixels','Color','k','XColor','w','YColor','w','Visible','off'); hold on
            tool.handles.NPS2DImage = imshow(zeros(128),'Parent',tool.handles.NPS2DAxis,'XData',[-1.2 1.2],'Ydata',[-1.2 1.2]);
            set(tool.handles.NPS2DImage,'Visible','off')
            axis image
            %--------------------------------------------------------------
            
            %Make the d-prime tab------------------------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Detectability');
            set(tool.handles.tabs.Dprime,'ResizeFcn',fun)
            %Create task function panel
            tool.handles.taskFunctionController = imquest_TaskFunctionController(tool.handles.tabs.Dprime,[0 .5 1 .5],tool.taskFunction);
            %Create the observer model panel
            tool.handles.ObserverModelController = imquest_ObserverModelController(tool.handles.tabs.Dprime,[0 0 1 .5],tool);
            %--------------------------------------------------------------
            
            %set the userDefined optional settings
            %set the optional properties
            switch length(varargin)
                case 1
                    setOptionalSettings(tool,varargin{1})
                otherwise
                    setOptionalSettings(tool,varargin)
            end
            
            %Run resize function to force all GUI elements to be
            %positioned properly
            panelResizeFunction([],[],tool,buff,'')
            tool.Visible=tool.Visible;
            tool.logMessage('Info','imquest ready')

        end
        
        function handlePropEvents(tool,src,evnt,varargin)
            %this function handles all the events that are being listened
            %for
            switch evnt.EventName
                case 'newSlice'
                    showNewSlice(tool)
                    
                case 'newCTDataLoaded'
                    %adjust the range of the slice handles
                    S = getImageSize(tool.handles.imtool);
                    tool.handles.TTFsliceSelector.globalMax = S(3);
                    tool.handles.TTFzsliceSelector.globalMax = S(3);
                    tool.handles.NPSsliceSelector.globalMax = S(3);
                    notify(tool.handles.imtool,'newSlice')
                case 'ROIdeleted'
                    removeTTFROI(tool,src)
                    removeTTFzROI(tool,src)
                    removeNPSROI(tool,src)
                case 'nTTFROIsChanged'
                    if tool.nTTFROIs>0
                        set(tool.handles.TTFVisibleCheck,'Enable','on')
                        set(tool.handles.MeasureTTFROIButton,'Enable','on');
                        set(tool.handles.ClearTTFROIButton,'Enable','on')
                        tool.handles.TTFsliceSelector.enable = true;
                        
                    else
                        set(tool.handles.TTFVisibleCheck,'Enable','off')
                        set(tool.handles.MeasureTTFROIButton,'Enable','off');
                        set(tool.handles.ClearTTFROIButton,'Enable','off')
                        tool.handles.TTFsliceSelector.enable = false;
                        
                    end
                    runCallback(tool,tool.handles.TTFVisibleCheck,[]);
                case 'nTTFzROIsChanged'
                    if tool.nTTFzROIs>0
                        set(tool.handles.TTFzVisibleCheck,'Enable','on')
                        set(tool.handles.MeasureTTFzROIButton,'Enable','on');
                        set(tool.handles.ClearTTFzROIButton,'Enable','on')
                        tool.handles.TTFzsliceSelector.enable = true;
                    else
                        set(tool.handles.TTFzVisibleCheck,'Enable','off')
                        set(tool.handles.MeasureTTFzROIButton,'Enable','off');
                        set(tool.handles.ClearTTFzROIButton,'Enable','off')
                        tool.handles.TTFzsliceSelector.enable = false;
                        
                    end
                case 'nNPSROIsChanged'
                    if tool.nNPSROIs>0
                        set(tool.handles.NPSVisibleCheck,'Enable','on')
                        set(tool.handles.MeasureNPSROIButton,'Enable','on');
                        set(tool.handles.ClearNPSROIButton,'Enable','on')
                        tool.handles.NPSsliceSelector.enable = true;
                        
                    else
                        set(tool.handles.NPSVisibleCheck,'Enable','off')
                        set(tool.handles.MeasureNPSROIButton,'Enable','off');
                        set(tool.handles.ClearNPSROIButton,'Enable','off')
                        tool.handles.NPSsliceSelector.enable = false;
                        
                    end
                    runCallback(tool,tool.handles.NPSVisibleCheck,[]);
                case 'nTTFsChanged'
                    updateTTFlist(tool)
                case 'nTTFzsChanged'
                    updateTTFzlist(tool)
                case 'nNPSsChanged'
                    updateNPSlist(tool)
                    
                    
            end
        end
        
        function setOptionalSettings(tool,args)
            
            if ~isempty(args)
                for i=1:2:length(args)
                    tool.(args{i})=args{i+1};
                end
            end
            
        end
        
        function set.status(tool,status)
            set(tool.handles.statusText,'String',status)
            drawnow
            tool.status=status;
            log = char(strcat(string(datetime), ':Status update: ', status)); 
            tool.log{end+1,1}=log;
        end
        
        function logMessage(tool,type, message)
            log = char(strcat(string(datetime), ':', type, ': ', message));
            tool.log{end+1,1}=log;
        end
        
        function logError(tool,Error)
            log = char(strcat(string(datetime), ':Error: ', Error.identifier,':'));
            tool.log{end+1,1}=log;
            tool.log{end+1,1}=char(strcat('Error message: ',Error.message));
            for i=1:length(Error.stack)
                s = Error.stack(i);
                tool.log{end+1,1}= sprintf('file: %s, name; %s, line: %d',s.file,s.name,s.line);
            end
        end
        
        function set.lastDir(tool,lastDir)
            if isdir(lastDir)
                if ~strcmp(lastDir(end),filesep)
                    lastDir=[lastDir filesep];
                end
                tool.lastDir=lastDir;
                
            end
        end
        
        function set.ACRZeroOffset(tool,newOffset)
            tool.ACRZeroOffset = newOffset;
            set(tool.handles.SetZeroSliceButtonText,'String',[num2str(newOffset) ' mm'])
        end
        
        function showTempStatus(tool,status)
            oldStatus = tool.status;
            tool.status=status;
            pause(3);
            tool.status=oldStatus;
        end
           
        function loadCTdata(tool,input)
            tool.status='Reading CT Data';
            try
            %load the images
            [I, info]=readCTSeries(input);
            %put the image data on the imtool3D object
            setImage(tool.handles.imtool, I)
            try
                setWindowLevel(tool.handles.imtool,info(1).WindowWidth(1),info(1).WindowCenter(1))
            end
            tool.DICOMheaders=info;
            tool.CTDataLoaded = true;
            % added next line to display Siemens dose page, which is stored as an overlay object (STB 8/27/2021)
            if strcmp(tool.DICOMheaders(1).SeriesDescription,'Patient Protocol'), setImage(tool.handles.imtool, tool.DICOMheaders.OverlayData_0), end
            notify(tool,'newCTDataLoaded')
            %Set the lastDir
            if strcmp(input(end),filesep) %Remove file seperator if needed at the end
                input = input(1:end-1);
            end
            k = strfind(input,filesep);
            input = input(1:k(end));
            tool.lastDir=input;
            catch Error
                tool.logError(Error)
                showTempStatus(tool,'Error reading CT Images')
            end
            tool.status=tool.defaultStatus;
            
            
        end
        
        function sortDICOMfiles(tool,input,varargin)
            tool.status='Sorting DICOM files by series';
            if strcmp(input(end),filesep)
                input = input(1:end-1);
            end
            if nargin==3
                output = varargin{1};
            else
                
                output = [input '_SortedByImquest' filesep];
            end
            try
                sortDicomsRecursive(input,output);
                k = strfind(output,filesep);
                output = output(1:k(end));
                tool.lastDir=output;
                showTempStatus(tool,'Done sorting DICOM files')
            catch ME
                tool.logError(ME)
                showTempStatus(tool,'Error sorting DICOM files')
            end
            tool.status=tool.defaultStatus;
        end
        
        function set.CTDataLoaded(tool,CTDataLoaded)
            tool.CTDataLoaded = CTDataLoaded;
            if CTDataLoaded
                set(tool.handles.AddTTFROIButton,'Enable','on')
                set(tool.handles.AddTTFzROIButton,'Enable','on')
                set(tool.handles.AddNPSROIButton,'Enable','on')
                set(tool.handles.CT_WLbutton,'Enable','on');
                set(tool.handles.CTInfoButton,'Enable','on');
                set(tool.handles.SetZeroSliceButton,'Enable','on')
                set(tool.handles.menu.figure.AECProfile,'Enable','on');
                set(tool.handles.imtool_Panel,'Title','Image viewer');
                tool.handles.imtool.Visible=true;
                
            else
                set(tool.handles.AddTTFROIButton,'Enable','off')
                set(tool.handles.AddTTFzROIButton,'Enable','off')
                set(tool.handles.AddNPSROIButton,'Enable','off')
                set(tool.handles.CT_WLbutton,'Enable','off');
                set(tool.handles.SetZeroSliceButton,'Enable','off')
                set(tool.handles.menu.figure.AECProfile,'Enable','off');
                set(tool.handles.imtool_Panel,'Title','Image viewer (Select "File"->"Load CT Images" or drag/drop image folder)');
                tool.handles.imtool.Visible=false;
            end
        end
        
        function measureTTF(tool)
            
            for i=1:tool.nTTFROIs
                try
                    tool.status=['Measuring TTF ' num2str(i) '/' num2str(tool.nTTFROIs)];
                    imquest_TTF(tool,tool.TTFROIs(i));
                catch ME
                    tool.logError(ME)
                    showTempStatus(tool,['Error measuring TTF ' num2str(i) '/' num2str(tool.nTTFROIs)])
                    disp('Error Stack:')
                    for i=1:length(ME.stack)
                        ME.stack(i)
                    end
                end
            end
            tool.status=tool.defaultStatus;
            
        end
        
        function measureTTFz(tool)
            
            for i=1:tool.nTTFzROIs
                try
                    tool.status=['Measuring TTFz ' num2str(i) '/' num2str(tool.nTTFzROIs)];
                    imquest_TTF_z(tool,tool.TTFzROIs(i));
                catch ME
                    tool.logError(ME)
                    showTempStatus(tool,['Error measuring TTFz ' num2str(i) '/' num2str(tool.nTTFzROIs)])
                    disp('Error Stack:')
                    for i=1:length(ME.stack)
                        ME.stack(i)
                    end
                end
            end
            tool.status=tool.defaultStatus;
            
        end
        
        function InnerCircleROI = getMatchingTTFInnerCircleROI(tool,TTFROI)
            for i=1:length(tool.TTFInnerCircleROIs)
                ROIs(i) = tool.TTFInnerCircleROIs(i).ROI;
            end
            InnerCircleROI = tool.TTFInnerCircleROIs(find(TTFROI==ROIs));
        end
        
        function InnerCircleROI = getMatchingTTFzInnerCircleROI(tool,TTFROI)
            for i=1:length(tool.TTFzInnerCircleROIs)
                ROIs(i) = tool.TTFzInnerCircleROIs(i).ROI;
            end
            InnerCircleROI = tool.TTFzInnerCircleROIs(find(TTFROI==ROIs));
        end
        
        function measureNPS(tool)
            try
                tool.status = ['Measuring NPS from ' num2str(tool.nNPSROIs) ' ROIs'];
                imquest_NPS(tool,tool.NPSROIs);
            catch ME
                tool.logError(ME)
                showTempStatus(tool,'Error measuring NPS');
            end
            tool.status=tool.defaultStatus;
        end
        
        function im = getPixelValues(tool,ROI,slider)
            
            zmin = round(slider.selectedMin);
            zmax = round(slider.selectedMax);
            lims = getBoundingBox(ROI);
            lims = [lims; zmin zmax];
            im = getImageValues(tool.handles.imtool,lims);
            
        end
        
        function varargout = getImageSlices(tool,slider)
            zmin = round(slider.selectedMin);
            zmax = round(slider.selectedMax);
            S = getImageSize(tool.handles.imtool);
            lims = [1 S(1); 1 S(2); zmin zmax];
            switch nargout
                case 1
                    varargout{1} = getImageValues(tool.handles.imtool,lims);
                case 2
                    varargout{1} = getImageValues(tool.handles.imtool,lims);
                    varargout{2} = [zmin zmax];
            end
        end
        
        function setTTFSliceRange(tool,range)
            setRange(tool.handles.TTFsliceSelector,range);
        end
        
        function setTTFzSliceRange(tool,range)
            setRange(tool.handles.TTFzsliceSelector,range);
        end
        
        function varargout = addTTFROI(tool,varargin)
            %parse the inputs
            n = tool.nTTFROIs;
            switch nargin
                case 1
                    psize = tool.CTpsize;
                   
                    S = getImageSize(tool.handles.imtool);
                    position = zeros(1,4);
                    position(1) = S(2)/2; position(2) = S(1)/2;
                    position(3) = (tool.TTF_rod/tool.TTF_R)/psize(1);
                    position(4) = (tool.TTF_rod/tool.TTF_R)/psize(2);
                    %position =[256 256 64 64];
                case 2
                    position = varargin{1};
            end
            
            %Create ROI object
            if tool.CTDataLoaded
                h = getHandles(tool.handles.imtool);
                ROI = imtool3DROI_ellipse(h.I,position);
                ROI.textVisible=false;
                ROI.lineColor=[4, 55, 131]/256;
                ROI.fixedAspectRatio=true;
                innerROI = TTFinnerCircle(ROI);
                innerROI.R = tool.TTF_R;
                tool.TTFROIs = [tool.TTFROIs ROI];
                tool.TTFInnerCircleROIs = [tool.TTFInnerCircleROIs innerROI];
                %Add a listener for the ROI being deleted (so the ROI list
                %can be appropriately updated if the user deletes the ROI)
                %fun = @(tool,src,evnt) handlePropEvents(tool,src,evnt,'TTFROI'); 
                %addlistener(ROI,'ROIdeleted',fun);
                addlistener(ROI,'ROIdeleted',@tool.handlePropEvents);
            else
                ROI = [];
            end
            
            switch nargout
                case 1
                    varargout{1} = ROI;
            end
            nNew = tool.nTTFROIs;
            if n~=nNew
                notify(tool,'nTTFROIsChanged')
            end
        end
        
        function varargout = addTTFzROI(tool,varargin)
            n = tool.nTTFzROIs;
            switch nargin
                case 1
                    psize = tool.CTpsize;
                    S = getImageSize(tool.handles.imtool);
                    position = zeros(1,4);
                    position(1) = S(2)/2; position(2) = S(1)/2;
                    position(3) = (tool.TTFz_rod/tool.TTFz_R)/psize(1);
                    position(4) = (tool.TTFz_rod/tool.TTFz_R)/psize(2);
                    %position =[256 256 64 64];
                case 2
                    position = varargin{1};
            end
            
            %Create ROI object
            if tool.CTDataLoaded
                h = getHandles(tool.handles.imtool);
                ROI = imtool3DROI_ellipse(h.I,position);
                ROI.textVisible=false;
                ROI.lineColor=[4, 55, 131]/256;
                ROI.fixedAspectRatio=true;
                innerROI = TTFinnerCircle(ROI);
                innerROI.R = tool.TTFz_R;
                tool.TTFzROIs = [tool.TTFzROIs ROI];
                tool.TTFzInnerCircleROIs = [tool.TTFzInnerCircleROIs innerROI];
                addlistener(ROI,'ROIdeleted',@tool.handlePropEvents);
            else
                ROI=[];
            end
            switch nargout
                case 1
                    varargout{1} = ROI;
            end
            nNew = tool.nTTFzROIs;
            if n~=nNew
                notify(tool,'nTTFzROIsChanged')
            end
            
        end
        
        function removeTTFROI(tool,varargin)
            
            n = tool.nTTFROIs;
            switch nargin
                case 1
                    %remove all the ROIs
                    delete(tool.TTFROIs);
                    tool.TTFROIs = [];
                    tool.TTFInnerCircleROIs = [];
                case 2
                    %Remove a specific ROI
                    ROI = varargin{1};
                    ind = tool.TTFROIs==ROI;
                    delete(tool.TTFROIs(ind)); %Dont need to delete the inner circle ROI because it get deleted automatically when its parent ROI is deleted
                    tool.TTFROIs = tool.TTFROIs(~ind);
                    tool.TTFInnerCircleROIs = tool.TTFInnerCircleROIs(~ind);
                    if isempty(tool.TTFROIs)
                        tool.TTFROIs = [];
                        tool.TTFInnerCircleROIs = [];
                    end
            end
            nNew = tool.nTTFROIs;
            if n~=nNew
                notify(tool,'nTTFROIsChanged')
            end
            
            
            
            
        end
        
        function removeTTFzROI(tool,varargin)
            n = tool.nTTFzROIs;
            switch nargin
                case 1
                    %remove all the ROIs
                    delete(tool.TTFzROIs);
                    tool.TTFzROIs = [];
                    tool.TTFzInnerCircleROIs = [];
                case 2
                    %Remove a specific ROI
                    ROI = varargin{1};
                    ind = tool.TTFzROIs==ROI;
                    delete(tool.TTFzROIs(ind)); %Dont need to delete the inner circle ROI because it get deleted automatically when its parent ROI is deleted
                    tool.TTFzROIs = tool.TTFzROIs(~ind);
                    tool.TTFzInnerCircleROIs = tool.TTFzInnerCircleROIs(~ind);
                    if isempty(tool.TTFzROIs)
                        tool.TTFzROIs = [];
                        tool.TTFzInnerCircleROIs = [];
                    end
            end
            nNew = tool.nTTFzROIs;
            if n~=nNew
                notify(tool,'nTTFzROIsChanged')
            end
        
        end
        
        function nTTFROIs = get.nTTFROIs(tool)
            nTTFROIs = length(tool.TTFROIs);
        end
        
        function nTTFzROIs = get.nTTFzROIs(tool)
            nTTFzROIs = length(tool.TTFzROIs);
        end
        
        function can = get.canComputeDetectability(tool)
            
            if tool.nTTFs > 0 && tool.nNPSs > 0
                can = true;
            else
                can = false;
            end
        end
        
        function addDprime(tool,Dprime)
            tool.Dprimes = [tool.Dprimes Dprime];
            notify(tool,'nDprimesChanged');
        end
        
        function nDprimes = get.nDprimes(tool)
            nDprimes = length(tool.Dprimes);
        end
        
        function removeDprime(tool,varargin)
            n = tool.nDprimes;
            switch nargin
                case 1
                    %remove all dprimes
                    delete(tool.Dprimes);
                    tool.Dprimes = [];
                case 2
                    %Remove a specific dprime
                    Dprime = varargin{1};
                    ind = tool.Dprimes == Dprime;
                    delete(tool.Dprimes(ind));
                    tool.Dprimes = tool.Dprimes(~ind);
                    if isempty(tool.Dprimes)
                        tool.Dprimes = [];
                    end
            end
            
            nNew = tool.nDprimes;
            if n~=nNew
                notify(tool,'nDprimesChanged');
            end
        end
        
        function addTTF(tool,TTF)
            tool.TTFs = [tool.TTFs TTF];
            set(tool.handles.MeasureTTFList,'Value',tool.nTTFs);
            notify(tool,'nTTFsChanged');
        end
        
        function addTTFz(tool,TTF)
            tool.TTFzs = [tool.TTFzs TTF];
            set(tool.handles.MeasureTTFzList,'Value',tool.nTTFzs);
            notify(tool,'nTTFzsChanged');
        end
        
        function removeTTF(tool,varargin)
            n = tool.nTTFs;
            switch nargin
                case 1
                    %remove all the TTFs
                    delete(tool.TTFs);
                    tool.TTFs=[];
                case 2
                    %remove a specific TTF
                    TTF = varargin{1};
                    ind = tool.TTFs==TTF;
                    delete(tool.TTFs(ind));
                    tool.TTFs=tool.TTFs(~ind);
                    if isempty(tool.TTFs)
                        tool.TTFs=[];
                    end
            end
            nNew = tool.nTTFs;
            if n~=nNew
                notify(tool,'nTTFsChanged')
            end
        end
        
        function removeTTFz(tool,varargin)
            n = tool.nTTFzs;
            switch nargin
                case 1
                    %remove all the TTFs
                    delete(tool.TTFzs);
                    tool.TTFzs=[];
                case 2
                    %remove a specific TTF
                    TTF = varargin{1};
                    ind = tool.TTFzs==TTF;
                    delete(tool.TTFzs(ind));
                    tool.TTFzs=tool.TTFzs(~ind);
                    if isempty(tool.TTFzs)
                        tool.TTFzs=[];
                    end
            end
            nNew = tool.nTTFzs;
            if n~=nNew
                notify(tool,'nTTFzsChanged')
            end
        end
        
        function updateTTFlist(tool)
            
            if tool.nTTFs>0
                %get the selected value of the TTF list
                val = get(tool.handles.MeasureTTFList,'Value');
                %Update val in case that its not in sync with the list (can
                %happen after a TTF is deleted)
                if val == 0
                    val = 1;
                elseif val > tool.nTTFs
                    val = tool.nTTFs;
                end
                
                
                
                %Update the list in the TTF listbox
                maxTTF=0;
                for i=1:tool.nTTFs
                    if i==val
                        tool.TTFs(i).selected = true;
                    else
                        tool.TTFs(i).selected = false;
                    end
                    str{i} = tool.TTFs(i).label;
                    
                    if tool.TTFs(i).TTFmax>maxTTF
                        maxTTF=tool.TTFs(i).TTFmax;
                    end
                end
                set(tool.handles.MeasureTTFList,'Value',val,'Enable','on','String',str);
                
                
                %Update the text of the TTF that's selected
                set(tool.handles.TTFInfoText,'String',tool.TTFs(val).fullLabel);
                set(tool.handles.DeleteTTFButton,'Enable','on');
                set(tool.handles.ShowTTFInfoButton,'Enable','on');
                set(tool.handles.menu.file.export.TTFselected,'Enable','on');
                
                %Update the axis legend
                set(tool.handles.TTFaxis,'Ylim',[0 maxTTF+.1]);
                legend(tool.handles.TTFaxis,'off');
                h=legend(tool.handles.TTFaxis,str);
                set(h,'TextColor','w','Box','off')
                
                
                
                
            else
                %This happens when all the TTFs are deleted
                set(tool.handles.MeasureTTFList,'Value',1,'Enable','off','String','');
                set(tool.handles.DeleteTTFButton,'Enable','off');
                set(tool.handles.ShowTTFInfoButton,'Enable','off');
                set(tool.handles.TTFInfoText,'String','');
                legend(tool.handles.TTFaxis,'off');
                set(tool.handles.TTFaxis,'Ylim',[0 1.1]);
                set(tool.handles.menu.file.export.TTFselected,'Enable','off');
                
            end
            
        end
        
        function updateTTFzlist(tool)
            
            if tool.nTTFzs>0
                %get the selected value of the TTF list
                val = get(tool.handles.MeasureTTFzList,'Value');
                %Update val in case that its not in sync with the list (can
                %happen after a TTF is deleted)
                if val == 0
                    val = 1;
                elseif val > tool.nTTFzs
                    val = tool.nTTFzs;
                end
                
                
                
                %Update the list in the TTF listbox
                maxTTF=0;
                for i=1:tool.nTTFzs
                    if i==val
                        tool.TTFzs(i).selected = true;
                    else
                        tool.TTFzs(i).selected = false;
                    end
                    str{i} = tool.TTFzs(i).label;
                    
                    if tool.TTFzs(i).TTFmax>maxTTF
                        maxTTF=tool.TTFzs(i).TTFmax;
                    end
                end
                set(tool.handles.MeasureTTFzList,'Value',val,'Enable','on','String',str);
                
                
                %Update the text of the TTF that's selected
                set(tool.handles.TTFzInfoText,'String',tool.TTFzs(val).fullLabel);
                set(tool.handles.DeleteTTFzButton,'Enable','on');
                set(tool.handles.ShowTTFzInfoButton,'Enable','on');
                set(tool.handles.menu.file.export.TTFzselected,'Enable','on');
                
                %Update the axis legend
                set(tool.handles.TTFzaxis,'Ylim',[0 maxTTF+.1]);
                legend(tool.handles.TTFzaxis,'off');
                h=legend(tool.handles.TTFzaxis,str);
                set(h,'TextColor','w','Box','off')
                
                
                
                
            else
                %This happens when all the TTFs are deleted
                set(tool.handles.MeasureTTFzList,'Value',1,'Enable','off','String','');
                set(tool.handles.DeleteTTFzButton,'Enable','off');
                set(tool.handles.ShowTTFzInfoButton,'Enable','off');
                set(tool.handles.TTFzInfoText,'String','');
                legend(tool.handles.TTFzaxis,'off');
                set(tool.handles.TTFzaxis,'Ylim',[0 1.1]);
                set(tool.handles.menu.file.export.TTFzselected,'Enable','off');
                
            end
            
        end
        
        function nTTFs = get.nTTFs(tool)
            nTTFs = length(tool.TTFs);
        end
        
        function nTTFzs = get.nTTFzs(tool)
            nTTFzs = length(tool.TTFzs);
        end
        
        function addNPS(tool,NPS)
            tool.NPSs = [tool.NPSs NPS];
            set(tool.handles.MeasureNPSList,'Value',tool.nNPSs);
            notify(tool,'nNPSsChanged');
        end
        
        function removeNPS(tool,varargin)
            n = tool.nNPSs;
            switch nargin
                case 1
                    %remove all the NPSs
                    delete(tool.NPSs);
                    tool.NPSs=[];
                case 2
                    %remove a specific NPS
                    NPS = varargin{1};
                    ind = tool.NPSs==NPS;
                    delete(tool.NPSs(ind));
                    tool.NPSs=tool.NPSs(~ind);
                    if isempty(tool.NPSs)
                        tool.NPSs=[];
                    end
            end
            nNew = tool.nNPSs;
            if n~=nNew
                notify(tool,'nNPSsChanged')
            end
        end
        
        function nNPSs = get.nNPSs(tool)
            nNPSs = length(tool.NPSs);
        end
        
        function setNPSSliceRange(tool,range)
            setRange(tool.handles.NPSsliceSelector,range);
        end
        
        function autoPlaceNPSROIs(tool,rng,rsize,varargin)
            %This method places ROIs in regions that are estimated to be
            %acceptable for NPS measurement. Works similar to how the
            %global noise index is computed
            tool.status = ['Identifying ROI locations automatically'];
            
            try
                %Adjust the current slice to the slice of interest
                if ~isempty(varargin)
                    setCurrentSlice(tool.handles.imtool,varargin{1})
                end
                
                %Set defaults
                if isempty(rng)
                    rng=[-300 100];
                end
                
                if isempty(rsize)
                    rsize=33;
                end
                rsize=floor(rsize/2)*2+1;
                rsize_im = rsize;
                rsize = 9;
                
                %Get the slice image values
                dcmimg = getCurrentImageSlice(tool.handles.imtool);
                
                %Set unwanted pixels to NaN
                h=fspecial('average',rsize);
                avg=filter2(h,dcmimg);
                vars=stdfilt(dcmimg,ones(rsize));
                vars(1:floor(rsize/2),:)=NaN;
                vars(size(vars,1)-floor(rsize/2):size(vars,1),:)=NaN;
                vars(:,1:floor(rsize/2))=NaN;
                vars(:,size(vars,2)-floor(rsize/2):size(vars,2))=NaN;
                vars(avg<rng(1) | avg>rng(2))=NaN;
                
                %Create histogram
                t=0.1;
                [h,g]=hist(vars(vars<200),(0:t:200));
                h(1)=0;
                h=smooth(h,round(2/t));
                
                %Get the mode +- 10% of max
                i=find(h>0.90*max(h));
                if length(i)>1
                    q=g(i);
                    if 1<(length(q)-1)
                        q=q(1:end-1);
                    elseif 1==(length(q)-1)
                        q=q(1);
                    else
                        q=double.empty(1,0); %NaN JMW 5/3/2017
                    end
                    %%%%%%%%%%%%%%%%%%
                    
                    d=diff(g(i))./q;
                    gap=find(d>0.1,1,'first');
                    %gap=min(find(d>0.1));
                    if gap
                        i(gap+1:end)=[];
                    end
                    
                    g=g(:);
                    h=h(:);
                    noise=mean(g(i).*h(i))/mean(h(i));
                    count=length(i);
                end
                
                %Get the acceptable ROI locations
                rsize = rsize_im;
                vars=stdfilt(dcmimg,ones(rsize+4));
                mask = vars>=noise*.99 & vars <= noise*1.1 & avg >= rng(1);
                %mask = vars>=noise*.99 & vars <= noise & avg >= rng(1);
                
                %Remove edges
                %rsize = 32;
                mask(1:ceil(rsize/2),:)=false;
                mask(:,1:ceil(rsize/2)) = false;
                mask(size(mask,1)-floor(rsize/2):size(mask,1),:)=false;
                mask(:,size(mask,2)-floor(rsize/2):size(mask,2))=false;
                
                %Get pixel coordinates of the mask
                ix=1:size(mask,2); iy = 1:size(mask,2);
                [iX,iY] = meshgrid(ix,iy);
                iX = iX(mask); iY=iY(mask);
                
                %Get only ROI locations with certain seperation
                ind = true(size(iX));
                indN = (1:length(ind))';
                dmin = sqrt(2)*rsize/2;
                i=0;
                while i < length(iX)
                    i=i+1;
                    %Get distance between pixel of interest and other pixels
                    d = sqrt((iX(i)-iX).^2 + (iY(i)-iY).^2);
                    indN = (1:length(iX))';
                    
                    %Remove all pixels within a certain distance
                    ind = ~(d<=dmin & indN>i);
                    iX = iX(ind); iY=iY(ind);
                    
                end
                
                %Add the ROIs
                for i=1:length(iX)
                    addNPSROI(tool,[iX(i) iY(i) rsize rsize]);
                end
                
            catch ME
                tool.logError(ME)
                showTempStatus(tool,'Error getting NPS ROI locations');
            end
            tool.status=tool.defaultStatus;
            
        end
        
        function varargout = addNPSROI(tool,varargin)
            n = tool.nNPSROIs;
            switch nargin
                case 1
                    position =[256 256 64 64];
                case 2
                    position = varargin{1};
            end
            %Create ROI object
            if tool.CTDataLoaded
                h = getHandles(tool.handles.imtool);
                ROI = imtool3DROI_rect(h.I,position);
                ROI.textVisible=false;
                ROI.lineColor=[4, 55, 131]/256;
                ROI.fixedAspectRatio=true;
                tool.NPSROIs = [tool.NPSROIs ROI];
                %Add a listener for the ROI being deleted (so the ROI list
                %can be appropriately updated if the user deletes the ROI)
                %fun = @(tool,src,evnt) handlePropEvents(tool,src,evnt,'TTFROI');
                %addlistener(ROI,'ROIdeleted',fun);
                addlistener(ROI,'ROIdeleted',@tool.handlePropEvents);
            else
                ROI = [];
            end
            
            switch nargout
                case 1
                    varargout{1} = ROI;
            end
            nNew = tool.nNPSROIs;
            if n~=nNew
                notify(tool,'nNPSROIsChanged')
            end
            
        end
        
        function removeNPSROI(tool,varargin)
            n = tool.nNPSROIs;
            switch nargin
                case 1
                    %remove all the ROIs
                    delete(tool.NPSROIs);
                    tool.NPSROIs = [];
                    
                case 2
                    %Remove a specific ROI
                    ROI = varargin{1};
                    ind = tool.NPSROIs==ROI;
                    delete(tool.NPSROIs(ind));
                    tool.NPSROIs = tool.NPSROIs(~ind);
                    if isempty(tool.NPSROIs)
                        tool.NPSROIs = [];
                    end
            end
            nNew = tool.nNPSROIs;
            if n~=nNew
                notify(tool,'nNPSROIsChanged')
            end
        end
        
        function updateNPSlist(tool)
            
            if tool.nNPSs>0
                %get the selected value of the NPS list
                val = get(tool.handles.MeasureNPSList,'Value');
                %Update val in case that its not in sync with the list (can
                %happen after a TTF is deleted)
                if val == 0
                    val = 1;
                elseif val > tool.nNPSs
                    val = tool.nNPSs;
                end
                
                %Update the list in the NPS listbox
                for i=1:tool.nNPSs
                    if i==val
                        tool.NPSs(i).selected = true;
                    else
                        tool.NPSs(i).selected = false;
                    end
                    str{i} = tool.NPSs(i).label;
                end
                set(tool.handles.MeasureNPSList,'Value',val,'Enable','on','String',str);
                
                %Update the text of the NPS that's selected
                set(tool.handles.NPSInfoText,'String',tool.NPSs(val).fullLabel);
                set(tool.handles.DeleteNPSButton,'Enable','on');
                set(tool.handles.ShowNPSInfoButton,'Enable','on');
                set(tool.handles.menu.file.export.NPSselected,'Enable','on');
                
                %Update the axis legend
                legend(tool.handles.NPSaxis,'off');
                h=legend(tool.handles.NPSaxis,str);
                set(h,'TextColor','w','Box','off')
                
                %Update the 2D NPS image
                nps = tool.NPSs(val).stats.NPS_2D;
                fx =  tool.NPSs(val).stats.fx;
                fy =  tool.NPSs(val).stats.fy;
                set(tool.handles.NPS2DImage,'CData',nps,'XData',fx,'YData',fy,'Visible','on');
                set(tool.handles.NPS2DAxis,'Clim',[0 max(nps(:))],'Visible','on')
                
            else
                %This happens when all the NPSs are deleted
                set(tool.handles.MeasureNPSList,'Value',1,'Enable','off','String','');
                set(tool.handles.DeleteNPSButton,'Enable','off');
                set(tool.handles.ShowNPSInfoButton,'Enable','off');
                set(tool.handles.NPSInfoText,'String','');
                legend(tool.handles.NPSaxis,'off');
                set(tool.handles.NPS2DAxis,'Visible','off')
                set(tool.handles.NPS2DImage,'Visible','off','CData',zeros(128));
                set(tool.handles.menu.file.export.NPSselected,'Enable','off');
                
            end
            
        end
        
        function nNPSROIs = get.nNPSROIs(tool)
            nNPSROIs = length(tool.NPSROIs);
        end
        
        function measureDprime(tool)
            measureDprime(tool.handles.ObserverModelController);
        end
        
        function names = get.DICOMFileNames(tool)
            if tool.CTDataLoaded
                tab = struct2table(tool.DICOMheaders);
                names = tab.Filename;
            else
                names = '';
            end
        end
        
        function psize = get.CTpsize(tool)
            if tool.CTDataLoaded
                psize = tool.DICOMheaders(1).PixelSpacing;
            else
                psize=[];
            end
        end
        
        function sliceThickness = get.CTsliceThickness(tool)
            if tool.CTDataLoaded
                sliceThickness = tool.DICOMheaders(1).SliceThickness;
            else
                sliceThickness = [];
            end
                
        end
        
        function CTImageSize = get.CTImageSize(tool)
            if tool.CTDataLoaded
                CTImageSize = getImageSize(tool.handles.imtool);
            else
                CTImageSize=[];
            end
        end
        
        function CTPath = get.CTPath(tool)
            if tool.CTDataLoaded
                [PATHSTR,NAME,EXT] = fileparts(tool.DICOMheaders(1).Filename);
                CTPath = [PATHSTR filesep];
            else
                CTPath='';
            end
        end
        
        function tab = get.keyDICOMHeaderInfoTable(tool)
            if tool.CTDataLoaded
                fnames = {'InstitutionName','Manufacturer','ManufacturerModelName',...
                          'StationName','SoftwareVersion','PatientName',...
                          'StudyDescription','SeriesDescription','StudyDate','SeriesDate',...
                          'AccessionNumber','ProtocolName','SeriesNumber','AcquisitionNumber','FilterType',...
                          'BodyPartExamined','KVP','XrayTubeCurrent','XRayTubeCurrent','ExposureTime','RevolutionTime',...
                          'SingleCollimationWidth','TotalCollimationWidth','TableFeedPerRotation',...
                          'SpiralPitchFactor','FocalSpot','CTDIvol','ScanOptions','DataCollectionDiameter','ReconstructionDiameter','PixelSpacing'...
                          'ConvolutionKernel','SliceThickness','SliceLocation','ImagePositionPatient','SeriesInstanceUID',...
                          'IrradiationEventUID'...
                };
            tab = struct2table(tool.DICOMheaders);
            [~, ~, IB] = intersect(tab.Properties.VariableNames,fnames);
            IB = sort(IB);
            fnames = fnames(IB);
            tab = tab(:,fnames);
                
            else
                tab = table;
            end
            
        end
        
        function str = get.CTSeriesSummary(tool)
            tab = tool.keyDICOMHeaderInfoTable;
            fnames = tab.Properties.VariableNames;
            str = {};
            for i=1:length(fnames)
                f = fnames{i};
                col = tab{:,fnames{i}};
                try
                    switch fnames{i}
                        case 'PatientName'
                            col = struct2table(col);
                            vals = unique(col{:,1});
                            val = '';
                            for j=1:length(vals)
                                if j==1
                                    val = vals{j};
                                else
                                    val = [val ', ' vals{j}];
                                end
                            end
                        case 'PixelSpacing'
                            vals = cell2mat(col);
                            vals = reshape(vals,[2 size(col,1)]);
                            vals = vals';
                            vals = unique(vals,'rows');
                            for j=1:size(vals,1)
                                if j==1
                                    val = ['[' num2str(vals(j,1)) ', ' num2str(vals(j,1)) ']'];
                                else
                                    val = [val ', [' num2str(vals(j,1)) ', ' num2str(vals(j,1)) ']'];
                                end
                            end
                            
                        case {'XrayTubeCurrent', 'XRayTubeCurrent', 'CTDIvol'}
                            vals = unique(col);
                            if length(vals)>1
                                vals = col;
                                val = ['Mean = ' sprintf('%.1f',mean(vals)) ', Min = ' sprintf('%.1f',min(vals))...
                                    ', Max = ' sprintf('%.1f',max(vals)) ', N unique = ' num2str(length(unique(col)))];
                            else
                                val = num2str(vals);
                            end
                        case {'ImagePositionPatient', 'SliceLocation'}
                            continue
                            
                        otherwise
                            vals = unique(col);
                            if iscell(vals)
                                for j=1:length(vals)
                                    if j==1
                                        val = vals{j};
                                    else
                                        val = [val ', ' vals{j}];
                                    end
                                end
                            elseif isnumeric(vals) && isvector(vals)
                                for j=1:length(vals)
                                    if j==1
                                        val = num2str(vals(j));
                                    else
                                        val = [val ', ' num2str(vals(j))];
                                    end
                                end
                            else
                                val = 'Error';
                                
                            end
                    end
                    str{end+1,1} = [f ': ' val];
                end
            end
            
            S=tool.CTImageSize;
            str{end+1} = ['SeriesImageSize: [' num2str(S(1)) ', ' num2str(S(2)) ', ' num2str(S(3)) ']'];
            
            [~,~,z]=getCTcoordinates(tool.DICOMheaders);
            vals = unique(diff(z));
            for j=1:length(vals)
                if j==1
                    val = num2str(vals(j));
                else
                    val = [val ', ' num2str(vals(j))];
                end
            end
            str{end+1} = ['Slice Interval: ' val];
            
        end
        
        function t = get.sliceLabel(tool)
            if tool.CTDataLoaded
                slice = getCurrentSlice(tool.handles.imtool);
                t={};
                try t{end+1}=['Institution: ' tool.DICOMheaders(slice).InstitutionName]; end
                try t{end+1}=['Manufacturer: ' tool.DICOMheaders(slice).Manufacturer]; end
                try t{end+1}=['Model: ' tool.DICOMheaders(slice).ManufacturerModelName]; end
                try t{end+1}=['Station: ' tool.DICOMheaders(slice).StationName]; end
                try t{end+1}=['Software Version: ' tool.DICOMheaders(slice).SoftwareVersion]; end
                try t{end+1}=['Patient Name: ' tool.DICOMheaders(slice).PatientName.FamilyName]; end
                try t{end+1}=['Patient ID: ' tool.DICOMheaders(slice).PatientID]; end
                try t{end+1}=['Study Description: ' tool.DICOMheaders(slice).StudyDescription]; end
                try t{end+1}=['Series Description: ' tool.DICOMheaders(slice).SeriesDescription]; end
                try t{end+1}=['Study Date: ' tool.DICOMheaders(slice).StudyDate]; end
                try t{end+1}=['Series Date: ' tool.DICOMheaders(slice).SeriesDate]; end
                try t{end+1}=['Protocol Name: ' tool.DICOMheaders(slice).ProtocolName]; end
                try t{end+1}=['Series: ' num2str(tool.DICOMheaders(slice).SeriesNumber)];end
                try t{end+1}=['Acquisition: ' num2str(tool.DICOMheaders(slice).AcquisitionNumber)];end
                try t{end+1}=['Filter Type: ' tool.DICOMheaders(slice).FilterType]; end
                try t{end+1}=['Body Part Examined: ' tool.DICOMheaders(slice).BodyPartExamined]; end
                try t{end+1}=['kV: ' num2str(tool.DICOMheaders(slice).KVP)]; end
                try t{end+1}=['mA: ' num2str(tool.DICOMheaders(slice).XRayTubeCurrent)]; end
                try t{end+1}=['Exposure Time: ' num2str(tool.DICOMheaders(slice).ExposureTime/1000) ' s']; end
                try t{end+1}=['Revolution Time: ' num2str(tool.DICOMheaders(slice).RevolutionTime) ' s']; end
                try t{end+1}=['Single Collimation: ' num2str(tool.DICOMheaders(slice).SingleCollimationWidth) ' mm']; end
                try t{end+1}=['Total Collimation: ' num2str(tool.DICOMheaders(slice).TotalCollimationWidth) ' mm']; end
                try t{end+1} = ['Table Feed: ' num2str(tool.DICOMheaders(slice).TableFeedPerRotation) ' mm/rot']; end
                try t{end+1}=['Pitch: ' num2str(tool.DICOMheaders(slice).SpiralPitchFactor)]; end
                try t{end+1}=['Focal spot: ' num2str(tool.DICOMheaders(slice).FocalSpot) ' mm']; end
                try t{end+1}=['CTDIvol: ' num2str(tool.DICOMheaders(slice).CTDIvol) ' mGy']; end
                try t{end+1}=['Scan Options: ' tool.DICOMheaders(slice).ScanOptions];end
                 try t{end+1}=['Data Collection Diameter: ' num2str(tool.DICOMheaders(slice).DataCollectionDiameter) ' mm']; end
                try t{end+1}=['Reconstructed FOV: ' num2str(tool.DICOMheaders(slice).ReconstructionDiameter) ' mm']; end
                try t{end+1}=['Kernel: ' tool.DICOMheaders(slice).ConvolutionKernel];end
                try 
                    t{end+1} = ['Image Position Patient:' ];
                    t{end+1} = ['  x: ' num2str(tool.DICOMheaders(slice).ImagePositionPatient(1)) ' mm'];
                    t{end+1} = ['  y: ' num2str(tool.DICOMheaders(slice).ImagePositionPatient(2)) ' mm'];
                    t{end+1} = ['  z: ' num2str(tool.DICOMheaders(slice).ImagePositionPatient(3)) ' mm'];
                end
                try t{end+1}=['Slice Location: ' num2str(tool.DICOMheaders(slice).SliceLocation) ' mm']; end
                try t{end+1}=['Pixel Spacing: ' num2str(tool.DICOMheaders(slice).PixelSpacing(1)) ', ' num2str(tool.DICOMheaders(slice).PixelSpacing(2)) ' mm']; end
                try t{end+1}=['Slice Thickness: ' num2str(tool.DICOMheaders(slice).SliceThickness) ' mm']; end
                try t{end+1}=['Instance: ' num2str(tool.DICOMheaders(slice).InstanceNumber)]; end
                
                
                %Remove any empty rows
                ind= true(size(t));
                for i=1:length(t)
                    if isempty(t{i})
                        ind(i)=false;
                    end
                end
                t=t(ind);
            else
                t='';
            end
            
        end
        
        function set.Visible(tool,Visible)
            tool.Visible=Visible;
            set(tool.handles.fig,'Visible',tool.VisibleString);
        end
        
        function str = get.VisibleString(tool)
            if tool.Visible
                str='on';
            else
                str='off';
            end
        end
        
        function showNewSlice(tool)
            %update the info string
            set(tool.handles.CTInfotext,'String',tool.sliceLabel);
            
            %make sure the string is visible on top of other graphics
            uistack(tool.handles.CTtextAxes,'top')
            
            %update location of TTF and NPS current slice selection objects
            %and update the world coordinates
            if tool.CTDataLoaded
                slice = getCurrentSlice(tool.handles.imtool);
                tool.handles.TTFsliceSelector.currentSlice=slice;
                tool.handles.NPSsliceSelector.currentSlice=slice;
                tool.handles.TTFzsliceSelector.currentSlice=slice;
                updateWorldCoordinatesFromDICOMHeader(tool.handles.imtool,tool.DICOMheaders(slice));
            end
            
        end
        
        function set.showCTInfo(tool,showCTInfo)
            tool.showCTInfo = showCTInfo;
            if tool.showCTInfo
                set(tool.handles.CTInfotext,'Visible','on');
                set(tool.handles.CTInfoVisibleCheck,'Value',true);
            else
                set(tool.handles.CTInfotext,'Visible','off');
                set(tool.handles.CTInfoVisibleCheck,'Value',false);
            end
        end
        
        function im = get.bannerImage(tool)
            pos = round(getPixelPosition(tool.handles.bannerAxis));
            pos = pos(3:4);
            im = zeros(pos(2),pos(1),3);
            im(:,:,1) = repmat(linspace(tool.BannerColor(1,1),tool.BannerColor(2,1),pos(1)),[pos(2) 1]);
            im(:,:,2) = repmat(linspace(tool.BannerColor(1,2),tool.BannerColor(2,2),pos(1)),[pos(2) 1]);
            im(:,:,3) = repmat(linspace(tool.BannerColor(1,3),tool.BannerColor(2,3),pos(1)),[pos(2) 1]);
        end
        
        function tab = get.versionInfoTable(tool)
            for i=1:length(tool.dependentRepositories)
                tab(i).RepositoryName = tool.dependentRepositories{i};
                tab(i).GitID = tool.gitSHA1CheckSums{i};
                tab(i).BranchName = tool.gitBranchNames{i};
            end
            tab = struct2table(tab);
        end
        
        function res = autoAnalyzeACRPhantom(tool,varargin)
            
            switch nargin
                case 2
                    input = varargin{1};
                    %Load the images
                    loadCTdata(tool,input);
                    
            end
            
            
            %Run the analysis
            res = imquest_ACRPhantomAutoAnalyze(tool);
            
        end
        
        function varargout = savemAProfileReportFigure(tool,varargin)
            %This method saves an image which overlays the mA profile onto
            %a simulated scout image
            
            switch nargin
                case 1
                    fname = '';
                case 2
                    fname = varargin{1};
            end
            
            %get scout image
            [x,y,z]=getCTcoordinates(tool.DICOMheaders);
            scout1 = mat2gray(squeeze(mean(getImageSlices(tool.handles.imtool,1,tool.CTImageSize(3)),2)));
            scout2 = mat2gray(squeeze(max(getImageSlices(tool.handles.imtool,1,tool.CTImageSize(3)),[],2)));
            scout = (2/3)*scout1 + (1/3)*scout2; %This blends a MIP with a mean image
            
            %Get aspect ratios
            w = range(z);
            h = range(y);
            Ai = w/h; %Aspect ratio of scout image
            
            %Make the figure
            ySize = 5;
            xSize = 6.5;
            
            if Ai>1
                ySize=xSize/Ai;
            else
                xSize = Ai*ySize;
            end
            fSize=10;
            fig = createFigForPrinting(xSize,ySize);
            set(fig,'Visible','on')
            
            as = axes('Parent',fig,'Position',[.125 .125 .75 .75]);
            %as = axes('Parent',fig,'Position',[0 0  1  1]);
            hold on
            hi = imshow(scout,'XData',z,'YData',y,'Parent',as);
            set(as,'YTick',[],'XLim',[min(z) max(z)],'YLim',[min(y) max(y)]);
            xlabel('Slice Position [mm]');
            
            %Create mA profile
            for i=1:tool.CTImageSize(3)
                mA(i,1)=tool.DICOMheaders(i).XRayTubeCurrent;
                ylab='Tube Current [mA]';
                ylims = [0 50 300 800 1000 2000];
            end
            
            %Find the max
            m = max(mA);
            ind = interp1(ylims,1:length(ylims),m);
            ind = floor(ind);
            if ind==length(ylims)
                ylims = [ylims(1) m*1.1];
            else
                ylims = [ylims(1) ylims(ind+1)];
            end
            
            %Get the label about rotation time and pitch
            try 
                s = tool.DICOMheaders(i).RevolutionTime;
            catch
                s = tool.DICOMheaders(i).ExposureTime/1000;
            end
            try
                p = tool.DICOMheaders(i).SpiralPitchFactor;
            catch
                try
                    p = tool.DICOMheaders(i).TableFeedPerRotation/tool.DICOMheaders(i).TotalCollimationWidth;
                catch
                    p=1;
                end
            end
            tlab = sprintf('Rotation time: %0.1f s, Pitch: %0.1f',s,p);
            
            
            ap = axes('Parent',fig,'Color','none','Position',as.Position);
            hold on;
            h=plot(z,mA,'-or','LineWidth',1);
            h.Marker='.';
            set(ap,'Xlim',get(as,'Xlim'),'YLim',ylims,'YColor','r');
            ylabel(ylab);
            title({tool.DICOMheaders(1).SeriesDescription,tlab},'Interpreter','none');
            
            %Create the WED profile
            ap2 = axes('Parent',fig,'Color','none','Position',as.Position);
            hold on;
            WED = getWEDfromCT(getImage(tool.handles.imtool),tool.CTpsize(1));
            h=plot(z,WED,'-ob','LineWidth',1);
            h.Marker='.';
            set(ap2,'Xlim',get(as,'Xlim'),'YLim',[0 500],'YAxisLocation','right',...
                'YColor','b','XTick',[]);
            ylabel('Water Equivalent Diameter [mm]')
            
            if ~isempty(fname)
                print(fig, '-r300', '-dpng',fname);
            end
            
            switch nargout
                case 0
                    %Close the figure;
                    close(fig);
                case 1
                    varargout{1} = fig;
            end
            
            
        end
        
        function varargout = savePhysicsReportFigure(tool,fname,varargin)
            %This method saves an image that summarizes the TTF, NPS, and
            %d-prime calculations. It is meant to be included in the annual
            %physics baseline testing report
            
            %Make the figure
            xSize = 7;
            ySize = 5;
            fSize=10;
            fig = createFigForPrinting(xSize,ySize);
            set(fig,'Color','k');
            
            %Make TTF Axes
            ax = copyobj(tool.handles.TTFaxis,fig);
            set(ax,'Units','Normalized','Position',[.075 .6 .4 .35],'FontSize',fSize);
            h=legend(tool.handles.MeasureTTFList.String);
            set(h,'TextColor','w','Box','off','FontSize',fSize)
            
            %Make NPS Axes
            ax = copyobj(tool.handles.NPSaxis,fig);
            set(ax,'Units','Normalized','Position',[.55 .6 .4 .35],'FontSize',fSize);
            t = {['noise = ' sprintf('%2.2f',tool.NPSs(1).noise) ' HU'],['fpeak = ' sprintf('%1.2f',tool.NPSs(1).fpeak) ' mm^-^1'],['fav = ' sprintf('%1.2f',tool.NPSs(1).fav) ' mm^-^1']};
            x = ax.XLim(2) - diff(ax.XLim)/20;
            y = max(tool.NPSs(1).NPS);
            text(x,y,t,'Parent',ax,'Color','w','FontSize',fSize,'HorizontalAlignment','right','VerticalAlignment','Top');
            
            %Make Dprime images
            xmin = .05;
            ymin = 0;
            h=.5;
            buff = .1;
            n=length(tool.Dprimes);
            tw = 1-2*xmin - ((n-1)*buff);
            w = tw/n;
            for i=1:length(tool.Dprimes)
                %Select the dprime of interest
                tool.handles.ObserverModelController.handles.DprimeList.Value=i;
                %Copy the object
                ax = copyobj(tool.handles.ObserverModelController.handles.axes,fig);
                %Set the position
                x = xmin + (i-1)*buff + (i-1)*w;
                set(ax,'Units','Normalized','Position',[x ymin w h],'FontSize',fSize);
                %Change the colormap
                colormap gray
                %Display the text
                x=0;
                y=ax.YLim(1)-5;
                col = get(tool.TTFs(i).graphicHandle,'Color');
                t = ['d'' = ' sprintf('%2.2f',tool.Dprimes(i).detectabilityIndex)];
                text(x,y,t,'FontSize',fSize,'Color',col,'Parent',ax,'HorizontalAlignment','center')
            end
            
            if ~isempty(fname)
                print(fig, '-r300', '-dpng',fname);
            end
            
            switch nargout
                case 0
                    %Close the figure;
                    close(fig);
                case 1
                    varargout{1} = fig;
            end
            
        end
        
        function exportTTForNPStoCSV(tool,obj,varargin)
            switch length(varargin)
                case 0 %Need to get the file name
                    [FileName,PathName] = uiputfile({'*.csv'},'Save to .csv text file');
                    filename = [PathName FileName];
                case 1
                    filename = varargin{1}; 
            end
            writeToCSVFile(obj,filename);
            
        end
        
        function runCallback(tool,hObject,evnt)
            %This function just basically lets a user run one of the
            %graphical elements callback functions from within a script
            callbacks(hObject,evnt,tool);
        end
    end
end

function callbacks(hObject,evnt,tool)
switch hObject
    case tool.handles.CTInfoButton
        if tool.CTDataLoaded
            slice = getCurrentSlice(tool.handles.imtool);
            imageinfo(tool.DICOMheaders(slice))
        end
    case tool.handles.menu.figure.AECProfile
        %Get filename
        name = [tool.CTPath 'AECProfile.png'];
        [fname, pathname] = uiputfile('*.png','Choose a file name for the figure',name);
        %Make and save the figure
        savemAProfileReportFigure(tool,[pathname fname]);
    case tool.handles.menu.figures.PhysReport
        %Get filename
        name = [tool.CTPath 'DprimeSummary.png'];
        [fname, pathname] = uiputfile('*.png','Choose a file name for the figure',name);
        %Make and save the figure
        savePhysicsReportFigure(tool,[pathname fname]);
    case tool.handles.menu.file.loadImage
        %call function that loads CT data
        input = uigetdir(tool.lastDir,'Select Folder Containing DICOM Files');
        loadCTdata(tool,input);
    case tool.handles.menu.file.sortDICOMS
        input = uigetdir(tool.lastDir,'Select Folder Containing DICOM Files');
        sortDICOMfiles(tool,input)
    case tool.handles.AddTTFROIButton
        addTTFROI(tool);
    case tool.handles.AddTTFzROIButton
        addTTFzROI(tool);
    case tool.handles.TTFVisibleCheck
        if get(hObject,'Value')
            val = true;
        else
            val = false;
        end
        for i=1:length(tool.TTFROIs)
            tool.TTFROIs(i).visible = val;
        end
    case tool.handles.TTFzVisibleCheck
        if get(hObject,'Value')
            val = true;
        else
            val = false;
        end
        for i=1:length(tool.TTFzROIs)
            tool.TTFzROIs(i).visible = val;
        end
    case tool.handles.menu.auto.ACRPhantom
        if ~tool.CTDataLoaded
            input=uigetdir;
            res = autoAnalyzeACRPhantom(tool,input);
        else
            res = autoAnalyzeACRPhantom(tool);
        end
        name = [tool.CTPath res.SeriesDescription];
        [fname, pathname] = uiputfile('*.pdf','Choose a file name for the ACR phantom report',name);
        try
            generateReport(res,[pathname fname])
        catch ME
            tool.logError(ME)
            showTempStatus(tool,'Error generating report file');
            msgbox({ME.message, pwd})
            tool.status = 'Ready';
        end
    case {tool.handles.menu.auto.MercPhantom3,tool.handles.menu.auto.MercPhantom4}
        switch hObject
            case tool.handles.menu.auto.MercPhantom3
                version = '3.0';
            case tool.handles.menu.auto.MercPhantom4
                version = '4.0';
        end
        if ~tool.CTDataLoaded
            input=uigetdir;
            tool.loadCTdata(input);
            res = imquest_MercuryPhantomAutoAnalyze(tool,version);
        else
            res = imquest_MercuryPhantomAutoAnalyze(tool,version);
        end
        name = [tool.CTPath res.SeriesDescription];
        [fname, pathname] = uiputfile('*.pdf','Choose a file name for the Mercury phantom report',name);
        try
            generateReport(res,[pathname fname])
        catch ME
            tool.logError(ME)
            showTempStatus(tool,'Error generating report file');
            msgbox({ME.message, pwd})
            tool.status = 'Ready';
        end
        
    case tool.handles.menu.auto.MercPhantomConfig
        %Load the config file
        [filename,pathname]=uigetfile('*.json','Select json config file');
        if filename
            raw = fileread([pathname filename]);
            config=jsondecode(raw);
            res = imquest_MercuryPhantomAutoAnalyze(tool,config);
            
            %
%             res = imquest_MercuryPhantomAutoAnalyze(tool,'Bare');
%             res.phantomVersion=config.phantomVersion;
%             res.phantomDiameterOverride=config.diameters';
%             sliceSize = zeros(size(res.z));
%             NPSslice = false(size(res.z));
%             TTFslice = false(size(res.z));
%             z = res.z - config.landmark;
%             if strcmp(config.orientation,'+z:-p')
%                 z=z*-1;
%             end
%             ST = res.DICOMheaders(1).SliceThickness;
%             for j = 1:length(config.diameters)
%                 zmin = min([config.NPS.sliceRanges(j,1) config.TTF.sliceRanges(j,1)]);
%                 zmax = max([config.NPS.sliceRanges(j,2) config.TTF.sliceRanges(j,2)]);
%                 ind = z >= (zmin + ST/2) & z <= (zmax - ST/2);
%                 sliceSize(ind) = config.diameters(j);
%                 
%                 zmin=config.NPS.sliceRanges(j,1);
%                 zmax=config.NPS.sliceRanges(j,2);
%                 ind = z >= (zmin + ST/2) & z <= (zmax - ST/2);
%                 NPSslice(ind)=true;
%                 
%                 zmin = config.TTF.sliceRanges(j,1);
%                 zmax = config.TTF.sliceRanges(j,2);
%                 ind = z >= (zmin + ST/2) & z <= (zmax - ST/2);
%                 TTFslice(ind) = true;
%                 
%             end
%             res.z=z;
%             res.sliceSize = sliceSize;
%             res.NPSslice = NPSslice;
%             res.TTFslice = TTFslice;
%             makeMeasurements(res);
            
            name = [tool.CTPath res.SeriesDescription];
            [fname, pathname] = uiputfile('*.pdf','Choose a file name for the Mercury phantom report',name);
            try
                generateReport(res,[pathname fname])
            catch ME
                tool.logError(ME)
                showTempStatus(tool,'Error generating report file');
                msgbox({ME.message, pwd})
                tool.status = 'Ready';
            end
            
            
            
            
        end
        
        
    case tool.handles.ClearTTFROIButton
        removeTTFROI(tool);
    case tool.handles.ClearTTFzROIButton
        removeTTFzROI(tool)
    case tool.handles.MeasureTTFROIButton
        measureTTF(tool);
    case tool.handles.MeasureTTFzROIButton
        measureTTFz(tool)
    case tool.handles.MeasureTTFList
        updateTTFlist(tool)
    case tool.handles.MeasureTTFzList
        updateTTFzlist(tool)
    case tool.handles.DeleteTTFButton
        val = get(tool.handles.MeasureTTFList,'Value');
        removeTTF(tool,tool.TTFs(val));
    case tool.handles.DeleteTTFzButton
        val = get(tool.handles.MeasureTTFzList,'Value');
        removeTTFz(tool,tool.TTFzs(val));
    case tool.handles.ShowTTFInfoButton
        val = get(tool.handles.MeasureTTFList,'Value');
        showTTFinfo(tool.TTFs(val));
    case tool.handles.ShowTTFzInfoButton
        val = get(tool.handles.MeasureTTFzList,'Value');
        showTTFinfo(tool.TTFzs(val));
    case tool.handles.AddNPSROIButton
        addNPSROI(tool);
    case tool.handles.NPSVisibleCheck
        if get(hObject,'Value')
            val = true;
        else
            val = false;
        end
        for i=1:length(tool.NPSROIs)
            tool.NPSROIs(i).visible = val;
        end
    case tool.handles.ClearNPSROIButton
        removeNPSROI(tool);
    case tool.handles.MeasureNPSROIButton
        measureNPS(tool);
    case tool.handles.MeasureNPSList
        updateNPSlist(tool)
    case tool.handles.DeleteNPSButton
        val = get(tool.handles.MeasureNPSList,'Value');
        removeNPS(tool,tool.NPSs(val));
    case tool.handles.ShowNPSInfoButton
        val = get(tool.handles.MeasureNPSList,'Value');
        showNPSinfo(tool.NPSs(val));
    case tool.handles.menu.file.export.TTFselected
        val = get(tool.handles.MeasureTTFList,'Value');
        TTF=tool.TTFs(val);
        exportTTForNPStoCSV(tool,TTF);
    case tool.handles.menu.file.export.TTFzselected
        val = get(tool.handles.MeasureTTFzList,'Value');
        TTF=tool.TTFzs(val);
        exportTTForNPStoCSV(tool,TTF)
    case tool.handles.menu.file.export.NPSselected
        val = get(tool.handles.MeasureNPSList,'Value');
        NPS=tool.NPSs(val);
        exportTTForNPStoCSV(tool,NPS);
    case tool.handles.menu.file.export.Dprimeselected
        Dprime = tool.handles.ObserverModelController.selectedDprime;
        exportTTForNPStoCSV(tool,Dprime);
    case tool.handles.menu.file.export.DprimeAll
        exportTTForNPStoCSV(tool,tool.Dprimes);
    case tool.handles.CTInfoVisibleCheck
        if get(hObject,'Value')
            tool.showCTInfo = true;
        else
            tool.showCTInfo = false;
        end
    case tool.handles.SetZeroSliceButton
        %Get the current slice
        slice = getCurrentSlice(tool.handles.imtool);
        tool.ACRZeroOffset = tool.DICOMheaders(slice).ImagePositionPatient(3);
    case mat2cell(tool.handles.CT_WLbutton,1,ones(1,length(tool.handles.CT_WLbutton))) %Goes into this block for any WL button
        WL = get(hObject,'UserData');
        if isempty(WL)
            try
                slice = getCurrentSlice(tool.handles.imtool);
                W = tool.DICOMheaders(slice).WindowWidth(1);
                L = tool.DICOMheaders(slice).WindowCenter(1);
                WL = [W L];
            end 
        end
        if ~isempty(WL)
            setWindowLevel(tool.handles.imtool,WL(1),WL(2));
        end
    case tool.handles.menu.file.viewLog
        fig = figure('MenuBar','none','Name','Log','NumberTitle','off');
        h = uicontrol(fig,'Style','listbox','String',tool.log, 'Units','normalized','Position',[0,0,1,1]);

        
end
end

function panelResizeFunction(hObject,events,tool,buff,label)
try
    switch label
        case ''
            labels = {'fig','WL','Detectability','NPS','TTF'};
            for i=1:length(labels)
                panelResizeFunction([],[],tool,buff,labels{i})
            end
        
        case 'fig'
            if ~isempty(tool.handles)
                pos = getPixelPosition(tool.handles.fig);
                set(tool.handles.bannerAxis,'Position',[0 0 pos(3) pos(4)*.1]);
                im = tool.bannerImage;
                set(tool.handles.bannerBKGImage,'CData',im)
                set(tool.handles.bannerAxis,'Xlim',[0 size(im,2)],'Ylim',[0 size(im,1)])
                
            end
        case 'WL'
            pos = getPixelPosition(tool.handles.WLPanel);
            set(tool.handles.CTInfoVisibleCheck,'Position',[0 pos(4)/2 4*buff pos(4)/2]);
            set(tool.handles.CTInfoButton,'Position',[0 0 4*buff pos(4)/2]);
            set(tool.handles.SetZeroSliceButton,'Position',[4*buff 0 3*buff pos(4)/2])
            set(tool.handles.SetZeroSliceButtonText,'Position',[4*buff pos(4)/2 3*buff pos(4)/2])
            left = 7*buff/pos(3);
            width = (1-left)/length(tool.handles.CT_WLbutton);
            for i=1:length(tool.handles.CT_WLbutton)
                set(tool.handles.CT_WLbutton(i),'Position',[left+(i-1)*width 0 width 1]);
            end
            
            
        case 'Detectability'
            
        case 'NPS'
            pos = getPixelPosition(tool.handles.tabs.NPS);
            set(tool.handles.NPSaxis,'Position',[2*buff 2*buff pos(3)-3*buff pos(4)/2-2*buff])
            set(tool.handles.AddNPSROIButton,'Position',[buff pos(4)-2*buff 4*buff buff])
            set(tool.handles.NPSVisibleCheck,'Position',[5* buff pos(4)-2*buff 4*buff buff])
            set(tool.handles.ClearNPSROIButton,'Position',[buff pos(4)-3*buff 4*buff buff])
            tool.handles.NPSsliceSelector.position = [2*buff pos(4)/2+buff 4*buff pos(4)/2-5.5*buff];
            set(tool.handles.MeasureNPSROIButton,'Position',[buff pos(4)-4*buff 4*buff buff])
            set(tool.handles.MeasureNPSList,'Position',[7*buff pos(4)/2+2*buff 4*buff pos(4)/2-6.5*buff]);
            set(tool.handles.DeleteNPSButton,'Position',[10*buff pos(4)/2+buff buff buff]);
            set(tool.handles.ShowNPSInfoButton,'Position',[9*buff pos(4)/2+buff buff buff]);
            set(tool.handles.NPSInfoText,'Position',[11.5*buff pos(4)/2+2*buff 3*buff 2*buff]);
            set(tool.handles.NPSListText,'Position',[7*buff pos(4)/2+2*buff+tool.handles.MeasureTTFList.Position(4) 4*buff buff]);
            set(tool.handles.NPS2DAxis,'Position',[pos(3)/2+buff/2 pos(4)/2+5*buff pos(3)/2 pos(4)/2-6*buff]);
        case 'TTF'
            pos = getPixelPosition(tool.handles.tabs.TTF);
            set(tool.handles.TTFaxis,'Position',[2*buff 2*buff pos(3)-3*buff pos(4)/2-2*buff])
            set(tool.handles.AddTTFROIButton,'Position',[buff pos(4)-2*buff 4*buff buff])
            set(tool.handles.TTFVisibleCheck,'Position',[5* buff pos(4)-2*buff 4*buff buff])
            set(tool.handles.ClearTTFROIButton,'Position',[buff pos(4)-3*buff 4*buff buff])
            tool.handles.TTFsliceSelector.position = [2*buff pos(4)/2+buff 4*buff pos(4)/2-5.5*buff];
            set(tool.handles.MeasureTTFROIButton,'Position',[buff pos(4)-4*buff 4*buff buff])
            set(tool.handles.MeasureTTFList,'Position',[7*buff pos(4)/2+2*buff 4*buff pos(4)/2-6.5*buff]);
            set(tool.handles.DeleteTTFButton,'Position',[10*buff pos(4)/2+buff buff buff]);
            set(tool.handles.ShowTTFInfoButton,'Position',[9*buff pos(4)/2+buff buff buff]);
            set(tool.handles.TTFInfoText,'Position',[11.5*buff pos(4)/2+2*buff pos(3) - 12.5*buff pos(4)/2-6.5*buff]);
            set(tool.handles.TTFListText,'Position',[7*buff pos(4)/2+2*buff+tool.handles.MeasureTTFList.Position(4) 4*buff buff]);
        case 'TTFz'
            pos = getPixelPosition(tool.handles.tabs.TTFz);
            set(tool.handles.TTFzaxis,'Position',[2*buff 2*buff pos(3)-3*buff pos(4)/2-2*buff])
            set(tool.handles.AddTTFzROIButton,'Position',[buff pos(4)-2*buff 4*buff buff])
            set(tool.handles.TTFzVisibleCheck,'Position',[5* buff pos(4)-2*buff 4*buff buff])
            set(tool.handles.ClearTTFzROIButton,'Position',[buff pos(4)-3*buff 4*buff buff])
            tool.handles.TTFzsliceSelector.position = [2*buff pos(4)/2+buff 4*buff pos(4)/2-5.5*buff];
            set(tool.handles.MeasureTTFzROIButton,'Position',[buff pos(4)-4*buff 4*buff buff])
            set(tool.handles.MeasureTTFzList,'Position',[7*buff pos(4)/2+2*buff 4*buff pos(4)/2-6.5*buff]);
            set(tool.handles.DeleteTTFzButton,'Position',[10*buff pos(4)/2+buff buff buff]);
            set(tool.handles.ShowTTFzInfoButton,'Position',[9*buff pos(4)/2+buff buff buff]);
            set(tool.handles.TTFzInfoText,'Position',[11.5*buff pos(4)/2+2*buff pos(3) - 12.5*buff pos(4)/2-6.5*buff]);
            set(tool.handles.TTFzListText,'Position',[7*buff pos(4)/2+2*buff+tool.handles.MeasureTTFzList.Position(4) 4*buff buff]);
            
    end
    
end
end

function [proceed,paths] = checkForDependentRespositories(fnames)
%Initialize output
proceed = true;
paths = {};

%Get the search path
PathStr=regexp(path,pathsep,'split');

for i=1:length(fnames)
    p = what(fnames{i});
    if size(p,1)>0
        p=p(1);
        paths{i,1}=[p.path filesep];
    else
        paths{i,1}='';
        warning([fnames{i} ' respository not found and is required for imquest to run properly. ' ...
                'Get the repository and add to Matlab search path!']);
    end
        
    
%     if ~isdir(fnames{i})
%         proceed = false;
%         paths{i,1}='';
%         warning([fnames{i} ' respository not found and is required for imquest to run properly. ' ...
%                 'Get the repository and add to Matlab search path!']);
%     else
%         p = what(fnames{i});
%         p=p(1);
%         paths{i,1}=[p.path filesep];
%     end
end
end

function fileDropCallback(tool,src,evnt)
%Get the event data (string of the first file or folder selected)
input = evnt.Data{1};
if ~isdir(input)
    %Get the directory of the file
    [input,~,~] = fileparts(input);
end
%read the images
loadCTdata(tool,input)
end

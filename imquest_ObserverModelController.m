% Copyright 20xx - 2019. Duke University
classdef imquest_ObserverModelController < handle
    %This class provides the graphical support to control settings of how
    %the observer model calculation are done and displaying results of
    %those calculations
    
    properties
        handles
        tool %handle to the imquest object
        position
        units='normalized';
        buff = 30; %buffer of graphics elements in pixels
    end
    
    properties (Dependent = true)
        model
        noiseGenerationMode
        pixelPitch
        zoom
        viewingDistance
        EyeFilterMode
        alpha
        specifyContrast %Boolean which determines if the task contast to be used is from the TTF (false) or specified by the user (true)
        Contrast   %If the user specifies a contrast then it returns that contrast. Otherwise returns the contrast of the task function in imquest
        args %Cell array of observer model settings that will go into the observer model calculation
        selectedDprime
    end
    
    methods
        function MO = imquest_ObserverModelController(parent,position,tool) %constructor
            
            MO.handles.parent = parent;
            MO.position=position;
            MO.tool=tool;
            
            %make the panel
            MO.handles.panel = uipanel(parent,'Units',MO.units,'Position',position,'Title','Detectability Index Calculation',...
                'BackgroundColor','k','ForegroundColor','w','HighlightColor','k',...
                'TitlePosition','lefttop','FontSize',14);
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,MO);
            set(MO.handles.panel,'ResizeFcn',fun)
            
            %make the pulldown menu for model type
            fun=@(hObject,evnt) callbacks(hObject,evnt,MO);
            MO.handles.ModelPopup = uicontrol(MO.handles.panel,'Style','popupmenu','String',{'NPW','NPWE', 'NPWi', 'NPWEi'},'Units','Pixel',...
                'TooltipString','Choose an observer model','HorizontalAlignment','center','Enable','On','Callback',fun);
            %make the pulldown menu for noise interpolation mode
            MO.handles.noiseGenerationModePopup = uicontrol(MO.handles.panel,'Style','popupmenu','String',{'2D','Radial'},'Units','Pixel',...
                'TooltipString','Choose NPS interpolation method','HorizontalAlignment','center','Enable','On','Callback',fun);
            
            %Add edit box for pixel pitch
            MO.handles.PixelPitchEdit = uicontrol(MO.handles.panel,'Style','edit','String','.2','Units','Pixel',...
                'TooltipString','Assumed pixel pitch of display','HorizontalAlignment','center','Enable','On','UserData',.2,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            MO.handles.PixelPitchText = uicontrol(MO.handles.panel,'Style','text','String',' mm display pixel pitch','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Add edit box for zoom
            MO.handles.zoomEdit = uicontrol(MO.handles.panel,'Style','edit','String','3','Units','Pixel',...
                'TooltipString','Assumed ratio of display pixels to image pixels','HorizontalAlignment','center','Enable','On','UserData',3,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            MO.handles.zoomText = uicontrol(MO.handles.panel,'Style','text','String',' zoom factor','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Add edit box for viewing distance
            MO.handles.viewingDistanceEdit = uicontrol(MO.handles.panel,'Style','edit','String','500','Units','Pixel',...
                'TooltipString','Assumed viewing distance','HorizontalAlignment','center','Enable','On','UserData',500,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            MO.handles.viewingDistanceText = uicontrol(MO.handles.panel,'Style','text','String',' mm viewing distance','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Add popup menu for eye model type
            MO.handles.EyeFilterModePopup = uicontrol(MO.handles.panel,'Style','popupmenu','String',{'Eckstein'},'Units','Pixel',...
                'TooltipString','Choose an eye filter model','HorizontalAlignment','center','Enable','On','Callback',fun);
            
            %Add edit box for internal noise alpha
            MO.handles.alphaEdit = uicontrol(MO.handles.panel,'Style','edit','String','5','Units','Pixel',...
                'TooltipString','Internal Noise Scaling factor','HorizontalAlignment','center','Enable','On','UserData',5,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            MO.handles.alphaText = uicontrol(MO.handles.panel,'Style','text','String',' alpha','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Add check box for specifying contrast of task
            MO.handles.ContrastEdit = uicontrol(MO.handles.panel,'Style','edit','String','15','Units','Pixel',...
                'TooltipString','Internal Noise Scaling factor','HorizontalAlignment','center','Enable','On','UserData',15,'Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            MO.handles.ContrastText = uicontrol(MO.handles.panel,'Style','text','String',' HU','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            MO.handles.ContrastCheck = uicontrol(MO.handles.panel,'Style','checkbox','String','Specify contrast?','Units','Pixel','Value',false,...
                'TooltipString','Define a task contrast independent of the TTF contrast','HorizontalAlignment','center','Enable','On','Callback',fun,...
                'ForegroundColor','w','BackgroundColor','k');
            
            %Add button for computing d'
            MO.handles.DprimeButton = uicontrol(MO.handles.panel,'Style','pushbutton','String','Compute d''','Units','Pixel','Value',false,...
                'TooltipString','Compute detectability index','HorizontalAlignment','center','Enable','Off','Callback',fun);
            
            %make the listbox for the calculated dprimes
            MO.handles.DprimeList = uicontrol(MO.handles.panel,'Style','listbox','Units','Pixel',...
                'TooltipString','Select a d-prime to see more info','Enable','Off','BackgroundColor','k','ForegroundColor','w','Callback',fun);
            MO.handles.DeleteDprimeButton = uicontrol(MO.handles.panel,'Style','pushbutton','String','-','Units','Pixel',...
                'TooltipString','Delete d-prime','HorizontalAlignment','center','Enable','Off','Callback',fun);
            MO.handles.ShowDprimeInfoButton = uicontrol(MO.handles.panel,'Style','pushbutton','String','?','Units','Pixel',...
                'TooltipString','Show full d-prime info','HorizontalAlignment','center','Enable','Off','Callback',fun);
            MO.handles.DprimeListText = uicontrol(MO.handles.panel,'Style','text','String','Measured d-primes','Units','Pixel',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            
            %Make the image axis to show the dprime images
            MO.handles.axes = axes('Parent',MO.handles.panel,'Units','Normalized','Position',[.5 0 .5 1],...
                'Color','k','XColor','w','YColor','w','Visible','off'); hold on
            %make the image object
            MO.handles.image = imshow(zeros(MO.tool.taskFunction.N),'XData',MO.tool.taskFunction.x,'YData',MO.tool.taskFunction.x,'Parent',MO.handles.axes);
            lims = 1.05*[-MO.tool.taskFunction.FOV/2 MO.tool.taskFunction.FOV/2];
            set(MO.handles.axes,'Xlim',lims,'Ylim',lims);
            %draw the FOV line
            MO.handles.FOVline = plot([-MO.tool.taskFunction.FOV/2 MO.tool.taskFunction.FOV/2],[-MO.tool.taskFunction.FOV/2 -MO.tool.taskFunction.FOV/2],...
                's-r','MarkerFaceColor','r','Parent',MO.handles.axes,'Visible','off');
            MO.handles.FOVtext = text(0, -MO.tool.taskFunction.FOV/2,[num2str(MO.tool.taskFunction.FOV) ' mm FOV'],'Color','w',...
                'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',12,'EdgeColor','r','BackgroundColor','k','Visible','off');
            %Draw the diameter line
            MO.handles.Diameterline = plot([-MO.tool.taskFunction.diameter/2 MO.tool.taskFunction.diameter/2],[MO.tool.taskFunction.FOV/2 MO.tool.taskFunction.FOV/2],...
                's-r','MarkerFaceColor','r','Visible','off');
            MO.handles.Diametertext = text(0, MO.tool.taskFunction.FOV/2,[num2str(MO.tool.taskFunction.diameter) ' mm'],'Color','w',...
                'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',12,'EdgeColor','r','BackgroundColor','k','Clipping','off','Visible','off');
            %Make an invisible axis to put the text on
            MO.handles.textAxes = axes('Parent',MO.handles.panel,'Units','Pixels',...
                'Color','k','XColor','w','YColor','w','Visible','off'); hold on
            MO.handles.DprimeInfoText = text(0,1,'','Color','y',...
                'HorizontalAlignment','left','BackgroundColor','none','VerticalAlignment','top');
            
            %Add listener for the TTF and NPS being measured
            addlistener(MO.tool,'nNPSsChanged',@MO.handlePropEvents);
            addlistener(MO.tool,'nTTFsChanged',@MO.handlePropEvents);
            addlistener(MO.tool,'nDprimesChanged',@MO.handlePropEvents);
            addlistener(MO.handles.DprimeList,'Value','PostSet',@MO.handlePropEvents);
            
            %update the view
            updateConrollerView(MO)
        end
        
        function handlePropEvents(MO,src,evnt,varargin)
            switch evnt.EventName
                case {'nNPSsChanged','nTTFsChanged'}
                    updateConrollerView(MO);
                case {'nDprimesChanged','PostSet'}
                    
                    %Update the view
                    updateDprimeView(MO);
            end
        end
        
        function model = get.model(MO)
            val = get(MO.handles.ModelPopup,'Value');
            str = get(MO.handles.ModelPopup,'String');
            model = str{val};
        end
        
        function noiseGenerationMode = get.noiseGenerationMode(MO)
            val = get(MO.handles.noiseGenerationModePopup,'Value');
            str = get(MO.handles.noiseGenerationModePopup,'String');
            noiseGenerationMode = str{val};
        end
        
        function pixelPitch = get.pixelPitch(MO)
            pixelPitch = get(MO.handles.PixelPitchEdit,'UserData');
        end
        
        function zoom = get.zoom(MO)
            zoom = get(MO.handles.zoomEdit,'UserData');
        end
        
        function viewingDistance = get.viewingDistance(MO)
            viewingDistance = get(MO.handles.viewingDistanceEdit,'UserData');
        end
        
        function EyeFilterMode = get.EyeFilterMode(MO)
            val = get(MO.handles.EyeFilterModePopup,'Value');
            str = get(MO.handles.EyeFilterModePopup,'String');
            EyeFilterMode = str{val};
        end
        
        function alpha = get.alpha(MO)
            alpha = get(MO.handles.alphaEdit,'UserData');
        end
        
        function specifyContrast = get.specifyContrast(MO)
            specifyContrast = logical(get(MO.handles.ContrastCheck,'Value'));
        end
        
        function Contrast = get.Contrast(MO)
            if MO.specifyContrast
                Contrast = get(MO.handles.ContrastEdit,'UserData');
            else
                Contrast = MO.tool.taskFunction.Contrast;
            end
        end
        
        function updateConrollerView(MO)
            
            %make the d' button enabled or not depending on if the user has
            %measured NPS or TTF
            if MO.tool.canComputeDetectability
                set(MO.handles.DprimeButton,'Enable','on');
            else
                set(MO.handles.DprimeButton,'Enable','off');
            end
            
            %Set all the other graphics 
            if MO.specifyContrast
                set(MO.handles.ContrastEdit,'Visible','on')
                set(MO.handles.ContrastText,'Visible','on')
            else
                set(MO.handles.ContrastEdit,'Visible','off')
                set(MO.handles.ContrastText,'Visible','off')
            end
            
            switch MO.model
                case 'NPW'
                    EyefiltEnable ='off';
                    InternalNoiseEnable ='off';
                case 'NPWE'
                    EyefiltEnable ='on';
                    InternalNoiseEnable ='off';
                case 'NPWi'
                    EyefiltEnable ='off';
                    InternalNoiseEnable ='on';
                case 'NPWEi'
                    EyefiltEnable ='on';
                    InternalNoiseEnable ='on';
            end
            
            set(MO.handles.EyeFilterModePopup,'Visible',EyefiltEnable);
            set(MO.handles.PixelPitchEdit,'Visible',EyefiltEnable);
            set(MO.handles.zoomEdit,'Visible',EyefiltEnable);
            set(MO.handles.viewingDistanceEdit,'Visible',EyefiltEnable);
            set(MO.handles.alphaEdit,'Visible',InternalNoiseEnable);
            set(MO.handles.PixelPitchText,'Visible',EyefiltEnable);
            set(MO.handles.zoomText,'Visible',EyefiltEnable);
            set(MO.handles.viewingDistanceText,'Visible',EyefiltEnable);
            set(MO.handles.alphaText,'Visible',InternalNoiseEnable);
        end
        
        function updateDprimeView(MO)
            
            if MO.tool.nDprimes>0
                %Change the displayed dprime to the most recently
                %calculated one
                st = get(MO.handles.DprimeList,'String');
                if MO.tool.nDprimes > length(st)
                    set(MO.handles.DprimeList,'Value',MO.tool.nDprimes);
                end
                
                %get the selected value of the NPS list
                val = get(MO.handles.DprimeList,'Value');
                %Update val in case that its not in sync with the list (can
                %happen after a Dprime is deleted)
                if val == 0
                    val = 1;
                elseif val > MO.tool.nDprimes
                    val = MO.tool.nDprimes;
                end
                
                for i=1:MO.tool.nDprimes
                    str{i} = MO.tool.Dprimes(i).label;
                end
                
                set(MO.handles.DprimeList,'Value',val,'Enable','on','String',str);
                set(MO.handles.DprimeInfoText,'String',MO.tool.Dprimes(val).fullLabel);
                
                %Update the Dprime image
                FOV = MO.tool.Dprimes(val).FOV;
                diameter = MO.tool.Dprimes(val).task.diameter;
                lims = 1.05*[-FOV/2 FOV/2];
                im = MO.tool.Dprimes(val).im;
                set(MO.handles.image,'CData',im,'Visible','on','XData',MO.tool.Dprimes(val).task.x,'YData',MO.tool.Dprimes(val).task.x);
                set(MO.handles.axes,'Clim',[min(im(:)) max(im(:))],'XLim',lims,'Ylim',lims);
                
                %update the lines
                set(MO.handles.FOVline,'XData',[-FOV/2 FOV/2],'YData',[-FOV/2 -FOV/2],'Visible','on');
                set(MO.handles.FOVtext,'String',[num2str(FOV) ' mm FOV'],'Position',[0 -FOV/2],'Visible','on');
                set(MO.handles.Diameterline,'XData',[-diameter/2 diameter/2],'YData',[FOV/2 FOV/2],'Visible','on');
                set(MO.handles.Diametertext,'String',[num2str(diameter) ' mm'],'Position',[0 FOV/2],'Visible','on');
                
                %Update the buttons
                set(MO.handles.DeleteDprimeButton,'Enable','on')
                set(MO.handles.ShowDprimeInfoButton,'Enable','on')
                
                %Update the menu item in imquest main figure
                set(MO.tool.handles.menu.file.export.Dprimeselected,'Enable','on');
                set(MO.tool.handles.menu.file.export.DprimeAll,'Enable','on');
                set(MO.tool.handles.menu.view.DprimeSizeContrast,'Enable','on');
                set(MO.tool.handles.menu.figures.PhysReport,'Enable','on');
                
            else
                set(MO.handles.DprimeList,'Value',1,'String','','Enable','off');
                set(MO.handles.image,'Visible','off');
                
                set(MO.handles.FOVline ,'Visible','off');
                set(MO.handles.FOVtext ,'Visible','off');
                set(MO.handles.Diameterline ,'Visible','off');
                set(MO.handles.Diametertext ,'Visible','off');
                
                set(MO.handles.DeleteDprimeButton,'Enable','off')
                set(MO.handles.ShowDprimeInfoButton,'Enable','off')
                set(MO.handles.DprimeInfoText,'String','');
                
                set(MO.tool.handles.menu.file.export.Dprimeselected,'Enable','off');
                set(MO.tool.handles.menu.file.export.DprimeAll,'Enable','off');
                set(MO.tool.handles.menu.view.DprimeSizeContrast,'Enable','off');
                set(MO.tool.handles.menu.figures.PhysReport,'Enable','off');
            end
            
        end
        
        function Dprime = get.selectedDprime(MO)
            if MO.tool.nDprimes>0
                val = get(MO.handles.DprimeList,'Value');
                Dprime = MO.tool.Dprimes(val);
            else
                Dprime=[];
            end
        end
        
        function args = get.args(MO)
            %Returns a cell array of model observer settings
            args{1} = 'model'; args{2} = MO.model;
            args{3} = 'noiseGenerationMode'; args{4}=MO.noiseGenerationMode;
            args{5} = 'pixelPitch'; args{6}=MO.pixelPitch;
            args{7}='zoom'; args{8} = MO.zoom;
            args{9} = 'viewingDistance'; args{10}=MO.viewingDistance;
            args{11} = 'EyeFilterMode'; args{12}=MO.EyeFilterMode;
            args{13} = 'alpha'; args{14}=MO.alpha;
            
        end
        
        function measureDprime(MO)
            if MO.tool.canComputeDetectability
                task = copy(MO.tool.taskFunction); %Get a copy of the task function
                for i=1:MO.tool.nTTFs
                    %Set the contrast of the task as needed
                    if MO.specifyContrast
                        task.Contrast=MO.Contrast;
                    else
                        task.Contrast = MO.tool.TTFs(i).contrast;
                    end
                    
                    for j=1:MO.tool.nNPSs
                        Dprime = imquest_Dprime(task,MO.tool.TTFs(i),MO.tool.NPSs(j),MO.args); %This creates the Dprime object
                        addDprime(MO.tool,Dprime); %This adds the Dprime to the list
                    end
                end
            end
        end
    end
end

function callbacks(hObject,evnt,MO)
    switch hObject
        case  MO.handles.ModelPopup
            updateConrollerView(MO)
        case  MO.handles.noiseGenerationModePopup
        case  MO.handles.PixelPitchEdit
            checkEditBoxValue(hObject,'GreaterThanZero');
        case MO.handles.zoomEdit
            checkEditBoxValue(hObject,'GreaterThanZero');
        case MO.handles.viewingDistanceEdit
            checkEditBoxValue(hObject,'GreaterThanZero');
        case MO.handles.EyeFilterModePopup
        case MO.handles.alphaEdit
            checkEditBoxValue(hObject,'GreaterThanZero');
        case MO.handles.ContrastCheck
            updateConrollerView(MO)
        case MO.handles.DprimeButton
            measureDprime(MO)
        case MO.handles.ContrastEdit
            checkEditBoxValue(hObject,'GreaterThanZero');
        case MO.handles.DprimeList
            updateDprimeView(MO)
        case MO.handles.DeleteDprimeButton
            val = get(MO.handles.DprimeList,'Value');
            removeDprime(MO.tool,MO.tool.Dprimes(val));
        case MO.handles.ShowDprimeInfoButton
            val = get(MO.handles.DprimeList,'Value');
            showDprimeinfo(MO.tool.Dprimes(val));
    end
end

function panelResizeFunction(hObject,events,MO)
pos = getPixelPosition(hObject);
buff=MO.buff;
set(MO.handles.DprimeButton,'Position',[buff pos(4)-2*buff 4*buff buff])
set(MO.handles.ContrastCheck,'Position',[buff pos(4)-3*buff 4*buff buff])
set(MO.handles.ContrastEdit,'Position',[5*buff pos(4)-3*buff buff buff])
set(MO.handles.ContrastText,'Position',[6*buff pos(4)-3*buff buff buff])
set(MO.handles.ModelPopup,'Position',[buff pos(4)-4*buff 4*buff buff])
set(MO.handles.noiseGenerationModePopup,'Position',[buff pos(4)-5*buff 4*buff buff])
set(MO.handles.EyeFilterModePopup,'Position',[buff pos(4)-6*buff 4*buff buff])
set(MO.handles.PixelPitchEdit,'Position',[buff pos(4)-7*buff buff buff])
set(MO.handles.PixelPitchText,'Position',[2*buff pos(4)-7*buff 4*buff buff])
set(MO.handles.zoomEdit,'Position',[buff pos(4)-8*buff buff buff])
set(MO.handles.zoomText,'Position',[2*buff pos(4)-8*buff 4*buff buff])
set(MO.handles.viewingDistanceEdit,'Position',[buff pos(4)-9*buff buff buff])
set(MO.handles.viewingDistanceText,'Position',[2*buff pos(4)-9*buff 4*buff buff])
set(MO.handles.alphaEdit,'Position',[buff pos(4)-10*buff buff buff])
set(MO.handles.alphaText,'Position',[2*buff pos(4)-10*buff 4*buff buff])
set(MO.handles.DprimeList,'Position',[7*buff 2*buff pos(3)/2-7*buff pos(4)-4*buff])
set(MO.handles.DeleteDprimeButton,'Position',[7*buff + pos(3)/2-7*buff-buff buff buff buff])
set(MO.handles.ShowDprimeInfoButton,'Position',[7*buff + pos(3)/2-7*buff-2*buff buff buff buff])
set(MO.handles.DprimeListText,'Position',[7*buff pos(4)-2*buff pos(3)/2-7*buff buff]);
set(MO.handles.textAxes,'Position',[pos(3)/2+buff/5 2*buff pos(3)/2-buff pos(4)-4*buff]);
end

% Copyright 20xx - 2019. Duke University
function im = readDICOMforDatastoreObject(filename)

info = dicominfo(filename);
im = double(dicomread(info));

im=mat2gray(im*info.RescaleSlope+info.RescaleIntercept);


end
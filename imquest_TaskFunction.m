% Copyright 20xx - 2019. Duke University
classdef imquest_TaskFunction < handle
    
    properties
        diameter = 5;
        profileType = 'Designer';
        n = 1;      %this is the exponent of the designer nodule or blur factor of the gaussian nodule (is not used for flat nodules)
        Contrast = 15;
        psize = .05;
        N = 300; %Number of voxels (always a square image)
        
    end
    
    properties (Dependent = true)
        im  %image of the task function
        W   %Task function: magnitude squared of FFT of the task image, returns in shifted format (i.e., DC at center of W)
        FOV %Field of view
        x   %Spatial corrdinates of the task function
        R   %Radial coordinates of the task function
        fx  %Frequency coordinates of the task function
        df  %Sampling rate in frequency domain (i.e., spacing of elements in frequency axis)
        Fr  %Radial frequency coordinates of the task function
        taskinfo
        
    end
    
    properties (Constant = true)
        possibleProfileTypes = {'Designer','Gaussian','Flat'};
    end
    
    events
        taskFunctionChanged %is triggered whenever a property of the class is changed
    end
    
    methods
        
        
        function set.diameter(task,diameter)
            task.diameter=diameter;
            notify(task,'taskFunctionChanged')
        end
        
        function set.profileType(task,profileType)
            if any(strcmp(task.possibleProfileTypes,profileType))
                task.profileType=profileType;
                notify(task,'taskFunctionChanged')
            else
                warning('Profile type not supported');
            end
        end
        
        function set.n(task,n)
            task.n=n;
            notify(task,'taskFunctionChanged')
        end
        
        function set.Contrast(task,Contrast)
            task.Contrast=Contrast;
            notify(task,'taskFunctionChanged')
        end
        
        function set.psize(task,psize)
            task.psize=psize;
            notify(task,'taskFunctionChanged')
        end
        
        function set.N(task,N)
            task.N=N;
            notify(task,'taskFunctionChanged')
        end
        
        function im = get.im(task)
            %This method creates an image of the task function
            switch task.profileType
                case 'Designer'
                    fun = @(R) designerNodule(R,task.diameter/2,task.n,task.Contrast);
                case 'Gaussian'
                    fun = @(R) gaussianNodule(R,task.diameter/2,task.n,task.Contrast);
                case 'Flat'
                    fun = @(R) flatNodule(R,task.diameter/2,task.Contrast);
                otherwise
                    warning('Profile type not supported, using Designer');
                    task.profileType='Designer';
            end
            im = fun(task.R);
            
            
            
        end
        
        function W = get.W(task)
            W = fftshift(abs(task.psize^2*fftn(task.im)).^2);
        end
        
        function fx = get.fx(task)
            fx = getFFTfrequency(task.psize,task.N);
        end
        
        function df = get.df(task)
            Fs = 1/task.psize;
            df = Fs/task.N;
        end
        
        function Fr = get.Fr(task)
            [Fx,Fy]=meshgrid(task.fx,task.fx);
            [~,Fr]=cart2pol(Fx,Fy);
        end
        
        function FOV = get.FOV(task)
            FOV = task.psize*task.N;
        end
        
        function x = get.x(task)
            x = (0:(task.N-1))*task.psize - task.FOV/2;
        end
        
        function R = get.R(task)
            [X,Y]=meshgrid(task.x,task.x);
            [~,R]=cart2pol(X,Y);
        end
        
        function taskinfo = get.taskinfo(task)
            taskinfo.diameter=task.diameter;
            taskinfo.profileType=task.profileType;
            taskinfo.n=task.n;
            taskinfo.Contrast=task.Contrast;
            taskinfo.psize=task.psize;
            taskinfo.N=task.N;
        end
        
        function newTask = copy(task)
            %make a new copy of a task function
            newTask = imquest_TaskFunction;
            newTask.diameter=task.diameter;
            newTask.profileType=task.profileType;
            newTask.n=task.n;
            newTask.Contrast=task.Contrast;
            newTask.psize=task.psize;
            newTask.N=task.N;
        end
    end
    
    
end

function c = designerNodule(r,R,n,C)

c=(1-(r./R).^2).^n;
c(r>R)=0;
c=c*C;

end

function c = gaussianNodule(r,R,n,C)

c = (C/2)*(1-erf((r-R)/n));

end

function c = flatNodule(r,R,C)
c = C*double(r<=R);


end
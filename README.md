# imquest
imquest is an image analysis tool built in Matlab designed to measure resolution (MTF/TTF), noise (NPS), and detectability (d') properties from CT images. The tool is designed to help implement the task-based image qualtiy analysis methods described in AAPM Report 233.

See the project's [wiki](https://gitlab.oit.duke.edu/railabs/SameiResearchGroup/imquest/-/wikis/home) page for more details.
